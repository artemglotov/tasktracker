from irontasklib.services.services import Services
from irontasklib.storage.sqlalchemy_repos import SQLAlchemyRepos


storage = SQLAlchemyRepos("mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
service = Services(storage, need_check=True, is_archived=False)


def get_service():
    return service

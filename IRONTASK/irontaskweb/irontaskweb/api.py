import json
from datetime import datetime

from django.shortcuts import render_to_response
from django.template.loader import render_to_string

from irontaskweb import get_service
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.services import exceptions as ex

from irontaskweb.views import (page404, page403)

EXECUTORS = 'executors'
OBSERVERS = 'observers'

@login_required(login_url='/irontask/login/')
def remove_task(request):
    user_id = request.user.id
    task_id = request.POST[model.TASK_ID]
    try:
        get_service().remove_task(task_id, current_user_id=user_id)
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page403(str(e))

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def remove_board(request):
    user_id = request.user.id
    board_id = request.POST[model.BOARD_ID]
    try:
        get_service().remove_board(board_id, current_user_id=user_id)
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page403(str(e))

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def remove_plan(request):
    user_id = request.user.id
    plan_id = request.POST[model.PLAN_ID]
    try:
        get_service().remove_plan(plan_id, current_user_id=user_id)
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page403(str(e))

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def send_message(request):
    user_id = request.user.id
    task_id = request.POST[model.TASK_ID]
    text = request.POST[model.MESSAGE_TEXT]
    if user_id and task_id:
        get_service().add_message(model.Message(text, task_id, datetime.now(), user_id), current_user_id=user_id)
    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def get_free_task_users(request):
    user_id = request.user.id
    task_id = request.POST[model.TASK_ID]
    user_type = request.POST['user_type']
    task = get_service().get_task(task_id, current_user_id=user_id)

    role = enum.Role.EXECUTOR.value if user_type == EXECUTORS else enum.Role.OBSERVER.value

    added_users = get_service().get_task_user_relations_filtered(task_id=task_id, roles=[role], current_user_id=user_id)
    added_users_id = [relation.user_id for relation in added_users]

    all_users = get_service().get_board_users(permissions=[enum.BoardPermission.EDITOR.value,
                                                           enum.BoardPermission.CREATOR.value,
                                                           enum.BoardPermission.ADMIN.value],
                                              board_id=task.board_id,
                                              current_user_id=user_id)

    result_users = [user for user in all_users if user.user_id not in added_users_id]
    html = render_to_string(template_name='components/user_list_component.html',
                            context={'users': result_users, 'is_add': True, 'is_remove': False, 'user_type': user_type})
    return HttpResponse(html)


@login_required(login_url='/irontask/login/')
def load_task_users(request):
    user_id = request.user.id
    task_id = request.POST[model.TASK_ID]
    user_type = request.POST['user_type']

    if user_type == 'executors':
        users = get_service().get_task_executors(task_id=task_id, current_user_id=user_id)
    else:
        users = get_service().get_task_observers(task_id=task_id, current_user_id=user_id)

    html = render_to_string(template_name='components/user_list_component.html',
                            context={'users': users, 'is_add': False, 'is_remove': True, 'user_type': user_type})
    return HttpResponse(html)


@login_required(login_url='/irontask/login/')
def add_task_user(request):
    user_id = request.user.id
    add_user_id = request.POST['add_user_id']
    task_id = request.POST[model.TASK_ID]
    user_type = request.POST['user_type']

    role = enum.Role.EXECUTOR.value if user_type == EXECUTORS else enum.Role.OBSERVER.value
    new_user = model.TaskUserRelation(role=role, task_id=task_id, user_id=add_user_id)

    get_service().add_task_user_relation(new_user, current_user_id=user_id)

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def remove_task_user(request):
    user_id = request.user.id
    remove_user_id = request.POST['remove_user_id']
    task_id = request.POST[model.TASK_ID]
    user_type = request.POST['user_type']

    role = enum.Role.EXECUTOR.value if user_type == EXECUTORS else enum.Role.OBSERVER.value

    relations = get_service().get_task_user_relations_filtered(task_id=task_id, user_id=remove_user_id,
                                                               roles=[role], current_user_id=user_id)
    relation = next(iter(relations or []), None)

    if relation:
        get_service().remove_task_user_relation(relation.relation_id, current_user_id=user_id)
        return JsonResponse({'success': 'true'})

    return JsonResponse({'success': 'false'})


@login_required(login_url='/irontask/login/')
def get_free_board_users(request):
    user_id = request.user.id
    board_id = request.POST[model.BOARD_ID]
    permission = request.POST[model.USER_BOARD_RELATION_PERMISSION]

    added_users = get_service().get_user_board_relations_filtered(board_id=board_id, current_user_id=user_id)
    added_users_id = [relation.user_id for relation in added_users]

    all_users = get_service().get_users()

    result_users = [user for user in all_users if user.user_id not in added_users_id]
    html = render_to_string(template_name='components/user_list_component.html',
                            context={'users': result_users, 'is_add': True, 'is_remove': False,
                                     'user_type': permission, 'is_board': True})
    return HttpResponse(html)


@login_required(login_url='/irontask/login/')
def load_board_users(request):
    user_id = request.user.id
    board_id = request.POST[model.BOARD_ID]
    permission = request.POST[model.USER_BOARD_RELATION_PERMISSION]

    get_users = {
        enum.BoardPermission.ADMIN.name.lower(): get_service().get_board_users(
            board_id=board_id, permissions=[enum.BoardPermission.ADMIN.value], current_user_id=user_id
        ),
        enum.BoardPermission.READER.name.lower(): get_service().get_board_users(
            board_id=board_id, permissions=[enum.BoardPermission.READER.value], current_user_id=user_id
        ),
        enum.BoardPermission.EDITOR.name.lower(): get_service().get_board_users(
            board_id=board_id, permissions=[enum.BoardPermission.EDITOR.value], current_user_id=user_id
        ),
        enum.BoardPermission.BLOCKED.name.lower(): get_service().get_board_users(
            board_id=board_id, permissions=[enum.BoardPermission.BLOCKED.value], current_user_id=user_id
        )
    }

    users = get_users[permission]
    html = render_to_string(template_name='components/user_list_component.html',
                            context={'users': users, 'is_add': False, 'is_remove': True,
                                     'user_type': permission, 'is_board': True})
    return HttpResponse(html)


@login_required(login_url='/irontask/login/')
def add_board_user(request):
    user_id = request.user.id
    add_user_id = request.POST['add_user_id']
    board_id = request.POST[model.BOARD_ID]
    permission = enum.BoardPermission[request.POST[model.USER_BOARD_RELATION_PERMISSION].upper()].value

    new_user = model.UserBoardRelation(permission=permission, board_id=board_id, user_id=add_user_id)

    get_service().add_user_board_relation(new_user, current_user_id=user_id)

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def remove_board_user(request):
    user_id = request.user.id
    remove_user_id = request.POST['remove_user_id']
    board_id = request.POST[model.BOARD_ID]
    permission = enum.BoardPermission[request.POST[model.USER_BOARD_RELATION_PERMISSION].upper()].value

    relations = get_service().get_user_board_relations_filtered(board_id=board_id, user_id=remove_user_id,
                                                                permission=permission, current_user_id=user_id)
    relation = next(iter(relations or []), None)

    if relation:
        get_service().remove_user_board_relation(relation.relation_id, current_user_id=user_id)
        return JsonResponse({'success': 'true'})

    return JsonResponse({'success': 'false'})


@login_required(login_url='/irontask/login/')
def get_boards(request):
    user_id = request.user.id
    term = request.POST['term']

    boards = get_service().get_user_boards(current_user_id=user_id, title=term)
    result = [{'label': board.title, 'value': board.board_id} for board in boards]
    return JsonResponse(result, safe=False)


@login_required(login_url='/irontask/login/')
def get_tasks(request):
    user_id = request.user.id
    term = request.POST['term']

    tasks = get_service().get_user_tasks(current_user_id=user_id, title=term,
                                         statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value,
                                                   enum.TaskStatus.REQUEST.value, enum.TaskStatus.COMPLETED.value,
                                                   enum.TaskStatus.FAILED.value])
    result = [{'label': task.title, 'value': task.task_id} for task in tasks]
    return JsonResponse(result, safe=False)


@login_required(login_url='/irontask/login/')
def get_tags(request):
    user_id = request.user.id
    term = request.POST['term']

    tags = get_service().get_user_tags(text=term, current_user_id=user_id)
    result = list(set([tag.text for tag in tags]))
    return JsonResponse(result, safe=False)


@login_required(login_url='/irontask/login/')
def get_child_tasks(request):
    user_id = request.user.id
    parent_id = int(request.POST[model.TASK_ID])
    parent_task = get_service().get_task(parent_id, current_user_id=user_id)

    poss_child_tasks = get_service().get_tasks_filtered(
        statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value],
        board_id=parent_task.board_id
    )

    result = [{'value': task.task_id, 'text': task.title} for task in poss_child_tasks if task.task_id != parent_id]
    return JsonResponse(result, safe=False)


@login_required(login_url='/irontask/login/')
def add_task_relation(request):
    user_id = request.user.id
    parent_id = int(request.POST[model.TASK_RELATION_PARENT_ID])
    child_id = int(request.POST[model.TASK_RELATION_CHILD_ID])
    relation_type = int(request.POST[model.TASK_RELATION_TYPE])

    try:
        get_service().add_task_relation(model.TaskRelation(parent_id=parent_id, child_id=child_id,
                                                           relation_type=relation_type), current_user_id=user_id)
    except ex.LogicError as err:
        return JsonResponse({'success': 'false', 'error': str(err)})
    except ex.DuplicationError as err:
        return JsonResponse({'success': 'false', 'error': str(err)})

    return JsonResponse({'success': 'true'})


@login_required(login_url='/irontask/login/')
def remove_task_relation(request):
    user_id = request.user.id
    relation_id = request.POST[model.TASK_RELATION_ID]

    get_service().remove_task_relation(relation_id, current_user_id=user_id)

    return JsonResponse({'success': 'true'})
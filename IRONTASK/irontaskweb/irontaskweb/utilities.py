def get_enum_choices(enum_class):
    return [(choice.value, choice.name.lower()) for choice in enum_class]


def get_model_choices(value_field, name_field, items):
    return [(getattr(item, value_field), getattr(item, name_field)) for item in items]

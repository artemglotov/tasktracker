from django.conf.urls import url
from irontaskweb import api


urlpatterns = [
    url(r"^remove_task/$", api.remove_task, name="remove_task"),
    url(r"^remove_board/$", api.remove_board, name="remove_board"),
    url(r"^remove_plan/$", api.remove_plan, name="remove_plan"),

    url(r"^send_message/$", api.send_message, name="send_message"),

    url(r"^get_free_task_users/$", api.get_free_task_users, name="get_free_task_users"),
    url(r"^add_task_user/$", api.add_task_user, name="add_task_user"),
    url(r"^remove_task_user/$", api.remove_task_user, name="remove_task_user"),
    url(r"^load_task_users/$", api.load_task_users, name="load_task_users"),

    url(r"^get_free_board_users/$", api.get_free_board_users, name="get_free_board_users"),
    url(r"^add_board_user/$", api.add_board_user, name="add_board_user"),
    url(r"^remove_board_user/$", api.remove_board_user, name="remove_board_user"),
    url(r"^load_board_users/$", api.load_board_users, name="load_board_users"),

    url(r"^get_boards/$", api.get_boards, name="get_boards"),
    url(r"^get_tasks/$", api.get_tasks, name="get_tasks"),
    url(r"^get_tags/$", api.get_tags, name="get_tags"),

    url(r"^get_child_tasks/$", api.get_child_tasks, name="get_child_tasks"),
    url(r"^add_task_relation/$", api.add_task_relation, name="add_task_relation"),
    url(r"^remove_task_relation/$", api.remove_task_relation, name="remove_task_relation")
]
from irontaskweb import views
from django.contrib.auth import views as auth_views
from django.conf.urls import url, include

app_name = "irontask"
urlpatterns = [
    url(r"^$", views.home),
    url(r"^home/$", views.home, name="home"),

    url(r'^task/', include('irontaskweb.urlpatterns.task_urls')),
    url(r'^board/', include('irontaskweb.urlpatterns.board_urls')),
    url(r'^plan/', include('irontaskweb.urlpatterns.plan_urls')),

    url(r'^api/', include('irontaskweb.urlpatterns.api_urls')),
    url(r"^registration/$", views.registration, name="registration"),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout')
]
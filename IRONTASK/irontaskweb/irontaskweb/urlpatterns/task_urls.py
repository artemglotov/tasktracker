from django.conf.urls import url
from irontaskweb import views

urlpatterns = [
    url(r"^all/$", views.show_tasks, name="show_tasks"),
    url(r"^create/$", views.add_task, name="add_task"),
    url(r"^details/(?P<task_id>[0-9]+)$", views.show_task, name="show_task"),
    url(r"^update/(?P<task_id>[0-9]+)$", views.update_task, name="update_task"),
    url(r"^filtered/$", views.filter_tasks, name="filter_tasks"),
]

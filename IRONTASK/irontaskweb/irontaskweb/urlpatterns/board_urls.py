from django.conf.urls import url
from irontaskweb import views

urlpatterns = [
    url(r"^all/$", views.show_boards, name="show_boards"),
    url(r"^create/$", views.add_board, name="add_board"),
    url(r"^details/(?P<board_id>[0-9]+)$", views.show_board, name="show_board"),
    url(r"^update/(?P<board_id>[0-9]+)$", views.update_board, name="update_board"),
    # url(r"^remove/(?P<board_id>[0-9]+)$", views.remove_board, name="remove_board")
]

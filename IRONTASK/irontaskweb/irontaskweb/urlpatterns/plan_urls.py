from django.conf.urls import url
from irontaskweb import views

urlpatterns = [
    url(r"^all/$", views.show_plans, name="show_plans"),
    url(r"^create/$", views.add_plan, name="add_plan"),
    url(r"^details/(?P<plan_id>[0-9]+)$", views.show_plan, name="show_plan"),
    url(r"^update/(?P<plan_id>[0-9]+)$", views.update_plan, name="update_plan"),
    # url(r"^remove/(?P<plan_id>[0-9]+)$", views.remove_plan, name="remove_plan")
]

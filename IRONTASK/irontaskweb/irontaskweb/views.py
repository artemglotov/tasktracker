from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.urls import reverse

from irontaskweb import get_service
from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.model.enums import Periodicity
from irontasklib.services import exceptions as ex
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from collections import namedtuple
from irontaskweb.forms import (TaskForm, BoardForm, PlanForm)


def registration(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password2']

            user = authenticate(username=username, password=password)
            auth_login(request, user)
            get_service().add_user(model.User(login=user.username, user_id=user.id))
            return HttpResponseRedirect(reverse('irontask:home'))
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request=request, template_name='profile/registration.html', context=context)


def login(request):
    context = {}
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect(reverse('irontask:home'))
            else:
                context['message'] = 'Invalid login'
        except Exception:
            context['message'] = 'Unknown error'
    return render(request=request, template_name='profile/login.html', context=context)


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('irontask:login'))


@login_required(login_url='/login/')
def home(request):
    user_id = request.user.id
    tasks = get_service().get_user_tasks(current_user_id=user_id,
                                         statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value,
                                                   enum.TaskStatus.REQUEST.value, enum.TaskStatus.COMPLETED.value,
                                                   enum.TaskStatus.FAILED.value])
    boards = get_service().get_user_boards(current_user_id=user_id, status=enum.BoardStatus.OPENED.value)
    plans = [get_service().get_plan_view(plan.plan_id, current_user_id=user_id)
             for plan in get_service().get_user_plans(current_user_id=user_id)]
    messages = [get_service().get_message_view(message.message_id, current_user_id=user_id) for message in get_service().get_user_messages(current_user_id=user_id)]
    messages = sorted(messages, key=lambda message: message.message.date, reverse=True)
    context = {'tasks': tasks, 'boards': boards, 'plans': plans, 'messages': messages}
    return render(request, 'home.html', context=context)


@login_required(login_url='/login/')
def show_task(request, task_id):
    user_id = request.user.id

    try:
        task_view = get_service().get_task_view(task_id, current_user_id=user_id)
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page403(str(e))

    messages = [get_service().get_message_view(message.message_id, current_user_id=user_id)
                for message in task_view.messages]
    task_view.messages = sorted(messages, key=lambda message: message.message.date, reverse=True)

    task_view.subtasks = []
    TaskTuple = namedtuple('TaskTuple', ['task', 'relation'])
    for relation in task_view.child_relations:
        related_task = get_service().get_task(relation.child_id, current_user_id=user_id)
        task_view.subtasks.append(TaskTuple(related_task, relation))

    return render(request, 'task/details.html', context=vars(task_view))


@login_required(login_url='/login/')
def show_tasks(request):
    user_id = request.user.id

    tasks = get_service().get_user_tasks(current_user_id=user_id,
                                         statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value,
                                                   enum.TaskStatus.REQUEST.value, enum.TaskStatus.COMPLETED.value,
                                                   enum.TaskStatus.FAILED.value])
    return render(request, 'task/tasks.html', context={'tasks': tasks})


@login_required(login_url='/login/')
def filter_tasks(request):
    user_id = request.user.id

    term = request.POST['search_term']
    tags = term.split(',')

    filtered_tasks = get_service().get_tasks_by_tags(tags=tags, current_user_id=user_id)
    return render(request, 'task/tasks.html', context={'tasks': filtered_tasks})


@login_required(login_url='/login/')
def add_task(request):
    user_id = request.user.id
    if request.method == 'GET':
        form = TaskForm()
    else:
        form = TaskForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data

            task = model.Task(title=arg_dict[model.TASK_TITLE], description=arg_dict[model.TASK_DESCRIPTION],
                              priority=arg_dict[model.TASK_PRIORITY], status=arg_dict[model.TASK_STATUS],
                              board_id=arg_dict[model.TASK_BOARD_ID])
            task = get_service().add_task(task, current_user_id=user_id)

            for date_type, date_type_key in [(enum.TaskDateType.START.value, model.TASK_DATE_START),
                                             (enum.TaskDateType.END.value, model.TASK_DATE_END),
                                             (enum.TaskDateType.EVENT.value, model.TASK_DATE_EVENT)]:
                if form.cleaned_data[date_type_key]:
                    get_service().add_task_date(model.TaskDate(form.cleaned_data[date_type_key].replace(tzinfo=None),
                                                               date_type, task.task_id),
                                                current_user_id=user_id)

            new_tags = [model.Tag(task.task_id, tag) for tag in form.cleaned_data[model.TAGS].split(',')]
            get_service().add_tags(new_tags, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_task', args=[task.task_id]))
    context = {'form': form, 'is_creation': True}
    return render(request=request, template_name="task/edit.html", context=context)


@login_required(login_url='/login/')
def update_task(request, task_id):
    user_id = request.user.id
    try:
        task = get_service().get_task(task_id, current_user_id=user_id)
        dates = get_service().get_task_dates_filtered(date_types=[enum.TaskDateType.START.value,
                                                                  enum.TaskDateType.END.value,
                                                                  enum.TaskDateType.EVENT.value],
                                                      task_id=task.task_id, current_user_id=user_id)
        board = get_service().get_board(task.board_id, current_user_id=user_id) if task.board_id else None
        start_date = next(iter([date for date in dates if date.date_type == enum.TaskDateType.START.value] or []), None)
        end_date = next(iter([date for date in dates if date.date_type == enum.TaskDateType.END.value] or []), None)
        event_date = next(iter([date for date in dates if date.date_type == enum.TaskDateType.EVENT.value] or []), None)
        tags = get_service().get_task_tags(task_id=task_id, current_user_id=user_id)

    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page404(str(e))

    if request.method == 'GET':
        initial = {
            model.TASK_TITLE: task.title,
            model.TASK_STATUS: task.status,
            model.TASK_PRIORITY: task.priority,
            model.TASK_DESCRIPTION: task.description,
            'date_type': 2 if event_date else 1 if start_date or end_date else 0,
            model.TASK_DATE_START: format(start_date.date, '%Y-%m-%dT%H:%M') if start_date else None,
            model.TASK_DATE_END: format(end_date.date, '%Y-%m-%dT%H:%M') if end_date else None,
            model.TASK_DATE_EVENT: format(event_date.date, '%Y-%m-%dT%H:%M') if event_date else None,
            model.TAGS: ','.join([tag.text for tag in tags]),
            model.TASK_BOARD_ID: task.board_id,
            'board_title': board.title if board else None
        }
        form = TaskForm(initial=initial)
    else:
        form = TaskForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data

            for key, value in arg_dict.items():
                if key in dir(model.Task):
                    setattr(task, key, value)

            get_service().update_task(task, current_user_id=user_id)

            for date in dates:
                if date:
                    get_service().remove_task_date(date.task_date_id, current_user_id=user_id)

            for date_type, date_type_key in [(enum.TaskDateType.START.value, model.TASK_DATE_START),
                                             (enum.TaskDateType.END.value, model.TASK_DATE_END),
                                             (enum.TaskDateType.EVENT.value, model.TASK_DATE_EVENT)]:
                if form.cleaned_data[date_type_key]:
                    get_service().add_task_date(model.TaskDate(form.cleaned_data[date_type_key].replace(tzinfo=None),
                                                               date_type, task_id),
                                                current_user_id=user_id)

            for tag in tags:
                get_service().remove_tag(tag.tag_id, current_user_id=user_id)

            new_tags = [model.Tag(task_id, tag) for tag in form.cleaned_data[model.TAGS].split(',')]
            get_service().add_tags(new_tags, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_task', args=[task_id]))

    context = {'task_id': task_id, 'form': form}
    return render(request, 'task/edit.html', context=context)


@login_required(login_url='/login/')
def show_board(request, board_id):
    user_id = request.user.id
    board_view = get_service().get_board_view(board_id, current_user_id=user_id)
    messages = [get_service().get_message_view(message.message_id, current_user_id=user_id)
                for message in board_view.messages]
    board_view.messages = sorted(messages, key=lambda message: message.message.date, reverse=True)

    task_dates = []
    for date in board_view.dates:
        task_date = get_service().get_task_date_view(date.task_date_id, current_user_id=user_id)
        task_dates.append(task_date)
    board_view.dates = sorted(task_dates, key=lambda t_date: t_date.task_date.date)
    return render(request, 'board/details.html', context=vars(board_view))


@login_required(login_url='/login/')
def show_boards(request):
    user_id = request.user.id
    boards = get_service().get_user_boards(current_user_id=user_id, status=enum.BoardStatus.OPENED.value)
    return render(request, 'board/boards.html', context={'boards': boards})


@login_required(login_url='/login/')
def add_board(request):
    user_id = request.user.id
    if request.method == 'GET':
        form = BoardForm()
    else:
        form = BoardForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data
            board = model.Board(title=arg_dict[model.BOARD_TITLE], description=arg_dict[model.BOARD_DESCRIPTION],
                                board_type=arg_dict[model.BOARD_TYPE])
            board = get_service().add_board(board, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_board', args=[board.board_id]))
    context = {'form': form, 'is_creation': True}
    return render(request=request, template_name="board/edit.html", context=context)


@login_required(login_url='/login/')
def update_board(request, board_id):
    user_id = request.user.id

    try:
        board = get_service().get_board(board_id, current_user_id=user_id)
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page404(str(e))

    if request.method == 'GET':
        initial = {
            model.BOARD_TITLE: board.title,
            model.BOARD_STATUS: board.status,
            model.BOARD_DESCRIPTION: board.description,
            model.BOARD_TYPE: board.board_type
        }
        form = BoardForm(initial=initial)
    else:
        form = BoardForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data

            for key, value in arg_dict.items():
                if key in dir(model.Board):
                    setattr(board, key, value)

            get_service().update_board(board, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_board', args=[board_id]))

    context = {'board_id': board_id, 'form': form}
    return render(request, 'board/edit.html', context=context)


@login_required(login_url='/login/')
def show_plan(request, plan_id):
    user_id = request.user.id
    plan_view = get_service().get_plan_view(plan_id, current_user_id=user_id)

    periodicity = ', '.join(['{} {}'.format(plan_view.plan.periodicity[period.value], period.value)
                             for period in Periodicity
                             if plan_view.plan.periodicity.get(period.value, None)])

    plan_view.plan.periodicity = periodicity
    if plan_view.plan.count_iteration and plan_view.plan.current_iteration is None:
        plan_view.plan.current_iteration = 0

    return render(request, 'plan/details.html', context=vars(plan_view))


@login_required(login_url='/login/')
def show_plans(request):
    user_id = request.user.id
    plans = [get_service().get_plan_view(plan.plan_id, current_user_id=user_id)
             for plan in get_service().get_user_plans(current_user_id=user_id)]
    return render(request, 'plan/plans.html', context={'plans': plans})


@login_required(login_url='/login/')
def add_plan(request):
    user_id = request.user.id
    if request.method == 'GET':
        form = PlanForm()
    else:
        form = PlanForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data
            if not arg_dict[model.TASK_ID] and arg_dict[model.TASK_TITLE]:
                task = model.Task(title=arg_dict[model.TASK_TITLE], description=arg_dict[model.TASK_DESCRIPTION],
                                  priority=arg_dict[model.TASK_PRIORITY], status=enum.TaskStatus.TEMPLATE.value,
                                  board_id=arg_dict[model.TASK_BOARD_ID])

                task = get_service().add_task(task, current_user_id=user_id)

                new_tags = [model.Tag(task.task_id, tag) for tag in form.cleaned_data[model.TAGS].split(',')]
                get_service().add_tags(new_tags, current_user_id=user_id)
                task_id = task.task_id
            else:
                task_id = arg_dict[model.TASK_ID]

            plan = model.Plan(task_id=task_id, count_iteration=arg_dict[model.PLAN_COUNT_ITERATION],
                              start_date=arg_dict[model.PLAN_START], end_date=arg_dict[model.PLAN_END],
                              end_type=arg_dict[model.PLAN_END_TYPE], periodicity=arg_dict[model.PLAN_PERIODICITY])
            import pdb
            pdb.set_trace()
            plan = get_service().add_plan(plan, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_plan', args=[plan.plan_id]))

    context = {'form': form, 'is_creation': True}
    return render(request=request, template_name="plan/edit.html", context=context)


@login_required(login_url='/login/')
def update_plan(request, plan_id):
    user_id = request.user.id
    try:
        plan = get_service().get_plan(plan_id, current_user_id=user_id)
        task = get_service().get_task(plan.task_id, current_user_id=user_id)
        tags = get_service().get_task_tags(task_id=task.task_id, current_user_id=user_id)
        board = get_service().get_board(task.board_id) if task.board_id else None
    except ex.NotFoundError as e:
        return page404(str(e))
    except ex.AuthorizationError as e:
        return page404(str(e))

    if request.method == 'GET':
        periodicity = ''
        if plan.periodicity:
            periodicity += ('{}Y '.format(plan.periodicity[Periodicity.YEARS.value])
                            if plan.periodicity[Periodicity.YEARS.value] else '')
            periodicity += ('{}M '.format(plan.periodicity[Periodicity.MONTHS.value])
                            if plan.periodicity[Periodicity.MONTHS.value] else '')
            periodicity += ('{}D '.format(plan.periodicity[Periodicity.DAYS.value])
                            if plan.periodicity[Periodicity.DAYS.value] else '')
            periodicity += ('{}h '.format(plan.periodicity[Periodicity.HOURS.value])
                            if plan.periodicity[Periodicity.HOURS.value] else '')
            periodicity += ('{}m '.format(plan.periodicity[Periodicity.MINUTES.value])
                            if plan.periodicity[Periodicity.MINUTES.value] else '')

        initial = {
            model.PLAN_COUNT_ITERATION: plan.count_iteration,
            model.PLAN_START: format(plan.start_date, '%Y-%m-%dT%H:%M') if plan.start_date else None,
            model.PLAN_END: format(plan.end_date, '%Y-%m-%dT%H:%M') if plan.end_date else None,
            model.PLAN_STATUS: plan.status,
            model.PLAN_END_TYPE: plan.end_type,
            model.TASK_TITLE: task.title,
            model.TASK_DESCRIPTION: task.description,
            model.TASK_PRIORITY: task.priority,
            model.TASK_BOARD_ID: task.board_id,
            model.PLAN_PERIODICITY: periodicity,
            'board_title': board.title if board else None,
            model.TAGS: ','.join([tag.text for tag in tags]),
            'task_type': 1
        }

        form = PlanForm(initial=initial)
    else:
        form = PlanForm(request.POST)
        if form.is_valid():
            arg_dict = form.cleaned_data
            form.cleaned_data[model.TASK_ID] = task.task_id
            for key, value in arg_dict.items():
                if key in dir(model.Task):
                    setattr(task, key, value)

            get_service().update_task(task, current_user_id=user_id)

            for key, value in arg_dict.items():
                if key in dir(model.Plan):
                    setattr(plan, key, value)

            get_service().update_plan(plan, current_user_id=user_id)

            for tag in tags:
                get_service().remove_tag(tag.tag_id, current_user_id=user_id)

            new_tags = [model.Tag(task.task_id, tag) for tag in form.cleaned_data[model.TAGS].split(',')]
            get_service().add_tags(new_tags, current_user_id=user_id)

            return HttpResponseRedirect(reverse('irontask:show_plan', args=[plan.plan_id]))

    context = {'plan_id': plan_id, 'form': form}
    return render(request=request, template_name="plan/edit.html", context=context)


def page404(message=None):
    context = {}
    if message:
        context["message"] = message
    response = render_to_response('errors/404.html', context=context, status=404)
    return response


def page403(message=None):
    context = {}
    if message:
        context["message"] = message
    response = render_to_response('errors/403.html', context=context, status=404)
    return response

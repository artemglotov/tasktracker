from django import forms
import re
from irontasklib.model import enums as enum
from irontasklib.model import models as model
from irontasklib.model.enums import Periodicity
from django.forms import ValidationError
from irontaskweb.utilities import get_enum_choices

periodicity_regex = '((?P<years>\d+)Y )?((?P<months>\d+)M )?((?P<days>\d+)D )?((?P<hours>\d+)h )?((?P<minutes>\d+)m )?'


class TaskForm(forms.Form):
    title = forms.CharField(max_length=50, required=True)
    status = forms.ChoiceField(choices=get_enum_choices(enum.TaskStatus),
                               required=False)
    priority = forms.ChoiceField(choices=get_enum_choices(enum.TaskPriority),
                                 initial=enum.TaskPriority.NORMAL.value)
    description = forms.CharField(widget=forms.Textarea, required=False, max_length=500)

    start_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=False,
                                     input_formats=['%Y-%m-%dT%H:%M'], localize=False)
    end_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=False,
                                   input_formats=['%Y-%m-%dT%H:%M'], localize=False)
    event_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=False,
                                     input_formats=['%Y-%m-%dT%H:%M'], localize=False)

    date_type = forms.ChoiceField(choices=[(0, "Without dates"), (1, "Start-end dates"), (2, "Event date")],
                                  initial=0, widget=forms.RadioSelect())
    tags = forms.CharField(widget=forms.TextInput(attrs={'data-role': 'tagsinput'}), required=False)

    board_title = forms.CharField(required=False)

    board_id = forms.IntegerField(initial=None, required=False)

    def clean_start_date(self):
        form_data = self.data
        start_date = form_data.get(model.TASK_DATE_START, None)
        end_date = form_data.get(model.TASK_DATE_END, None)

        if not start_date and end_date:
            raise ValidationError("If you specify end date you must specify start date")

        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")

        return self.cleaned_data.get(model.TASK_DATE_START, None)

    def clean_end_date(self):
        form_data = self.data
        start_date = form_data.get(model.TASK_DATE_START, None)
        end_date = form_data.get(model.TASK_DATE_END, None)

        if start_date and not end_date:
            raise ValidationError("If you specify end date you must specify start date")

        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")

        return self.cleaned_data.get(model.TASK_DATE_END, None)


class BoardForm(forms.Form):
    title = forms.CharField(max_length=50, required=True)

    description = forms.CharField(widget=forms.Textarea, required=False, max_length=500)
    status = forms.ChoiceField(choices=get_enum_choices(enum.BoardStatus),
                               required=False, initial=enum.BoardStatus.OPENED.value)
    board_type = forms.ChoiceField(choices=get_enum_choices(enum.BoardType),
                                   required=False, initial=enum.BoardType.SINGLE.value)


class PlanForm(forms.Form):
    title = forms.CharField(max_length=50, required=False)

    priority = forms.ChoiceField(choices=get_enum_choices(enum.TaskPriority),
                                 initial=enum.TaskPriority.NORMAL.value)
    description = forms.CharField(widget=forms.Textarea, initial=None, required=False, max_length=500)

    tags = forms.CharField(widget=forms.TextInput(attrs={'data-role': 'tagsinput'}), required=False)

    periodicity = forms.CharField(widget=forms.TextInput(
        attrs={'title': '1Y - 1 year\n4M - 4 month\n3D - 3 days\n12h - 12 hours\n7m - 7 minutes',
               'placeholder': 'Write period like \'#Y #M #m\' or \'#M #D #h #m\''}),
        initial=None, required=False
    )

    end_type = forms.ChoiceField(choices=get_enum_choices(enum.PlanEndType), required=True,
                                 initial=enum.PlanEndType.NEVER.value, widget=forms.RadioSelect())
    status = forms.ChoiceField(choices=get_enum_choices(enum.PlanStatus),
                               required=False, initial=enum.PlanStatus.ON.value)
    start_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=True,
                                     input_formats=['%Y-%m-%dT%H:%M'], localize=False)
    end_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=False,
                                   input_formats=['%Y-%m-%dT%H:%M'], localize=False)
    count_iteration = forms.IntegerField(required=False, min_value=1)

    task_type = forms.ChoiceField(choices=[(0, "Existing task"), (1, "New task")], required=False,
                                  widget=forms.RadioSelect(), initial=0)

    board_title = forms.CharField(required=False)

    board_id = forms.IntegerField(initial=None, required=False)

    task_title = forms.CharField(required=False)

    task_id = forms.IntegerField(initial=None, required=False)

    def clean_periodicity(self):
        periodicity = self.cleaned_data[model.PLAN_PERIODICITY]
        periodicity = periodicity.strip() + ' '
        pattern = re.compile(periodicity_regex)
        match = pattern.match(periodicity)
        if match:
            result = match.groupdict()
            return result
        else:
            raise ValidationError('Period format needs to be "##Y ##M ##D ##h ##m"')

    def clean_end_type(self):
        end_type = self.cleaned_data.get(model.PLAN_END_TYPE)
        return int(end_type)

    # def clean_interval(self):
    #     interval = self.cleaned_data.get(model.PLAN_INTERVAL)
    #     return int(interval)

    def clean_count_iteration(self):
        count_iteration = self.cleaned_data.get(model.PLAN_COUNT_ITERATION)
        if count_iteration:
            return int(count_iteration)
        return count_iteration

    def clean_task_id(self):
        form_data = self.cleaned_data
        task_id = form_data.get(model.TASK_ID, None)
        title = form_data.get(model.TASK_TITLE, None)
        if not task_id and not title:
            raise ValidationError("You must specify existing task or add new.")
        return task_id

    def clean_task_title(self):
        form_data = self.cleaned_data
        task_id = form_data.get(model.TASK_ID, None)
        title = form_data.get(model.TASK_TITLE, None)
        if not task_id and not title:
            raise ValidationError("You must specify existing task or add new.")
        return

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get(model.PLAN_START, None)
        end_date = form_data.get(model.PLAN_END, None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get(model.PLAN_START, None)
        end_date = form_data.get(model.PLAN_END, None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("Start date can't be later than end date")
        return end_date

import os
from setuptools import setup, find_packages

setup(
    name="irontaskterminal",
    version="0.2.0",
    author="Glotov Artem",
    author_email="glotovartemalex@gmail.com",
    description="Console version for IronTask",
    packages=find_packages(),
    entry_points={
        "console_scripts":
            ["iron = irontaskterminal.main:main"]
    }
)
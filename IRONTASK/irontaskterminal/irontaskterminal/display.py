from irontasklib.model import enums as enum
from irontasklib.model import models as model
from irontaskterminal.representation import value_to_str_translator as to_str

from irontasklib.model.enums import Periodicity


def show_task(task, board=None, creator=None, executors=None, observers=None, requester=None, finisher=None,
              date_create=None, date_event=None, date_start=None, date_end=None, date_close=None, messages=None,
              reminders=None, tags=None, header=None, short=False, parent_relations=None, child_relations=None,
              dates_edit=None):
    if task is None:
        return

    if header:
        print(header)

    task_display_model = model.get_display_model(task, to_str.task_attributes)

    if short:
        print("{task_id} - {title} ({status})".format(**task_display_model))
    else:
        print("{title}\n"
              "Description: {description}\n"
              "Task id: {task_id}\n"
              "Status: {status}\n"
              "Priority: {priority}"
              .format(**task_display_model))

        if board:
            board_display_model = model.get_display_model(board, to_str.board_attributes)
            print("Board: {title} (id: {board_id})".format(**board_display_model))

        if date_create:
            date_create_display_model = model.get_display_model(date_create, to_str.task_date_attributes)
            print("Created on:\n"
                  "        {date} (id: {task_date_id})".format(**date_create_display_model))

        if date_event:
            date_event_display_model = model.get_display_model(date_event, to_str.task_date_attributes)
            print("Event date:\n"
                  "        {date} (id: {task_date_id})".format(**date_event_display_model))

        if dates_edit:
            print("Edited on:")
            for date_edit in dates_edit:
                date_edit_display_model = model.get_display_model(date_edit, to_str.task_date_attributes)
                print("        {date} (id: {task_date_id})".format(**date_edit_display_model))

        if date_start:
            date_start_display_model = model.get_display_model(date_start, to_str.task_date_attributes)
            print("From:\n"
                  "        {date} (id: {task_date_id})".format(**date_start_display_model))

        if date_end:
            date_end_display_model = model.get_display_model(date_end, to_str.task_date_attributes)
            print("To:\n"
                  "        {date} (id: {task_date_id})".format(**date_end_display_model))

        if date_close:
            date_close_display_model = model.get_display_model(date_close, to_str.task_date_attributes)
            print("Closed on:\n"
                  "        {date} (id: {task_date_id})".format(**date_close_display_model))

        if tags:
            print("Tags: {tags}".format(tags=", ".join([tag.text for tag in tags])))

        if creator:
            creator_display_model = model.get_display_model(creator, to_str.user_attributes)
            print("Creator:\n"
                  "        '{login}' (id: {user_id})".format(**creator_display_model))

        if executors:
            print("Executor:" if len(executors) == 1 else "Executors:")
            for executor in executors:
                executor_display_model = model.get_display_model(executor, to_str.user_attributes)
                print("        '{login}' (id: {user_id})".format(**executor_display_model))

        if observers:
            print("Observer:" if len(observers) == 1 else "Observers:")
            for observer in observers:
                observer_display_model = model.get_display_model(observer, to_str.user_attributes)
                print("        '{login}' (id: {user_id})".format(**observer_display_model))

        if requester:
            requester_display_model = model.get_display_model(requester, to_str.user_attributes)
            print("Requester:\n"
                  "        '{login}' (id: {user_id})".format(**requester_display_model))

        if finisher:
            finisher_display_model = model.get_display_model(finisher, to_str.user_attributes)
            print("Finisher:\n"
                  "        '{login}' (id: {user_id})".format(**finisher_display_model))

        if messages:
            print("Messages:")
            for message in messages:
                message_display_model = model.get_display_model(message, to_str.message_attributes)
                print("        user {user_id}: {text} (id: {message_id})".format(**message_display_model))

        if reminders:
            print("Reminders:")
            for reminder in reminders:
                reminder_display_model = model.get_display_model(reminder, to_str.message_attributes)
                print("        '{text}' (id: {reminder_id})".format(**reminder_display_model))

        if parent_relations:
            print("Child task for :")
            for relation in parent_relations:
                relation_display_model = model.get_display_model(relation, to_str.task_relation_attributes)
                print("        task {parent_id} | {relation_type} (id: {relation_id})".format(**relation_display_model))

        if child_relations:
            print("Parent task for :")
            for relation in child_relations:
                relation_display_model = model.get_display_model(relation, to_str.task_relation_attributes)
                print("        task {child_id} | {relation_type} (id: {relation_id})".format(**relation_display_model))


def show_plan(plan, task=None, board=None, creator=None, executors=None, observers=None, date_create=None, tags=None,
              header=None, short=False):
    if plan is None:
        return

    if header:
        print(header)

    plan_display_model = model.get_display_model(plan, to_str.plan_attributes)

    periodicity = ', '.join(['{} {}'.format(plan.periodicity[period.value], period.value)
                             for period in Periodicity
                             if plan.periodicity.get(period.value, None)])

    if short:
        print("{plan_id} - repeat task id: {task_id} (status)".format(**plan_display_model))
    else:
        print("Plan id: {plan_id}\n"
              "Status: {status}\n"
              "Start repetition: {start_date}"
              .format(**plan_display_model))
        print("Repeat every {periodicity}".format(periodicity=periodicity))

        if plan.end_type == enum.PlanEndType.NUMBER.value:
            if plan.current_iteration is None:
                plan.current_iteration = 0
            print("Repeated: {current_iteration}/{count_iteration} times".format(**plan_display_model))
        elif plan.end_type == enum.PlanEndType.DATE.value:
            print("End repetition: {end_date}".format(**plan_display_model))

        if task:
            task_display_model = model.get_display_model(task, to_str.task_attributes)
            print("Template task:\n"
                  "        {title}\n"
                  "        Description: {description}\n"
                  "        Task id: {task_id}\n"
                  "        Board id: {board_id}\n"
                  "        Priority: {priority}"
                  .format(**task_display_model))


def show_board(board, creator=None, header=None, admins=None, editors=None, readers=None, blocked=None, short=False):
    if board is None:
        return

    if header:
        print(header)

    board_display_model = model.get_display_model(board, to_str.board_attributes)

    if short:
        print("{board_id} - {title} ({status})".format(**board_display_model))
    else:
        print("{title}\n"
              "Description: {description}\n"
              "Board id: {board_id}\n"
              "Type: {board_type}\n"
              "Status: {status}".format(**board_display_model))

        if creator:
            creator_display_model = model.get_display_model(creator, to_str.user_attributes)
            print("Creator: {login} (id: {user_id})".format(**creator_display_model))

        if admins:
            print("Admins:")
            for admin in admins:
                user_display_model = model.get_display_model(admin, to_str.user_attributes)
                print("        {login} (id: {user_id})".format(**user_display_model))
        if editors:
            print("Editors:")
            for editor in editors:
                user_display_model = model.get_display_model(editor, to_str.user_attributes)
                print("        {login} (id: {user_id})".format(**user_display_model))
        if readers:
            print("Readers:")
            for reader in readers:
                user_display_model = model.get_display_model(reader, to_str.user_attributes)
                print("        {login} (id: {user_id})".format(**user_display_model))
        if blocked:
            print("Blocked:")
            for user in blocked:
                user_display_model = model.get_display_model(user, to_str.user_attributes)
                print("        {login} (id: {user_id})".format(**user_display_model))


def show_task_date(task_date, task=None, header=None, short=False):
    if task_date is None:
        return

    if header:
        print(header)

    task_date_display_model = model.get_display_model(task_date, to_str.task_date_attributes)

    if short:
        print("{task_date_id} - {date} ({date_type})".format(**task_date_display_model))
    else:
        print("Task date id: {task_date_id}\n"
              "Type of date: {date_type}\n"
              "Date: {date}".format(**task_date_display_model))

        if task:
            task_display_model = model.get_display_model(task, to_str.task_attributes)
            print("Connected with task '{title}' (id: {task_id})".format(**task_display_model))


def show_reminder(reminder, task=None, header=None, short=None):
    if reminder is None:
        return

    if header:
        print(header)

    reminder_display_model = model.get_display_model(reminder, to_str.reminder_attributes)

    if short:
        print("{reminder_id} - reminder of task id: {task_id} ({status})".format(**reminder_display_model))
    else:
        print("Reminder id: {reminder_id}\n"
              "Status: {status}\n"
              "Notification text: {text}".format(**reminder_display_model))

        if task:
            task_display_model = model.get_display_model(task, to_str.task_attributes)
            print("Reminder of task '{title}' (id: {task_id})".format(**task_display_model))

        if not reminder.periodicity:
            print("Date: {start_date}".format(**reminder_display_model))
        else:
            periodicity = ', '.join(['{} {}'.format(reminder.periodicity[period.value], period.value)
                                     for period in Periodicity
                                     if reminder.periodicity.get(period.value, None)])
            print("Start repetition: {start_date}"
                  .format(**reminder_display_model))
            print("Repeat every {periodicity}".format(periodicity=periodicity))

            if reminder.end_type == enum.ReminderEndType.DATE.value:
                print("End repetition: {end_date}".format(**reminder_display_model))
            if reminder.end_type == enum.ReminderEndType.NUMBER.value and reminder.count_iteration:
                if reminder.current_iteration is None:
                    reminder.current_iteration = 0
                print("Repeated: {current_iteration}/{count_iteration} times".format(**reminder_display_model))


def show_task_relation(relation, parent_task=None, child_task=None, header=None, short=None):
    if relation is None:
        return

    if header:
        print(header)

    relation_display_model = model.get_display_model(relation, to_str.task_relation_attributes)
    if short:
        if relation.realtion_type == enum.TaskRelationType.SUBTASK.value:
            print("{relation_id} - task {child_id} is subtask of task {parent_id}"
                  .format(**relation_display_model))
        elif relation.realtion_type == enum.TaskRelationType.COMPLETE.value:
            print("{relation_id} - task {child_id} will be completed if task {parent_id} completes"
                  .format(**relation_display_model))
        elif relation.realtion_type == enum.TaskRelationType.BLOCK.value:
            print("{relation_id} - task {child_id} can't be completed until task {parent_id} completes"
                  .format(**relation_display_model))
    else:
        print("Relation id: {relation_id}\n"
              "Relation type: {relation_type}".format(**relation_display_model))

        parent_task_display_model = model.get_display_model(parent_task, to_str.task_attributes)
        print("Parent task:\n"
              "        id: {task_id}\n"
              "        title: {title}\n"
              "        description: {description}".format(**parent_task_display_model))

        child_task_display_model = model.get_display_model(child_task, to_str.task_attributes)
        print("Child task:\n"
              "        id: {task_id}\n"
              "        title: {title}\n"
              "        description: {description}".format(**child_task_display_model))


def show_task_user_relation(relations, task=None, user=None, header=None, short=None):
    if relations is None:
        return

    if header:
        print(header)

    if short:
        roles = (to_str.task_user_relation_attributes["role"](relation.role) for relation in relations)
        print(("User id {user_id} is " +
               ", ".join(roles) +
               " for the task id {task_id}").format(user_id=relations[0].user_id, task_id=relations[0].task_id))
    else:
        user_display_model = model.get_display_model(user, to_str.user_attributes)
        print("User '{login}' (id: {user_id}) is:".format(**user_display_model))

        for relation in relations:
            relation_display_model = model.get_display_model(relation, to_str.task_user_relation_attributes)
            print("        {role} (relation id: {relation_id})".format(**relation_display_model))

        task_display_model = model.get_display_model(task, to_str.task_attributes)
        print("for the task:\n"
              "        {title}\n"
              "        Description: {description}\n"
              "        Task id: {task_id}\n"
              "        Status: {status}\n".format(**task_display_model))


def show_user_board_relation(relation, user=None, board=None, header=None, short=None):
    if relation is None:
        return

    if header:
        print(header)

    relation_display_model = model.get_display_model(relation, to_str.user_board_relation_attributes)
    if short:
        print("User id {user_id} has '{permission}' permissions"
              " for the board id {board_id} (relation id: {relation_id})".format(**relation_display_model))
    else:
        user_display_model = model.get_display_model(user, to_str.user_attributes)
        print("User '{login}' (id: {user_id}) has:".format(**user_display_model))

        print("        {permission} permission (id: {relation_id})".format(**relation_display_model))

        board_display_model = model.get_display_model(board, to_str.board_attributes)
        print("for the board:\n"
              "        {title}\n"
              "        Description: {description}\n"
              "        Board id: {board_id}\n"
              "        Type: {board_type}\n"
              "        Status: {status}".format(**board_display_model))


def show_message(message, task=None, user=None, header=None, short=None):
    if message is None:
        return

    if header:
        print(header)

    message_display_model = model.get_display_model(message, to_str.message_attributes)
    if message.text is None:
        message.text = "'Take your attention'"
    if short:
        if message.user_id != '-':
            print("{date}: user {user_id} on task {task_id} - {text} (id: {message_id})"
                  .format(**message_display_model))
        else:
            print("{date}: reminder of task {task_id} - {text} (id: {message_id})"
                  .format(**message_display_model))
    else:
        if user:
            user_display_model = model.get_display_model(user, to_str.user_attributes)
            print("User '{login}' (id: {user_id}) add text: ".format(**user_display_model))

        else:
            print("Reminder's message:")
        print("              {text} (id: {message_id})".format(**message_display_model))
        task_display_model = model.get_display_model(task, to_str.task_attributes)
        print("for the task:\n"
              "        {title}\n"
              "        Description: {description}\n"
              "        Task id: {task_id}\n"
              "        Status: {status}\n".format(**task_display_model))


def show_tree(tree_view):
    def show_tree_node(node, level):
        task_display_model = model.get_display_model(node.task, to_str.task_attributes)
        tabs = "{tabs}".format(tabs="".join(["\t"]*level))
        task = "| {title} - {status} (id: {task_id})".format(**task_display_model)
        complete_tasks = (" (c: {})".format(",".join(str(task_id) for task_id in node.complete_task_ids))
                          if node.complete_task_ids else "")
        block_tasks = (" (b: {})".format(",".join(str(task_id) for task_id in node.block_task_ids))
                          if node.block_task_ids else "")

        print(tabs + task + complete_tasks + block_tasks + " |")

        for child_task in node.child_tasks:
            show_tree_node(child_task, level + 1)

    show_tree_node(tree_view, 0)

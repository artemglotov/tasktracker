from irontasklib.model import enums as enum
from irontasklib.model import models as model


def get_arguments(value_translator, namespace):
    return {key: value_translator[key](value) for key, value in namespace.items()
            if key in value_translator and value is not None}


task_attributes = {
    model.TASK_TITLE: lambda title: title,
    model.TASK_DESCRIPTION: lambda description: description,
    model.TASK_PRIORITY: lambda priority: enum.TaskPriority[priority.upper()].value,
    model.TASK_STATUS: lambda status: enum.TaskStatus[status.upper()].value,
    model.TASK_BOARD_ID: lambda board_id: board_id
}

tasks_attributes = {
    model.TASK_PRIORITY: lambda priority: enum.TaskPriority[priority.upper()].value,
    model.TASK_STATUS: lambda status: enum.TaskStatus[status.upper()].value,
    model.TASK_USER_RELATION_ROLE: lambda role: enum.Role[role.upper()].value
}

plan_attributes = {
    model.PLAN_TASK_ID: lambda task_id: task_id,
    model.PLAN_PERIODICITY: lambda periodicity: periodicity,
    model.PLAN_COUNT_ITERATION: lambda count_iteration: count_iteration,
    model.PLAN_CURRENT_ITERATION: lambda value: value,
    model.PLAN_START: lambda start_date: start_date,
    model.PLAN_END: lambda end_date: end_date,
    model.PLAN_STATUS: lambda status: enum.PlanStatus[status.upper()].value,
    model.PLAN_END_TYPE: lambda end_type: enum.PlanEndType[end_type.upper()].value
}


board_attributes = {
    model.BOARD_TITLE: lambda title: title,
    model.BOARD_DESCRIPTION: lambda description: description,
    model.BOARD_TYPE: lambda board_type: enum.BoardType[board_type.upper()].value,
    model.BOARD_STATUS: lambda status: enum.BoardStatus[status.upper()].value
}


task_date_attributes = {
    model.TASK_DATE_TASK_ID: lambda task_id: task_id,
    model.TASK_DATE_TYPE: lambda date_type: enum.TaskDateType[date_type.upper()].value,
    model.TASK_DATE: lambda date: date,
}


reminder_attributes = {
    model.REMINDER_STATUS: lambda status: enum.ReminderStatus[status.upper()].value,
    model.REMINDER_TASK_ID: lambda task_id: task_id,
    model.REMINDER_START: lambda date: date,
    model.REMINDER_PERIODICITY: lambda periodicity: periodicity,
    model.REMINDER_TEXT: lambda text: text,
    model.REMINDER_END: lambda end_date: end_date,
    model.REMINDER_COUNT_ITERATION: lambda value: value,
    model.REMINDER_CURRENT_ITERATION: lambda value: value
}


message_attributes = {
    model.MESSAGE_TEXT: lambda message: message,
    model.MESSAGE_TASK_ID: lambda task_id: task_id
}


task_relation_attributes = {
    model.TASK_RELATION_PARENT_ID: lambda parent_task_id: parent_task_id,
    model.TASK_RELATION_CHILD_ID: lambda child_task_id: child_task_id,
    model.TASK_RELATION_TYPE: lambda relation_type: enum.TaskRelationType[relation_type.upper()].value
}

task_user_relation_attributes = {
    model.TASK_USER_RELATION_USER_ID: lambda user_id: user_id,
    model.TASK_USER_RELATION_TASK_ID: lambda task_id: task_id,
    model.TASK_USER_RELATION_ROLE: lambda role: enum.Role[role.upper()].value
}

user_board_relation_attributes = {
    model.USER_BOARD_RELATION_USER_ID: lambda user_id: user_id,
    model.USER_BOARD_RELATION_BOARD_ID: lambda board_id: board_id,
    model.USER_BOARD_RELATION_PERMISSION: lambda permission: enum.BoardPermission[permission.upper()].value
}

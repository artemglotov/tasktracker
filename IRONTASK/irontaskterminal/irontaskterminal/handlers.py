from datetime import datetime

from sqlalchemy.exc import SQLAlchemyError


from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.services import exceptions as ex

from irontaskterminal import display
from irontaskterminal.config_service import ConfigMaker
from irontaskterminal.representation import value_translator as attr_to_value
from irontaskterminal.representation import constants as command


def error_catcher(func):
    def safe_func(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except SQLAlchemyError:
            print("Internal data storage error occurred while adding information to database")
        except ex.IronTaskError as e:
            print(str(e))
        except Exception:
            raise

    return safe_func


class TerminalMenu:
    def __init__(self, service):
        self.service = service

        self.objects_handlers = {
            command.TASK: self.handle_task_command,
            command.PLAN: self.handle_plan_command,
            command.BOARD: self.handle_board_command,
            command.DATE: self.handle_date_command,
            command.REMINDER: self.handle_reminder_command,
            command.RELATION: self.handle_relation_command,
            command.MESSAGE: self.handle_message_command,
            command.TAGS: self.handle_tags_command,
            command.TASKS: self.handle_show_tasks_command,
            command.PLANS: self.handle_show_plans_command,
            command.BOARDS: self.handle_show_boards_command,
            command.MESSAGES: self.handle_show_messages_command,
            command.REMINDERS: self.handle_show_reminders_command,
            command.TREE: self.handle_show_tree_command,
            command.INIT: self.handle_init_command,
            command.PROFILE: self.handle_profile_command
        }

        self.task_handlers = {
            command.ADD: self.handle_add_task_command,
            command.UPDATE: self.handle_update_task_command,
            command.SHOW: self.handle_show_task_command,
            command.REMOVE: self.handle_remove_task_command
        }

        self.plan_handlers = {
            command.ADD: self.handle_add_plan_command,
            command.UPDATE: self.handle_update_plan_command,
            command.SHOW: self.handle_show_plan_command,
            command.REMOVE: self.handle_remove_plan_command
        }

        self.board_handlers = {
            command.ADD: self.handle_add_board_command,
            command.SHOW: self.handle_show_board_command,
            command.UPDATE: self.handle_update_board_command,
            command.REMOVE: self.handle_remove_board_command
        }

        self.date_handlers = {
            command.ADD: self.handle_add_date_command,
            command.SHOW: self.handle_show_date_command,
            command.UPDATE: self.handle_update_date_command,
            command.REMOVE: self.handle_remove_date_command,
        }

        self.reminder_handlers = {
            command.ADD: self.handle_add_reminder_command,
            command.SHOW: self.handle_show_reminder_command,
            command.UPDATE: self.handle_update_reminder_command,
            command.REMOVE: self.handle_remove_reminder_command
        }

        self.relation_handlers = {
            command.TASK_RELATION: self.handle_task_relation_command,
            command.TASK_USER_RELATION: self.handle_task_user_relation_command,
            command.USER_BOARD_RELATION: self.handle_user_board_relation_command
        }

        self.message_handlers = {
            command.ADD: self.handle_add_message_command,
            command.SHOW: self.handle_show_message_command,
            command.REMOVE: self.handle_remove_message_command
        }

        self.task_relation_handlers = {
            command.ADD: self.handle_add_task_relation_command,
            command.SHOW: self.handle_show_task_relation_command,
            command.REMOVE: self.handle_remove_task_relation_command,
            command.UPDATE: self.handle_update_task_relation_command,
        }

        self.task_user_relation_handlers = {
            command.ADD: self.handle_add_task_user_relation_command,
            command.SHOW: self.handle_show_task_user_relation_command,
            command.REMOVE: self.handle_remove_task_user_relation_command
        }

        self.user_board_relation_handlers = {
            command.ADD: self.handle_add_user_board_relation_command,
            command.SHOW: self.handle_show_user_board_relation_command,
            command.REMOVE: self.handle_remove_user_board_relation_command,
            command.UPDATE: self.handle_update_user_board_relation_command,
        }

        self.profile_handlers = {
            command.REGISTER: self.handle_register_profile_command,
            command.UPDATE: self.handle_update_profile_command,
            command.REMOVE: self.handle_remove_profile_command,
            command.LOGIN: self.handle_login_profile_command,
            command.LOGOUT: self.handle_logout_profile_command
        }

        self.tags_actions = {
            command.ADD: self.handle_add_tags_command,
            command.SHOW: self.handle_show_tags_command,
            command.REMOVE: self.handle_remove_tags_command
        }

    @staticmethod
    def handle_init_command(service_class, data_storage_class, namespace):
        connection_string = namespace.connection_string
        connection_string = service_class.set_up_database(data_storage_class, connection_string)
        print("----Database was set up----")
        config = ConfigMaker("app.config")
        config.set_connection_string(connection_string)
        if namespace.login:
            data_storage = data_storage_class(connection_string=namespace.connection_string)
            service = service_class(data_storage)
            user = service.add_user(model.User(namespace.login))
            config.set_user_id(user.user_id)
            print("User was added: {} (id {})".format(user.login, user.user_id))

    @staticmethod
    @error_catcher
    def handle_command(commands, input_command, *args, **kwargs):
        if input_command in commands:
            commands[input_command](*args, **kwargs)
        else:
            raise KeyError("Could not find handler for {} command".format(input_command))

    @error_catcher
    def handle(self, namespace):
        self.handle_command(self.objects_handlers, namespace.first_command, namespace=namespace)

    # region object commands
    def handle_task_command(self, namespace, *args, **kwargs):
        self.handle_command(self.task_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_plan_command(self, namespace, *args, **kwargs):
        self.handle_command(self.plan_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_board_command(self, namespace, *args, **kwargs):
        self.handle_command(self.board_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_date_command(self, namespace, *args, **kwargs):
        self.handle_command(self.date_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_reminder_command(self, namespace, *args, **kwargs):
        self.handle_command(self.reminder_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.relation_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_message_command(self, namespace, *args, **kwargs):
        self.handle_command(self.message_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_task_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.task_relation_handlers, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    def handle_task_user_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.task_user_relation_handlers, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    def handle_user_board_relation_command(self, namespace, *args, **kwargs):
        self.handle_command(self.user_board_relation_handlers, namespace.third_command,
                            namespace=namespace, *args, **kwargs)

    def handle_profile_command(self, namespace, *args, **kwargs):
        self.handle_command(self.profile_handlers, namespace.second_command,
                            namespace=namespace, *args, **kwargs)

    def handle_tags_command(self, namespace, *args, **kwargs):
        self.handle_command(self.tags_actions, namespace.second_command,
                            namespace=namespace, *args, **kwargs)
    # endregion

    # region task commands
    @error_catcher
    def handle_add_task_command(self, namespace):
        # region add task
        args = attr_to_value.get_arguments(attr_to_value.task_attributes,
                                           vars(namespace))

        creator_id = getattr(namespace, model.CURRENT_USER_ID)

        task = model.Task(**args)

        task = self.service.add_task(task, creator_id)
        # endregion

        # region add task_dates
        start_date = (model.TaskDate(task_id=task.task_id,
                                     date_type=enum.TaskDateType.START.value,
                                     date=getattr(namespace, model.TASK_DATE_START))
                      if getattr(namespace, model.TASK_DATE_START) is not None else None)

        end_date = (model.TaskDate(task_id=task.task_id,
                                   date_type=enum.TaskDateType.END.value,
                                   date=getattr(namespace, model.TASK_DATE_END))
                    if getattr(namespace, model.TASK_DATE_END) is not None else None)

        event_date = (model.TaskDate(task_id=task.task_id,
                                     date_type=enum.TaskDateType.EVENT.value,
                                     date=getattr(namespace, model.TASK_DATE_EVENT))
                      if getattr(namespace, model.TASK_DATE_EVENT) is not None else None)

        if start_date or end_date or event_date:
            self.service.add_task_dates([start_date, end_date, event_date], creator_id)
        # endregion

        # region add tags
        if getattr(namespace, model.TAGS) is not None:
            tags = [model.Tag(task.task_id, tag) for tag in getattr(namespace, model.TAGS)]
            self.service.add_tags(tags, creator_id)
        # endregion

        # region add executors and observers
        if getattr(namespace, model.TASK_EXECUTORS) is not None:
            for executor_id in getattr(namespace, model.TASK_EXECUTORS):
                executor = model.TaskUserRelation(enum.Role.EXECUTOR, task.task_id, executor_id)
                self.service.add_task_user_relation(executor, creator_id)

        if getattr(namespace, model.TASK_OBSERVERS) is not None:
            for observer_id in getattr(namespace, model.TASK_OBSERVERS):
                observer = model.TaskUserRelation(enum.Role.OBSERVER, task.task_id, observer_id)
                self.service.add_task_user_relation(observer, creator_id)
        # endregion

        # endregion
        display.show_task(header="---- Task was created ----", **vars(self.service.get_task_view(task.task_id,
                                                                                                 creator_id)))
        return task

    @error_catcher
    def handle_update_task_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_attributes,
                                           vars(namespace))

        task = self.service.get_task(getattr(namespace, model.TASK_ID), current_user_id)

        for key, value in args.items():
            setattr(task, key, value)

        if getattr(namespace, model.TASK_WITHOUT_BOARD):
            task.board_id = None

        self.service.update_task(task, current_user_id)

        task_view = self.service.get_task_view(getattr(namespace, model.TASK_ID), current_user_id)
        display.show_task(**vars(task_view), header="---- Task was updated ----")

    @error_catcher
    def handle_show_task_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        task_view = self.service.get_task_view(getattr(namespace, model.TASK_ID), current_user_id)
        display.show_task(**vars(task_view))

    @error_catcher
    def handle_remove_task_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_task(getattr(namespace, model.TASK_ID), current_user_id) > 0:
            print("---- Task was successfully deleted ----")

    @error_catcher
    def handle_show_tasks_command(self, namespace):

        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.tasks_attributes,
                                           vars(namespace))
        if getattr(namespace, model.TASK_STATUS) is not None:
            tasks = self.service.get_user_tasks(tags=getattr(namespace, model.TAGS),
                                                statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value,
                                                          enum.TaskStatus.REQUEST.value,
                                                          enum.TaskStatus.COMPLETED.value,
                                                          enum.TaskStatus.FAILED.value],
                                                current_user_id=current_user_id, **args)
        else:
            tasks = self.service.get_current_user_tasks(tags=getattr(namespace, model.TAGS),
                                                        current_user_id=current_user_id, **args)

        if getattr(namespace, model.DETAILS):
            for task in tasks:
                task_view = self.service.get_task_view(task.task_id, current_user_id)
                display.show_task(**vars(task_view), header="----------------------")
        else:
            for task in tasks:
                display.show_task(task, short=True)

    @error_catcher
    def handle_show_tree_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        task_id = getattr(namespace, model.TASK_ID)
        is_detailed = getattr(namespace, model.TASK_ID) or None
        tree_view = self.service.get_tree_view(task_id, is_detailed, current_user_id)
        display.show_tree(tree_view)
    # endregion

    # region plan commands
    @error_catcher
    def handle_add_plan_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)

        if getattr(namespace, model.TASK_TITLE) and not getattr(namespace, model.PLAN_TASK_ID):
            task_args = attr_to_value.get_arguments(attr_to_value.task_attributes, vars(namespace))
            task_args[model.TASK_STATUS] = enum.TaskStatus.TEMPLATE.value

            task = model.Task(**task_args)
            task = self.service.add_task(task, current_user_id)

            if getattr(namespace, model.TAGS):
                tags = [model.Tag(task.task_id, text) for text in getattr(namespace, model.TAGS)]
                self.service.add_tags(tags, current_user_id)

            if getattr(namespace, model.TASK_EXECUTORS):
                executors = [model.TaskUserRelation(enum.Role.EXECUTOR.value, task.task_id, user_id)
                             for user_id in getattr(namespace, model.TASK_EXECUTORS)]
                for executor in executors:
                    self.service.add_task_user_relation(executor, current_user_id)

            if getattr(namespace, model.TASK_OBSERVERS):
                observers = [model.TaskUserRelation(enum.Role.OBSERVER.value, task.task_id, user_id)
                             for user_id in getattr(namespace, model.TASK_OBSERVERS)]
                for observer in observers:
                    self.service.add_task_user_relation(observer, current_user_id)

            setattr(namespace, model.PLAN_TASK_ID, task.task_id)
        if not getattr(namespace, model.PLAN_TASK_ID):
            print("Specify id of existing task or parameters to create new task.")
            return

        args = attr_to_value.get_arguments(attr_to_value.plan_attributes,
                                           vars(namespace))

        if getattr(namespace, model.PLAN_COUNT_ITERATION):
            args[model.PLAN_END_TYPE] = enum.PlanEndType.NUMBER.value
        elif getattr(namespace, model.PLAN_END):
            args[model.PLAN_END_TYPE] = enum.PlanEndType.DATE.value
        else:
            args[model.PLAN_END_TYPE] = enum.PlanEndType.NEVER.value

        plan = model.Plan(**args)
        plan = self.service.add_plan(plan, current_user_id)
        display.show_plan(**vars(self.service.get_plan_view(plan.plan_id, current_user_id)),
                          header="---- Plan was created ----")

    @error_catcher
    def handle_update_plan_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.plan_attributes,
                                           vars(namespace))

        if getattr(namespace, model.PLAN_COUNT_ITERATION):
            args[model.PLAN_END_TYPE] = enum.PlanEndType.NUMBER.value
        elif getattr(namespace, model.PLAN_END):
            args[model.PLAN_END_TYPE] = enum.PlanEndType.DATE.value
        elif getattr(namespace, model.PLAN_NOT_END):
            args[model.PLAN_END_TYPE] = enum.PlanEndType.NEVER.value

        plan = self.service.get_plan(getattr(namespace, model.PLAN_ID), current_user_id)
        for key, value in args.items():
            setattr(plan, key, value)
        plan = self.service.update_plan(plan, current_user_id)
        plan_view = self.service.get_plan_view(plan.plan_id, current_user_id)
        display.show_plan(**vars(plan_view), header="---- Plan was updated ----")

    @error_catcher
    def handle_show_plan_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        plan_view = self.service.get_plan_view(getattr(namespace, model.PLAN_ID), current_user_id)
        display.show_plan(**vars(plan_view))

    @error_catcher
    def handle_remove_plan_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_plan(getattr(namespace, model.PLAN_ID), current_user_id) > 0:
            print("---- Plan was successfully deleted ----")

    @error_catcher
    def handle_show_plans_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        plans = self.service.get_user_plans(current_user_id)
        if getattr(namespace, model.DETAILS):
            for plan in plans:
                plan_view = self.service.get_plan_view(plan.plan_id, current_user_id)
                display.show_plan(**vars(plan_view), header="----------------------")
        else:
            for plan in plans:
                display.show_plan(plan, short=True)
    # endregion

    # region board commands
    @error_catcher
    def handle_add_board_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.board_attributes,
                                           vars(namespace))
        board = model.Board(**args)
        board = self.service.add_board(board, current_user_id)
        board_view = self.service.get_board_view(board.board_id, current_user_id)
        display.show_board(**vars(board_view), header="---- Board was created ----")

    @error_catcher
    def handle_update_board_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.board_attributes,
                                           vars(namespace))
        board = self.service.get_board(getattr(namespace, model.BOARD_ID), current_user_id)
        for key, value in args.items():
            setattr(board, key, value)
        board = self.service.update_board(board, current_user_id)
        board_view = self.service.get_board_view(board.board_id, current_user_id)
        display.show_board(**vars(board_view), header="---- Board was updated ----")

    @error_catcher
    def handle_show_board_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        board_view = self.service.get_board_view(getattr(namespace, model.BOARD_ID), current_user_id)
        display.show_board(**vars(board_view))

    @error_catcher
    def handle_remove_board_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_board(getattr(namespace, model.BOARD_ID), current_user_id) > 0:
            print("---- Board was successfully deleted ----")

    @error_catcher
    def handle_show_boards_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.board_attributes,
                                           vars(namespace))
        boards = self.service.get_user_boards(current_user_id, **args)

        if getattr(namespace, model.DETAILS):
            for board in boards:
                board_view = self.service.get_board_view(board.board_id, current_user_id)
                display.show_board(**vars(board_view), header="----------------------")
        else:
            for board in boards:
                display.show_board(board, short=True)
    # endregion

    # region task_date commands
    @error_catcher
    def handle_add_date_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_date_attributes,
                                           vars(namespace))
        date = model.TaskDate(**args)
        date = self.service.add_task_date(date, current_user_id)
        date_view = self.service.get_task_date_view(date.task_date_id, current_user_id)
        display.show_task_date(**vars(date_view), header="---- Task date was created ----")

    @error_catcher
    def handle_update_date_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_date_attributes,
                                           vars(namespace))
        date = self.service.get_task_date(getattr(namespace, model.TASK_DATE_ID), current_user_id)
        for key, value in args.items():
            setattr(date, key, value)
        date = self.service.update_task_date(date, current_user_id)
        date_view = self.service.get_task_date_view(date.task_date_id, current_user_id)
        display.show_task_date(**vars(date_view), header="---- Task date was updated ----")

    @error_catcher
    def handle_show_date_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        date_view = self.service.get_task_date_view(getattr(namespace, model.TASK_DATE_ID), current_user_id)
        display.show_task_date(**vars(date_view))

    @error_catcher
    def handle_remove_date_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_task_date(getattr(namespace, model.TASK_DATE_ID), current_user_id) > 0:
            print("---- Task date was successfully deleted ----")
    # endregion

    # region reminder commands
    @error_catcher
    def handle_add_reminder_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.reminder_attributes,
                                           vars(namespace))

        if not getattr(namespace, model.REMINDER_PERIODICITY):
            args[model.REMINDER_END_TYPE] = enum.ReminderEndType.NUMBER.value
            args[model.REMINDER_COUNT_ITERATION] = 1
        elif getattr(namespace, model.REMINDER_END):
            args[model.REMINDER_END_TYPE] = enum.ReminderEndType.DATE.value
        elif getattr(namespace, model.REMINDER_COUNT_ITERATION):
            args[model.REMINDER_END_TYPE] = enum.ReminderEndType.NUMBER.value
        else:
            args[model.REMINDER_END_TYPE] = enum.ReminderEndType.NEVER.value
        reminder = model.Reminder(**args)
        reminder = self.service.add_reminder(reminder, current_user_id)
        reminder_view = self.service.get_reminder_view(reminder.reminder_id, current_user_id)
        display.show_reminder(**vars(reminder_view),
                              header="---- Task reminder was created ----")

    @error_catcher
    def handle_update_reminder_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        arg = attr_to_value.get_arguments(attr_to_value.reminder_attributes,
                                           vars(namespace))

        if getattr(namespace, model.REMINDER_END):
            arg[model.REMINDER_END_TYPE] = enum.ReminderEndType.DATE.value
        elif getattr(namespace, model.REMINDER_COUNT_ITERATION):
            arg[model.REMINDER_END_TYPE] = enum.ReminderEndType.NUMBER.value
        elif getattr(namespace, model.REMINDER_NOT_END):
            arg[model.REMINDER_END_TYPE] = enum.ReminderEndType.NEVER.value

        reminder = self.service.get_reminder(getattr(namespace, model.REMINDER_ID), current_user_id)
        for key, value in arg.items():
            setattr(reminder, key, value)

        reminder = self.service.update_reminder(reminder, current_user_id)
        reminder_view = self.service.get_reminder_view(reminder.reminder_id, current_user_id)
        display.show_reminder(**vars(reminder_view),
                              header="---- Task reminder was updated ----")

    @error_catcher
    def handle_show_reminder_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        reminder_view = self.service.get_reminder_view(getattr(namespace, model.REMINDER_ID), current_user_id)
        display.show_reminder(**vars(reminder_view))

    @error_catcher
    def handle_remove_reminder_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_reminder(getattr(namespace, model.REMINDER_ID), current_user_id):
            print("---- Reminder was successfully deleted ----")

    @error_catcher
    def handle_show_reminders_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        reminders = self.service.get_user_reminders(current_user_id)
        if getattr(namespace, model.DETAILS):
            for reminder in reminders:
                reminder_view = self.service.get_reminder_view(reminder.reminder_id, current_user_id)
                display.show_reminder(**vars(reminder_view), header="----------------------")
        else:
            for reminder in reminders:
                display.show_reminder(reminder, short=True)
    # endregion

    # region task_message commands
    @error_catcher
    def handle_add_message_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.message_attributes,
                                           vars(namespace))
        message = model.Message(date=datetime.now(), user_id=current_user_id, **args)
        message = self.service.add_message(message, current_user_id)
        message_view = self.service.get_message_view(message.message_id, current_user_id)
        display.show_message(**vars(message_view))

    @error_catcher
    def handle_show_message_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        message_view = self.service.get_message_view(getattr(namespace, model.MESSAGE_ID), current_user_id)
        display.show_message(**vars(message_view))

    @error_catcher
    def handle_remove_message_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_message(getattr(namespace, model.MESSAGE_ID), current_user_id) > 0:
            print("---- Message was successfully deleted ----")

    @error_catcher
    def handle_show_messages_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        date_from = getattr(namespace, model.MESSAGES_FROM_DATE)
        date_to = getattr(namespace, model.MESSAGES_TO_DATE)
        task_id = getattr(namespace, model.MESSAGE_TASK_ID)
        messages = self.service.get_user_messages(current_user_id=current_user_id, date_from=date_from,
                                                  date_to=date_to, task_id=task_id)
        for message in messages:
            display.show_message(message, short=True)
    # endregion

    # region task_task relation commands
    @error_catcher
    def handle_add_task_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_relation_attributes,
                                           vars(namespace))

        relation = model.TaskRelation(**args)
        relation = self.service.add_task_relation(relation, current_user_id)
        relation_view = self.service.get_task_relation_view(relation.relation_id, current_user_id)
        display.show_task_relation(**vars(relation_view),
                                   header="---- Task to task relation was created ----")

    @error_catcher
    def handle_show_task_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        task_relation_view = self.service.get_task_relation_view(getattr(namespace, model.TASK_RELATION_ID),
                                                                 current_user_id)
        display.show_task_relation(**vars(task_relation_view))

    @error_catcher
    def handle_remove_task_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_task_relation(getattr(namespace, model.TASK_RELATION_ID), current_user_id) > 0:
            print("---- Task to task relation was successfully deleted ----")

    @error_catcher
    def handle_update_task_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_relation_attributes,
                                           vars(namespace))

        relation = self.service.get_task_relation(getattr(namespace, model.TASK_RELATION_ID), current_user_id)
        for key, value in args.items():
            setattr(relation, key, value)

        self.service.update_task_relation(relation, current_user_id)
        relation_view = self.service.get_task_relation_view(getattr(namespace, model.TASK_RELATION_ID), current_user_id)
        display.show_task_relation(**vars(relation_view),
                                   header="---- Task to task relation was updated ----")
    # endregion

    # region task_user relation commands
    @error_catcher
    def handle_add_task_user_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.task_user_relation_attributes,
                                           vars(namespace))
        relation = model.TaskUserRelation(**args)
        self.service.add_task_user_relation(relation, current_user_id)
        relation_view = self.service.get_task_user_relation_view(getattr(namespace, model.TASK_USER_RELATION_TASK_ID),
                                                                 getattr(namespace, model.TASK_USER_RELATION_USER_ID),
                                                                 current_user_id)
        display.show_task_user_relation(**vars(relation_view),
                                        short=True, header="---- User to task relation was created ----")

    @error_catcher
    def handle_show_task_user_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        relation_view = self.service.get_task_user_relation_view(getattr(namespace, model.TASK_USER_RELATION_TASK_ID),
                                                                 getattr(namespace, model.TASK_USER_RELATION_USER_ID),
                                                                 current_user_id)
        display.show_task_user_relation(**vars(relation_view),
                                        short=False if getattr(namespace, model.TASK_USER_RELATION_FULL) else True)

    @error_catcher
    def handle_remove_task_user_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if self.service.remove_task_user_relation(getattr(namespace, model.TASK_USER_RELATION_ID), current_user_id) > 0:
            print("---- User to task relation was successfully deleted ----")
    # endregion

    # region user_board relation commands
    @error_catcher
    def handle_add_user_board_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.user_board_relation_attributes,
                                           vars(namespace))
        relation = model.UserBoardRelation(**args)
        relation = self.service.add_user_board_relation(relation, current_user_id)

        relation_view = self.service.get_user_board_relation_view(relation.relation_id, current_user_id)
        display.show_user_board_relation(**vars(relation_view),
                                         header="---- User to board relation was created ----")

    @error_catcher
    def handle_show_user_board_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        user_id = getattr(namespace, model.USER_BOARD_RELATION_USER_ID)
        board_id = getattr(namespace, model.USER_BOARD_RELATION_BOARD_ID)

        relations = self.service.get_user_board_relations_filtered(user_id=user_id, board_id=board_id,
                                                                   current_user_id=current_user_id)
        if relations:
            relation_id = relations[0].relation_id
        else:
            print("User {} hasn't permission on the board {}".format(user_id, board_id))
            return
        relation_view = self.service.get_user_board_relation_view(relation_id, current_user_id)
        display.show_user_board_relation(**vars(relation_view))

    @error_catcher
    def handle_remove_user_board_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        relation_id = getattr(namespace, model.USER_BOARD_RELATION_ID)
        if self.service.remove_user_board_relation(relation_id, current_user_id) > 0:
            print("---- User to board relation was successfully deleted ----")

    @error_catcher
    def handle_update_user_board_relation_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        args = attr_to_value.get_arguments(attr_to_value.user_board_relation_attributes,
                                           vars(namespace))
        relation = self.service.get_user_board_relation(getattr(namespace, model.USER_BOARD_RELATION_ID), current_user_id)
        for key, value in args.items():
            setattr(relation, key, value)

        self.service.update_user_board_relation(relation, current_user_id)
        relation_view = self.service.get_user_board_relation_view(getattr(namespace, model.USER_BOARD_RELATION_ID),
                                                                  current_user_id)
        display.show_user_board_relation(**vars(relation_view),
                                         header="---- User to board relation was updated ----")
    # endregion

    # region profile commands
    @error_catcher
    def handle_register_profile_command(self, namespace):
        user = model.User(getattr(namespace, model.USER_LOGIN))
        user = self.service.add_user(user)

        config = ConfigMaker("app.config")
        config.set_user_id(user.user_id)

        print("----Welcome to IRONTASK, {0}!----".format(getattr(namespace, model.USER_LOGIN)))

    @error_catcher
    def handle_update_profile_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        user = self.service.get_user(current_user_id)
        user.login = getattr(namespace, model.USER_LOGIN)
        self.service.update_user(user)

    @error_catcher
    def handle_login_profile_command(self, namespace):
        user = self.service.get_user_by_login(getattr(namespace, model.USER_LOGIN))
        config = ConfigMaker("app.config")
        config.set_user_id(user.user_id)
        print("----Login successful----")

    @error_catcher
    def handle_logout_profile_command(self, namespace):
        config = ConfigMaker("app.config")
        config.set_user_id('')
        print("----Logout successful----")

    @error_catcher
    def handle_remove_profile_command(self, namespace):
        self.service.remove_user_by_login(getattr(namespace, model.USER_LOGIN))
        config = ConfigMaker("app.config")
        config.set_user_id('')
        print("----Profile was removed----")

    # endregion

    # region tag commands
    @error_catcher
    def handle_add_tags_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if namespace.tags is not None:
            tags = [model.Tag(getattr(namespace, model.TAG_TASK_ID), tag)
                    for tag in getattr(namespace, model.TAGS)]
            self.service.add_tags(tags, current_user_id)

        task_view = self.service.get_task_view(getattr(namespace, model.TAG_TASK_ID), current_user_id)
        display.show_task(**vars(task_view),
                          header="---- Task was updated ----")

    @error_catcher
    def handle_show_tags_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        if getattr(namespace, model.TAG_TASK_ID):
            tags = self.service.get_tags_filtered(task_id=getattr(namespace, model.TAG_TASK_ID))
        elif getattr(namespace, model.TAG_BOARD_ID):
            tags = self.service.get_board_tags(getattr(namespace, model.TAG_BOARD_ID), current_user_id)
        else:
            tags = self.service.get_user_tags(getattr(namespace, model.TAG_USER_ID), current_user_id)

        for tag in tags:
            print(tag.tag)

    @error_catcher
    def handle_remove_tags_command(self, namespace):
        current_user_id = getattr(namespace, model.CURRENT_USER_ID)
        for tag in getattr(namespace, model.TAGS):
            self.service.remove_tag_by_text(getattr(namespace, model.TAG_TASK_ID), tag, current_user_id)

        display.show_task(**vars(self.service.get_task_view(getattr(namespace, model.TAG_TASK_ID), current_user_id)),
                          header="---- Task was updated ----")
    # endregion

import configparser
import os
from irontasklib.model import models as model


class ConfigMaker:
    def __init__(self, path):
        self.path = path
        self.config = configparser.ConfigParser()
        if not os.path.exists(path):
            self.create_config(path)

    def create_config(self, path):
        self.config.add_section("Settings")
        self.config.set("Settings", "user_id", "")
        self.config.set("Settings", "connection_string", "")

        with open(path, "w") as config_file:
            self.config.write(config_file)

    def set_user_id(self, user_id):
        if user_id is None:
            user_id = ''
        self.config.read(self.path)
        self.config.set("Settings", model.USER_ID, str(user_id))
        with open(self.path, "w") as config_file:
            self.config.write(config_file)

    def get_user_id(self):
        self.config.read(self.path)
        user_id = self.config.get("Settings", model.USER_ID)
        return int(user_id) if user_id else None

    def set_connection_string(self, connection_string):
        if connection_string is None:
            connection_string = ''
        self.config.read(self.path)
        self.config.set("Settings", "connection_string", connection_string)
        with open(self.path, "w") as config_file:
            self.config.write(config_file)

    def get_connection_string(self):
        self.config.read(self.path)
        con_str = self.config.get("Settings", "connection_string")
        return con_str if con_str else None

import argparse
import sys
import re
from datetime import datetime

from irontasklib.model import enums as enum
from irontasklib.model import models as model
from irontasklib.model.enums import Periodicity
from irontaskterminal.representation import constants as command


class IronParser(argparse.ArgumentParser):
    """
        Derives from argparse.ArgumentParser and
        overrides error method for help printing

    """
    def error(self, message):
        self.print_help()
        print("Cause: {}".format(message))
        sys.exit(2)


# region first command
def get_parsed_namespace():
    parser = IronParser(prog="iron",
                        epilog="(c) Artem Glotov BSUIR 2018",
                        description="""With this program you can schedule your day by creating boards
                                                    with tasks, subtasks and reminders.""",
                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # region action command subparsers
    subparsers = parser.add_subparsers(dest=command.FIRST,
                                       title="Possible object of command",
                                       description="""This argument go as first parameter specifying
                                                      object with which you want to work with""")

    subparsers.required = True

    task_subparser = subparsers.add_parser(name=command.TASK, help="Command to work with task")
    init_task_subparser(task_subparser)

    plan_subparser = subparsers.add_parser(name=command.PLAN, help="Command to work with planner")
    init_plan_subparser(plan_subparser)

    board_subparser = subparsers.add_parser(name=command.BOARD, help="Command to work with board")
    init_board_subparser(board_subparser)

    date_subparser = subparsers.add_parser(name=command.DATE, help="Command to work with dates")
    init_date_subparser(date_subparser)

    reminder_subparser = subparsers.add_parser(name=command.REMINDER, help="Command to work with reminder")
    init_reminder_subparser(reminder_subparser)

    relation_subparser = subparsers.add_parser(name=command.RELATION, help="Command to work with relations")
    init_relation_subparser(relation_subparser)

    message_subparser = subparsers.add_parser(name=command.MESSAGE, help="Command to work with messages")
    init_message_subparser(message_subparser)

    profile_subparser = subparsers.add_parser(name=command.PROFILE, help="Command to work with profile")
    init_profile_subparser(profile_subparser)

    tags_subparser = subparsers.add_parser(name=command.TAGS, help="Command to work with tags")
    init_tags_subparser(tags_subparser)

    show_tasks_parser = subparsers.add_parser(name=command.TASKS, help="Show user's tasks ")
    init_show_tasks_parser(show_tasks_parser)

    show_boards_parser = subparsers.add_parser(name=command.BOARDS, help="Show user's boards")
    init_show_boards_parser(show_boards_parser)

    show_plans_parser = subparsers.add_parser(name=command.PLANS, help="Show user's planners")
    init_show_plans_parser(show_plans_parser)

    show_messages_parser = subparsers.add_parser(name=command.MESSAGES, help="Show user's messages")
    init_show_messages_parser(show_messages_parser)

    show_reminders_parser = subparsers.add_parser(name=command.REMINDERS, help="Show user's reminders")
    init_show_reminders_parser(show_reminders_parser)

    show_tree_parser = subparsers.add_parser(name=command.TREE, help="Show tree of depended tasks of specified task")
    init_show_tree_subparser(show_tree_parser)

    init_parser = subparsers.add_parser(name=command.INIT, help="Init your task tracker")
    init_subparser(init_parser)
    # endregion

    return parser.parse_args()
# endregion


# region second command
def init_subparser(parser):
    parser.add_argument(dest="connection_string", type=str,
                        help="Connection string of database to set up")
    parser.add_argument("--login", type=str,
                        help="User login to add")


def init_task_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_task_parser = subparsers.add_parser(name=command.ADD,
                                            help="Add task specifying its parameters")
    init_add_task_parser(add_task_parser)

    show_task_parser = subparsers.add_parser(name=command.SHOW,
                                             help="Show user's task giving its id")
    init_show_task_parser(show_task_parser)

    remove_task_parser = subparsers.add_parser(name=command.REMOVE,
                                               help="Remove task and all its relations (subtasks, dependencies,"
                                                    "executors, observers) and related objects (dates, reminders)")
    init_remove_task_parser(remove_task_parser)

    update_task_parser = subparsers.add_parser(name=command.UPDATE,
                                               help="Update user's task giving its id")
    init_update_task_parser(update_task_parser)


def init_plan_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_plan_parser = subparsers.add_parser(name=command.ADD,
                                            help="Add planner specifying its parameters")
    init_add_plan_parser(add_plan_parser)

    show_plan_parser = subparsers.add_parser(name=command.SHOW,
                                             help="Show user's planners giving its id")
    init_show_plan_parser(show_plan_parser)

    remove_plan_parser = subparsers.add_parser(name=command.REMOVE,
                                               help="Remove planner and template task")
    init_remove_plan_parser(remove_plan_parser)

    update_plan_parser = subparsers.add_parser(name=command.UPDATE,
                                               help="Update user's planner giving its id")
    init_update_plan_parser(update_plan_parser)


def init_board_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_board_parser = subparsers.add_parser(name=command.ADD,
                                             help="Add board specifying its title")
    init_add_board_parser(add_board_parser)

    show_board_parser = subparsers.add_parser(name=command.SHOW,
                                              help="Show board information")
    init_show_board_parser(show_board_parser)

    remove_board_parser = subparsers.add_parser(name=command.REMOVE,
                                                help="Remove board and connected task-group relations")
    init_remove_board_parser(remove_board_parser)

    update_board_parser = subparsers.add_parser(name=command.UPDATE,
                                                help="Update board by giving id")
    init_update_board_parser(update_board_parser)


def init_date_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_date_parser = subparsers.add_parser(name=command.ADD,
                                            help="Add a date for the task")
    init_add_date_parser(add_date_parser)

    show_date_parser = subparsers.add_parser(name=command.SHOW,
                                             help="Show the task date")
    init_show_date_parser(show_date_parser)

    remove_date_parser = subparsers.add_parser(name=command.REMOVE,
                                               help="Remove the date connected with the task")
    init_remove_date_parser(remove_date_parser)

    update_date_parser = subparsers.add_parser(name=command.UPDATE,
                                               help="Specify id of the date you want to update")
    init_update_date_parser(update_date_parser)


def init_reminder_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_reminder_parser = subparsers.add_parser(name=command.ADD,
                                                help="Create a reminder for the task")
    init_add_reminder_parser(add_reminder_parser)

    show_reminder_parser = subparsers.add_parser(name=command.SHOW,
                                                 help="Show reminder parameters")
    init_show_reminder_parser(show_reminder_parser)

    remove_reminder_parser = subparsers.add_parser(name=command.REMOVE,
                                                   help="Remove the reminder specifying its id")
    init_remove_reminder_parser(remove_reminder_parser)

    update_reminder_parser = subparsers.add_parser(name=command.UPDATE,
                                                   help="Specify id of the reminder you want to update")
    init_update_reminder_parser(update_reminder_parser)


def init_relation_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND, title="Possible relations",
                                       description="Choose relation to work with")

    subparsers.required = True

    task_task_relation_parser = subparsers.add_parser(name=command.TASK_RELATION,
                                                      help="Work with task-task relation")
    init_task_relation_parser(task_task_relation_parser)

    task_user_relation_parser = subparsers.add_parser(name=command.TASK_USER_RELATION,
                                                      help="Work with task-user relation")
    init_task_user_relation_parser(task_user_relation_parser)

    user_board_relation_parser = subparsers.add_parser(name=command.USER_BOARD_RELATION,
                                                       help="Work with user-board relation")
    init_user_board_relation_parser(user_board_relation_parser)


def init_message_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    add_message_parser = subparsers.add_parser(name=command.ADD, help="Add text")
    init_add_message_parser(add_message_parser)

    show_message_parser = subparsers.add_parser(name=command.SHOW, help="Show text")
    init_show_message_parser(show_message_parser)

    remove_message_parser = subparsers.add_parser(name=command.REMOVE, help="Remove text")
    init_remove_message_parser(remove_message_parser)


def init_profile_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="Action to perform an operation")

    subparsers.required = True

    register_profile_parser = subparsers.add_parser(name=command.REGISTER, help="Register profile")
    init_register_profile_parser(register_profile_parser)

    update_profile_parser = subparsers.add_parser(name=command.UPDATE, help="Update profile")
    init_update_profile_parser(update_profile_parser)

    login_profile_parser = subparsers.add_parser(name=command.LOGIN, help="Log in profile")
    init_login_profile_parser(login_profile_parser)

    subparsers.add_parser(name=command.LOGOUT, help="Log out profile")

    remove_profile_parser = subparsers.add_parser(name=command.REMOVE, help="Remove profile")
    init_remove_profile_parser(remove_profile_parser)


def init_tags_subparser(parser):
    subparsers = parser.add_subparsers(dest=command.SECOND,
                                       title="Possible actions",
                                       description="action to perform an operation")

    subparsers.required = True

    add_tags_parser = subparsers.add_parser(name=command.ADD, help="Add tags")
    init_add_tags_parser(add_tags_parser)

    show_tags_parser = subparsers.add_parser(name=command.SHOW, help="Show tags")
    init_show_tags_parser(show_tags_parser)

    remove_tags_parser = subparsers.add_parser(name=command.REMOVE, help="Remove tags")
    init_remove_tags_parser(remove_tags_parser)
# endregion


# region third command
def init_task_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=command.THIRD, title="Possible actions",
                                       description="action to perform an operation")

    subparsers.required = True

    add_task_relation = subparsers.add_parser(name=command.ADD, help="Add new task-task relation")
    init_add_task_relation_parser(add_task_relation)

    show_task_relation = subparsers.add_parser(name=command.SHOW, help="Show task-task relation")
    init_show_task_relation_parser(show_task_relation)

    remove_task_relation = subparsers.add_parser(name=command.REMOVE, help="Remove task-task relation")
    init_remove_task_relation_parser(remove_task_relation)

    update_task_relation = subparsers.add_parser(name=command.UPDATE, help="Update task-task relation")
    init_update_task_relation_parser(update_task_relation)


def init_task_user_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=command.THIRD, title="Possible actions",
                                       description="action to perform an operation")

    subparsers.required = True

    add_task_user_relation = subparsers.add_parser(name=command.ADD, help="Add new task-user relation")
    init_add_task_user_relation_parser(add_task_user_relation)

    show_task_user_relation = subparsers.add_parser(name=command.SHOW, help="Show task-user relation")
    init_show_task_user_relation_parser(show_task_user_relation)

    remove_task_user_relation = subparsers.add_parser(name=command.REMOVE, help="Remove task-user relation")
    init_remove_task_user_relation_parser(remove_task_user_relation)


def init_user_board_relation_parser(parser):
    subparsers = parser.add_subparsers(dest=command.THIRD, title="Possible actions",
                                       description="action to perform an operation")

    subparsers.required = True

    add_user_board_relation = subparsers.add_parser(name=command.ADD, help="Add user-board relation")
    init_add_user_board_relation_parser(add_user_board_relation)

    show_user_board_relation = subparsers.add_parser(name=command.SHOW, help="Show user-board relation")
    init_show_user_board_relation_parser(show_user_board_relation)

    remove_user_board_relation = subparsers.add_parser(name=command.REMOVE, help="Remove user-board relation")
    init_remove_user_board_relation_parser(remove_user_board_relation)

    update_user_board_relation = subparsers.add_parser(name=command.UPDATE, help="Update user-board relation")
    init_update_user_board_relation_parser(update_user_board_relation)
# endregion


# region arguments
# region task parsers initialization
def init_add_task_parser(parser):
    parser.add_argument(dest=model.TASK_TITLE, type=str,
                        help="Title of the task")
    parser.add_argument("-d", "--description", dest=model.TASK_DESCRIPTION, type=str,
                        help="Brief description of the task")
    parser.add_argument("-p", "--priority", dest=model.TASK_PRIORITY, default="normal",
                        choices=[choice.name.lower() for choice in enum.TaskPriority],
                        help="Task priority")
    parser.add_argument("-b", "--board-id", dest=model.TASK_BOARD_ID, type=unsigned_int,
                        help="Id of board for the task")
    parser.add_argument("--date", dest=model.TASK_DATE_EVENT, type=valid_date,
                        help="Date for the task-event the in format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("--start", dest=model.TASK_DATE_START, type=valid_date,  # TODO may be group some arguments
                        help="Start date for the durable task in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("--end", dest=model.TASK_DATE_END, type=valid_date,
                        help="End date for the durable task in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("--tags", dest=model.TAGS,  type=str, nargs="*",
                        help="Tags for the task")
    parser.add_argument("--executors", dest=model.TASK_EXECUTORS, type=unsigned_int, nargs="*",
                        help="Executors ids for the task")
    parser.add_argument("--observers", dest=model.TASK_OBSERVERS, type=unsigned_int, nargs="*",
                        help="Executors ids for the task")


def init_show_task_parser(parser):
    parser.add_argument(dest=model.TASK_ID, type=unsigned_int,
                        help="Id of the task you want to see")


def init_remove_task_parser(parser):
    parser.add_argument(dest=model.TASK_ID, type=unsigned_int,
                        help="Id of the task you want to remove")


def init_update_task_parser(parser):
    parser.add_argument(dest=model.TASK_ID, type=unsigned_int,
                        help="Id of task you want to update")
    parser.add_argument("-t", "--title", dest=model.TASK_TITLE, type=str,
                        help="Title of the task")
    parser.add_argument("-d", "--description", dest=model.TASK_DESCRIPTION, type=str,
                        help="Brief description of the task")
    parser.add_argument("-p", "--priority", dest=model.TASK_PRIORITY,
                        choices=[choice.name.lower() for choice in enum.TaskPriority],
                        help="Priority of the task")
    exclusive_group = parser.add_mutually_exclusive_group()
    exclusive_group.add_argument("-b", "--board-id", dest=model.TASK_BOARD_ID, type=unsigned_int,
                                 help="Id of the board for the task")
    exclusive_group.add_argument("--not-board", dest=model.TASK_WITHOUT_BOARD, action="store_true",
                                 help="If you want remove task from board")
    parser.add_argument("-s", "--status", dest=model.TASK_STATUS,
                        choices=[choice.name.lower() for choice in enum.TaskStatus],
                        help="Status of the task")


def init_show_tasks_parser(parser):
    parser.add_argument("-d", "--details", dest=model.DETAILS, action="store_true",
                        help="Show full description of each task")
    parser.add_argument("-s", "--status", dest=model.TASK_STATUS,
                        choices=[choice.name.lower() for choice in enum.TaskStatus],
                        help="Specify this option if you want to see tasks of a specific status, "
                             "otherwise only the current tasks will be shown")
    parser.add_argument("-p", "--priority", dest=model.TASK_PRIORITY,
                        choices=[choice.name.lower() for choice in enum.TaskPriority],
                        help="To see the tasks with specific priority")
    parser.add_argument("--role", dest=model.TASK_USER_RELATION_ROLE,
                        choices=[choice.name.lower() for choice in enum.Role],
                        help="To see the tasks where the user performs a specific role")
    parser.add_argument("--tags", dest=model.TAGS, type=str, nargs="*",
                        help="To see the tasks with specific tags")


def init_show_tree_subparser(parser):
    parser.add_argument(dest=model.TASK_ID, type=unsigned_int,
                        help="Id of root task")
    parser.add_argument("-d", "--details", dest=model.DETAILS, action="store_true",
                        help="Detailed description for each node of the tree")
# endregion


# region plan parsers initialization
def init_add_plan_parser(parser):
    # plan
    parser.add_argument(dest=model.PLAN_PERIODICITY, type=valid_periodicity,
                        help="Period of task repetition (like '2Y 3M 12h' or '3M 7D 1m')")
    parser.add_argument(dest=model.PLAN_START, type=valid_date,
                        help="Date of the first task creation")

    exclusive_group = parser.add_mutually_exclusive_group()
    exclusive_group.add_argument("--end", dest=model.PLAN_END, type=valid_date,
                                 help="The end date for the repetition in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    exclusive_group.add_argument("--times", dest=model.PLAN_COUNT_ITERATION, type=unsigned_int,
                                 help="Count of task repetition")
    # template task
    parser.add_argument("-i", "--task-id", dest=model.PLAN_TASK_ID, type=unsigned_int,
                        help="Signify id of some task if you want add plan to existing task")
    parser.add_argument("-t", "--title", dest=model.TASK_TITLE, type=str,
                        help="Title of the task")
    parser.add_argument("-d", "--description", dest=model.TASK_DESCRIPTION, type=str,
                        help="Brief description of the task")
    parser.add_argument("-b", "--board-id", dest=model.TASK_BOARD_ID, type=unsigned_int,
                        help="Id of board for the task")
    parser.add_argument("-p", "--priority", dest=model.TASK_PRIORITY, default="normal",
                        choices=[choice.name.lower() for choice in enum.TaskPriority],
                        help="Task priority")

    # other
    parser.add_argument("--tags", dest=model.TAGS, type=str, nargs="*",
                        help="Tags for the task")
    parser.add_argument("--executors", dest=model.TASK_EXECUTORS, type=unsigned_int, nargs="*",
                        help="Executors ids for the task")
    parser.add_argument("--observers", dest=model.TASK_OBSERVERS, type=unsigned_int, nargs="*",
                        help="Executors ids for the task")


def init_show_plan_parser(parser):
    parser.add_argument(dest=model.PLAN_ID, type=unsigned_int,
                        help="Id of plan you want to see")


def init_remove_plan_parser(parser):
    parser.add_argument(dest=model.PLAN_ID, type=unsigned_int,
                        help="Id of plan you want to remove (template task also will be removed)")


def init_update_plan_parser(parser):
    parser.add_argument(dest=model.PLAN_ID, type=unsigned_int,
                        help="Id of plan you want to update")
    parser.add_argument("-p", "--period", dest=model.PLAN_PERIODICITY, type=valid_periodicity,
                        help="Period of task repetition (like '2Y 3M 12h' or '3M 7D 1m')")
    parser.add_argument("-s", "--status", dest=model.PLAN_STATUS,
                        choices=[choice.name.lower() for choice in enum.PlanStatus
                                 if choice != enum.PlanStatus.ARCHIVED.value],
                        help="Status for plan")
    parser.add_argument("--start", dest=model.PLAN_START, type=valid_date,
                        help="Date of the first task creation in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")

    exclusive_group = parser.add_mutually_exclusive_group()
    exclusive_group.add_argument("--not-end", dest=model.PLAN_NOT_END, action="store_true",
                                 help="Signify it of you want infinity plan")
    exclusive_group.add_argument("--end", dest=model.PLAN_END, type=valid_date,
                                 help="The end date for the repetition in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    exclusive_group.add_argument("--times", dest=model.PLAN_COUNT_ITERATION, type=unsigned_int,
                                 help="Count of task repetition")


def init_show_plans_parser(parser):
    parser.add_argument("-d", "--details", dest=model.DETAILS, action="store_true",
                        help="Show full description of each plan")
# endregion


# region board parsers initialization
def init_add_board_parser(parser):
    parser.add_argument(dest=model.BOARD_TITLE, type=str,
                        help="Title of the board")
    parser.add_argument("-d", "--description", dest=model.BOARD_DESCRIPTION, type=str,
                        help="Brief description of the board")
    parser.add_argument("--type", dest=model.BOARD_TYPE, default=enum.BoardType.PUBLIC.name,
                        choices=[choice.name.lower() for choice in enum.BoardType],
                        help="Choose board type")


def init_show_board_parser(parser):
    parser.add_argument(dest=model.BOARD_ID, type=unsigned_int,
                        help="Id of board")


def init_remove_board_parser(parser):
    parser.add_argument(dest=model.BOARD_ID, type=unsigned_int,
                        help="Specify board id")


def init_update_board_parser(parser):
    parser.add_argument(dest=model.BOARD_ID, type=unsigned_int,
                        help="Id of board you want to update")
    parser.add_argument("-t", "--title", dest=model.BOARD_TITLE, type=str,
                        help="Title of the board")
    parser.add_argument("-d", "--description", dest=model.BOARD_DESCRIPTION, type=str,
                        help="Brief description of the board")
    parser.add_argument("-s", "--status", dest=model.BOARD_STATUS,
                        choices=[choice.name.lower() for choice in enum.BoardStatus
                                 if choice != enum.BoardStatus.ARCHIVED],
                        help="Change board status")
    parser.add_argument("--type", dest=model.BOARD_TYPE,
                        choices=[choice.name.lower() for choice in enum.BoardType if choice != enum.BoardType.SINGLE],
                        help="Change board type")


def init_show_boards_parser(parser):
    parser.add_argument("-d", "--details", dest=model.DETAILS, action="store_true",
                        help="Show full description of each board")
    parser.add_argument("--status", dest=model.BOARD_STATUS,
                        choices=[choice.name.lower() for choice in enum.BoardStatus],
                        help="Specify board status")
    parser.add_argument("--type", dest=model.BOARD_TYPE,
                        choices=[choice.name.lower() for choice in enum.BoardType],
                        help="Specify board type")
# endregion


# region start_date parsers initialization
def init_add_date_parser(parser):
    parser.add_argument(dest=model.TASK_DATE_TASK_ID, type=unsigned_int,
                        help="Id of task to which the date will be assigned")
    parser.add_argument(dest=model.TASK_DATE, type=valid_date,
                        help="Date in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument(dest=model.TASK_DATE_TYPE,
                        choices=[choice.name.lower() for choice in enum.TaskDateType
                                 if choice != enum.TaskDateType.CREATE and choice != enum.TaskDateType.CLOSE],
                        help="Choose date type")


def init_show_date_parser(parser):
    parser.add_argument(dest=model.TASK_DATE_ID, type=unsigned_int,
                        help="Id of date you want to see information about")


def init_remove_date_parser(parser):
    parser.add_argument(dest=model.TASK_DATE_ID, type=unsigned_int,
                        help="Specify start_date id")


def init_update_date_parser(parser):
    parser.add_argument(dest=model.TASK_DATE_ID, type=unsigned_int,
                        help="Id of date to be updated")
    parser.add_argument("-i", "--task-id", dest=model.TASK_DATE_TASK_ID, type=unsigned_int,
                        help="Id of task to which the date will be assigned")
    parser.add_argument("-d", "--date", dest=model.TASK_DATE, type=valid_date,
                        help="Date in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("--type", dest=model.TASK_DATE_TYPE,
                        choices=[choice.name.lower() for choice in enum.TaskDateType
                                 if choice != enum.TaskDateType.CREATE and choice != enum.TaskDateType.CLOSE],
                        help="Choose date type")
# endregion


# region reminder parsers initialization
def init_add_reminder_parser(parser):
    parser.add_argument(dest=model.REMINDER_TASK_ID, type=unsigned_int,
                        help="Id of the task on which reminder will be hanged")
    parser.add_argument(dest=model.REMINDER_START, type=valid_date,
                        help="Date of the first notification")
    # parser.add_argument(dest=model.REMINDER_START_TIME, type=valid_time,
    #                     help="Time of the first notification")
    parser.add_argument("-t", "--text", dest=model.REMINDER_TEXT, type=str,
                        help="Text to notify you")
    parser.add_argument("-p", "--period", dest=model.PLAN_PERIODICITY, type=valid_periodicity,
                        help="Period of task repetition (like '2Y 3M 12h' or '3M 7D 1m')")

    exclusive_group = parser.add_mutually_exclusive_group()
    exclusive_group.add_argument("--end", dest=model.REMINDER_END, type=valid_date,
                                 help="The end date of repetition in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    exclusive_group.add_argument("--times", dest=model.REMINDER_COUNT_ITERATION, type=unsigned_int,
                                 help="Number of times to repeat a reminder")


def init_show_reminder_parser(parser):
    parser.add_argument(dest=model.REMINDER_ID, type=unsigned_int,
                        help="Id of reminder")


def init_remove_reminder_parser(parser):
    parser.add_argument(dest=model.REMINDER_ID, type=unsigned_int,
                        help="Id of reminder you want to remove")


def init_update_reminder_parser(parser):
    parser.add_argument(dest=model.REMINDER_ID, type=unsigned_int,
                        help="Id of reminder you want to update")
    parser.add_argument("-i", "--task-id", dest=model.REMINDER_TASK_ID, type=unsigned_int,
                        help="Id of the task on which reminder will be hanged")
    parser.add_argument("-D", "--date", dest=model.REMINDER_START, type=valid_date,
                        help="Date of the first notification in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("-t", "--text", dest=model.REMINDER_TEXT, type=str,
                        help="Text to notify you")
    parser.add_argument("-s", "--status", dest=model.REMINDER_STATUS,
                        choices=[choice.name.lower() for choice in enum.ReminderStatus],
                        help="Status of the reminder")
    parser.add_argument("-p", "--period", dest=model.PLAN_PERIODICITY, type=valid_periodicity,
                        help="Period of task repetition (like '2Y 3M 12h' or '3M 7D 1m')")

    exclusive_group = parser.add_mutually_exclusive_group()
    exclusive_group.add_argument("--not-end", dest=model.REMINDER_NOT_END, action="store_true",
                                 help="Signify it of you want infinity reminder")
    exclusive_group.add_argument("--end", dest=model.REMINDER_END, type=valid_date,
                                 help="The end date of repetition in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    exclusive_group.add_argument("--times", dest=model.REMINDER_COUNT_ITERATION, type=unsigned_int,
                                 help="Number of times to repeat a reminder")


def init_show_reminders_parser(parser):
    parser.add_argument("-d", "--details", dest=model.DETAILS, action="store_true",
                        help="Show full description of each reminder")

# endregion


# region relation parsers initialization
# region task user relation parsers initialization
def init_add_task_user_relation_parser(parser):
    parser.add_argument(dest=model.TASK_USER_RELATION_TASK_ID, type=unsigned_int,
                        help="Id of task to which user will be assigned")
    parser.add_argument(dest=model.TASK_USER_RELATION_USER_ID, type=unsigned_int,
                        help="Specify user to task")
    parser.add_argument(dest=model.TASK_USER_RELATION_ROLE,
                        choices=[enum.Role.EXECUTOR.name.lower(), enum.Role.OBSERVER.name.lower()],
                        help="Role of user on task")


def init_show_task_user_relation_parser(parser):
    parser.add_argument(dest=model.TASK_USER_RELATION_TASK_ID, type=unsigned_int,
                        help="Id of task")
    parser.add_argument(dest=model.TASK_USER_RELATION_USER_ID, type=unsigned_int,
                        help="Id of user whose roles will be issued")
    parser.add_argument("-f", "--full", dest=model.TASK_USER_RELATION_FULL, action="store_true",
                        help="Specify to show full description of relations")


def init_remove_task_user_relation_parser(parser):
    parser.add_argument(dest=model.TASK_USER_RELATION_ID, type=unsigned_int,
                        help="Id of user to task relation")
# endregion


# region task relation parsers initialization
def init_add_task_relation_parser(parser):
    parser.add_argument(dest=model.TASK_RELATION_PARENT_ID, type=unsigned_int,
                        help="Specify id of parent task")
    parser.add_argument(dest=model.TASK_RELATION_CHILD_ID, type=unsigned_int,
                        help="Specify id of child task")
    parser.add_argument(dest=model.TASK_RELATION_TYPE,
                        choices=[choice.name.lower() for choice in enum.TaskRelationType],
                        help="Relation type between this two tasks")


def init_show_task_relation_parser(parser):
    parser.add_argument(dest=model.TASK_RELATION_ID, type=unsigned_int,
                        help="Id of task-task relation")


def init_remove_task_relation_parser(parser):
    parser.add_argument(dest=model.TASK_RELATION_ID, type=unsigned_int,
                        help="Id of task-task relation")


def init_update_task_relation_parser(parser):
    parser.add_argument(dest=model.TASK_RELATION_ID, type=unsigned_int,
                        help="Id of the relation you want to update")
    parser.add_argument(dest=model.TASK_RELATION_TYPE,
                        choices=[choice.name.lower() for choice in enum.TaskRelationType],
                        help="New relation type between parent task and child task")
# endregion


# region user board relation parsers initialization
def init_add_user_board_relation_parser(parser):
    parser.add_argument(dest=model.USER_BOARD_RELATION_BOARD_ID, type=unsigned_int,
                        help="Id of board whose permission will be issued")
    parser.add_argument(dest=model.USER_BOARD_RELATION_USER_ID, type=unsigned_int,
                        help="Id of user to get permission on board")
    parser.add_argument(dest=model.USER_BOARD_RELATION_PERMISSION,
                        choices=[choice.name.lower() for choice in enum.BoardPermission
                                 if choice != enum.BoardPermission.CREATOR
                                 and choice != enum.BoardPermission.UNREGISTERED],
                        help="Type of permissions")


def init_show_user_board_relation_parser(parser):
    parser.add_argument(dest=model.USER_BOARD_RELATION_BOARD_ID, type=int,
                        help="Id of the board")
    parser.add_argument(dest=model.USER_BOARD_RELATION_USER_ID, type=int,
                        help="Id of the user to get permission on the board")


def init_remove_user_board_relation_parser(parser):
    parser.add_argument(dest=model.USER_BOARD_RELATION_ID, type=unsigned_int,
                        help="Id of user-board relation")


def init_update_user_board_relation_parser(parser):
    parser.add_argument(dest=model.USER_BOARD_RELATION_ID, type=unsigned_int,
                        help="Id of user-board relation")
    parser.add_argument(dest=model.USER_BOARD_RELATION_PERMISSION,
                        choices=[choice.name.lower() for choice in enum.BoardPermission
                                 if choice != enum.BoardPermission.CREATOR
                                 and choice != enum.BoardPermission.UNREGISTERED],
                        help="Type of permissions")

# endregion
# endregion


# region profile parsers initialization
def init_register_profile_parser(parser):
    parser.add_argument(dest=model.USER_LOGIN, type=str,
                        help="Login for your profile")


def init_update_profile_parser(parser):
    parser.add_argument(dest=model.USER_LOGIN, type=str,
                        help="New login for your profile")


def init_login_profile_parser(parser):
    parser.add_argument(dest=model.USER_LOGIN, type=str,
                        help="Login of your profile")


def init_remove_profile_parser(parser):
    parser.add_argument(dest=model.USER_LOGIN, type=str,
                        help="Login of the profile to be removed")
# endregion


# region message parsers initialization
def init_add_message_parser(parser):
    parser.add_argument(dest=model.MESSAGE_TASK_ID, type=unsigned_int,
                        help="Task to which will be added text")
    parser.add_argument(dest=model.MESSAGE_TEXT, type=str,
                        help="Message for task")


def init_show_message_parser(parser):
    parser.add_argument(dest=model.MESSAGE_ID, type=unsigned_int,
                        help="Id of message you want to read")


def init_remove_message_parser(parser):
    parser.add_argument(dest=model.MESSAGE_ID, type=unsigned_int,
                        help="Id of message you want to remove")


def init_show_messages_parser(parser):
    parser.add_argument("-i", "--task_id", dest=model.MESSAGE_TASK_ID, type=unsigned_int,
                        help="Task which messages will be shown")
    parser.add_argument("--from", dest=model.MESSAGES_FROM_DATE, type=valid_date,
                        help="Date from which to show messages in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
    parser.add_argument("--to", dest=model.MESSAGES_TO_DATE, type=valid_date,
                        help="Date to which to show messages in the format YYYY-mm-DD/HH:MM or YYYY-mm-DD")
# endregion


# region tag parsers initialization
def init_add_tags_parser(parser):
    parser.add_argument(dest=model.TAG_TASK_ID, type=unsigned_int,
                        help="Specify task id to where will be added tags")
    parser.add_argument(dest=model.TAGS, nargs="+", type=str,
                        help="List tags to add")


def init_show_tags_parser(parser):
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-b", "--board-id", dest=model.TAG_BOARD_ID, type=unsigned_int,
                       help="Specify board id whose task tags will be displayed")
    group.add_argument("-i", "--task-id", dest=model.TAG_TASK_ID, type=unsigned_int,
                       help="Specify task id whose tags will be showed")


def init_remove_tags_parser(parser):
    parser.add_argument(dest=model.TAG_TASK_ID, type=unsigned_int,
                        help="Specify task id where tags will be removed")
    parser.add_argument(dest=model.TAGS, nargs="+", type=str, help="List tags to remove")
# endregion
# endregion


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d/%H:%M")
    except ValueError:
        try:
            return datetime.strptime(s, "%Y-%m-%d")
        except ValueError:
            msg = "Not a valid start_date: '{0}'. Example: '2018-11-11/11:11' or '2018-11-11'.".format(s)
            raise argparse.ArgumentTypeError(msg)


def unsigned_int(i):
    try:
        i = int(i)
        if i > 0:
            return i
        else:
            raise ValueError()
    except ValueError:
        msg = "Not a positive integer: {}.".format(i)
        raise argparse.ArgumentTypeError(msg)


def valid_periodicity(s):
    regex = '((?P<years>\d+)Y )?((?P<months>\d+)M )?((?P<days>\d+)D )?((?P<hours>\d+)h )?((?P<minutes>\d+)m )?'
    s = s.strip() + ' '
    pattern = re.compile(regex)
    match = pattern.match(s)
    if match:
        result = {
            Periodicity.YEARS.value: (int(match.group(Periodicity.YEARS.value))
                                      if match.group(Periodicity.YEARS.value) else 0),
            Periodicity.MONTHS.value: (int(match.group(Periodicity.MONTHS.value))
                                       if match.group(Periodicity.MONTHS.value) else 0),
            Periodicity.DAYS.value: (int(match.group(Periodicity.DAYS.value))
                                     if match.group(Periodicity.DAYS.value) else 0),
            Periodicity.HOURS.value: (int(match.group(Periodicity.HOURS.value))
                                      if match.group(Periodicity.HOURS.value) else 0),
            Periodicity.MINUTES.value: (int(match.group(Periodicity.MINUTES.value))
                                        if match.group(Periodicity.MINUTES.value) else 0)
        }
        return result
    else:
        raise argparse.ArgumentTypeError('Period format needs to be "##Y ##M ##D ##h ##m"')

import logging

from irontasklib.services.services import Services
from irontasklib.storage.sqlalchemy_repos import SQLAlchemyRepos
from irontasklib.services import exceptions as ex

from irontaskterminal.handlers import TerminalMenu
from irontaskterminal import parsing
from irontaskterminal.representation import constants as command
from irontaskterminal.config_service import ConfigMaker


def main():
    namespace = parsing.get_parsed_namespace()

    initial_set_up = getattr(namespace, command.FIRST) == command.INIT
    if initial_set_up:
        TerminalMenu.handle_init_command(Services, SQLAlchemyRepos, namespace)
        return

    config = ConfigMaker('app.config')
    if not config.get_connection_string():
        print("To get started, please init your application specifying connection string (and optional login):\n"
              "Example: user$ iron init mysql//user:PASSWORD@host:port --login MY_LOGIN")
        return

    # data_storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306")
    data_storage = SQLAlchemyRepos(config.get_connection_string())

    service = Services(data_storage, need_check=True)

    if (config.get_user_id() is None
            and not (getattr(namespace, command.FIRST) == command.PROFILE
                     and (getattr(namespace, command.SECOND) == command.REGISTER
                          or getattr(namespace, command.SECOND) == command.LOGIN))):
        print("To get started you need to register or login.\n"
              "To register: user$ iron profile register 'YOUR_LOGIN'\n"
              "To login: user$ iron profile login 'YOUR_LOGIN'")
        return

    current_user_id = config.get_user_id()
    if current_user_id:
        try:
            user = service.get_user(user_id=current_user_id)
            namespace.current_user_id = user.user_id
        except ex.NotFoundError:
            print("Can't find user with id {user_id}".format(user_id=current_user_id))
            config.set_user_id('')
            return

    service.update_condition()
    # handling arguments
    menu = TerminalMenu(service)
    menu.handle(namespace)


if __name__ == '__main__':
    main()

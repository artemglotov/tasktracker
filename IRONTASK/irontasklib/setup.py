from setuptools import setup, find_packages

setup(
    name="irontasklib",
    version="0.2.0",
    author="Glotov Artem",
    author_email="glotovartemalex@gmail.com",
    description="Library for IronTask",
    packages=find_packages(exclude=['tests'])
)
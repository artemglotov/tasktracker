"""
    This package includes modules for work with
    data storage

    Modules:
        services - provides data access and interaction
        constraints - contains constraints for data
        exceptions - lists exceptions that can rise within this package
        log_service - provides logging, contains default logger and logger's handlers
        conditions - provides update tasks, plans, reminders in accordance to the data

    Functions:
        log - decorator to write logs of functions calling events
        exception_log - decorator to write logs of exceptions and warnings

"""

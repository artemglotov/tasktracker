import logging

from sqlalchemy.exc import SQLAlchemyError

from irontasklib.services.exceptions import IronTaskError


def log(logger):
    def decorator(func):
        """ Decorates function, logging its title on debug level"""
        def wrapper(*args, **kwargs):
            logger.debug("{}: {}()".format(func.__module__, func.__name__))
            return func(*args, **kwargs)
        return wrapper
    return decorator


def exception_log(logger):
    def decorator(func):
        """ Decorates function, logging exception and then raising it further"""
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except IronTaskError as e:
                logger.warning("[{error_name}] {module}: {msg}".format(module=func.__module__,
                                                                       error_name=e.__class__.__name__, msg=str(e)))
                raise
            except SQLAlchemyError as e:
                logger.exception(e.args, exc_info=False)
                raise
            except Exception as e:
                logger.critical(e)
                raise
        return wrapper
    return decorator


def default_logger(logger_name='default_logger', filename='default.log', is_file=True, is_stream=False, has_date=True):
    """

    Parameters
    ----------
    logger_name: str
    filename: str
        File where will be writen logs
    is_file:
        Specify 'true' to create file handler
    is_stream
        Specify 'true' to create stream handler (terminal)
    has_date
        Specify 'true' if you want logging dates of events

    Returns
    -------
    logger: Logger
        Default logger
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    if not logger.hasHandlers():
        add_handlers(logger, filename, is_file, is_stream, has_date)
    return logger


def add_handlers(logger, filename='default.log', is_file=False, is_stream=False, has_date=True):
    log_format = '%(levelname)s (%(name)s): %(message)s'
    if has_date:
        log_format = '%(asctime)s | ' + log_format

    formatter = logging.Formatter(log_format)

    file_handler = logging.FileHandler(filename=filename, mode='w')
    file_handler.setFormatter(formatter)
    if is_file:
        logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    if is_stream:
        logger.addHandler(stream_handler)

"""
    This module provides Service class with all
    necessary functions that can be used to access
    data layer

"""

from datetime import datetime

from irontasklib.storage.sqlalchemy_repos import session_scope

from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.model import models_view as vmodel

from irontasklib.services import exceptions as ex
from irontasklib.services.conditions import Condition
from irontasklib.services.constraints.authorization import AuthorizationConstraints
from irontasklib.services.constraints.duplication import DuplicationConstraint
from irontasklib.services.constraints.existence import ExistenceConstraint
from irontasklib.services.constraints.logic import LogicConstraint
from irontasklib.services.log_services import log
from irontasklib.services.log_services import exception_log
from irontasklib.services.log_services import default_logger


class Services:
    """
    Most functions has an argument 'current_user_id' it is necessary to authorize user
    when specified parameter 'need_check'
    """

    logger = default_logger(is_stream=False)
    ex_logger = default_logger(is_stream=False)

    def __init__(self, data_storage, need_check=False, is_archived=False):
        """Init service

        Parameters
        ----------
        data_storage : SQLAlchemyRepos
            DLA for this library.
        need_check : boolean
            Specify 'true' for check user's permissions for tasks and boards.
        is_archived : boolean
            Specify 'true' if you don't want delete models. It will be marked with status 'archived'.
        Returns
        -------
        type
            

        """
        self.need_check = need_check
        self.is_archived = is_archived
        self.storage = data_storage
        self.logic = LogicConstraint(data_storage)
        self.duplication = DuplicationConstraint(data_storage)
        self.existence = ExistenceConstraint(data_storage)
        self.authorization = AuthorizationConstraints(data_storage)
        self.updater = Condition(data_storage)

    @exception_log(ex_logger)
    @log(logger)
    def update_condition(self):
        """
        Update all conditions: reminder's, task's, plan's

        """
        self.updater.check_all()

    # region task methods
    @exception_log(ex_logger)
    @log(logger)
    def get_task(self, task_id, current_user_id=None):
        """Returns task specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        task_id : integer
        current_user_id : integer (defaults to None)

        Returns
        -------
        Task
            Found by id task.

        """
        task = self.storage.get_task(task_id)
        if task:
            if self.need_check:
                self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
            return task
        else:
            raise ex.NotFoundError(model.Task, task_id)

    # @exception_log(ex_logger)
    # @log(logger)
    # def get_poss_child_tasks(self, relation_type, parent_id, current_user_id=None):
    #     parent_task = self.storage.get_task(parent_id)
    #     if parent_task.board_id is None:
    #         return []
    #     board_tasks = self.storage.get_tasks_filtered(board_id=parent_task.board_id)
    #     poss_child_tasks = []
    #     if relation_type == enum.TaskRelationType.SUBTASK.value:
    #         for task in board_tasks:
    #             parent_task_relation = self.storage.get_task_relations_filtered(child_id=task.task_id,
    #                                                                             relation_types=[enum.TaskRelationType.SUBTASK.value])
    #             if parent_task_relation:
    #                 continue
    #             parent_other_relation = self.storage.get_task_relations_filtered(child_id=task.task_id,
    #                                                                              relation_types=[enum.TaskRelationType.COMPLETE.value,
    #                                                                                              enum.TaskRelationType.BLOCK.value])
    #             if parent_other_relation:
    #                 continue
    #             child_other_relation = self.storage.get_task_relations_filtered(parent_id=task.task_id,
    #                                                                             relation_types=[enum.TaskRelationType.COMPLETE.value,
    #                                                                                             enum.TaskRelationType.BLOCK.value])
    #             if child_other_relation:
    #                 continue
    #             poss_child_tasks.append(task)
    #     if relation_type in [enum.TaskRelationType.COMPLETE.value, enum.TaskRelationType.BLOCK.value]:
    #         grand_parent_task = self.storage.get_task_relations_filtered(child_id=parent_id,
    #                                                                      relation_types=[enum.TaskRelationType.SUBTASK.value])
    #         if grand_parent_task:
    #             for task in board_tasks:
    #                 parent_task_relation = self.storage.get_task_relations_filtered(child_id=task.task_id,
    #                                                                                 relation_types=[enum.TaskRelationType.SUBTASK.value])
    #                 if parent_task_relation.parent_id == grand_parent_task.task_id:
    #                     poss_child_tasks.append(task)
    #

    @exception_log(ex_logger)
    @log(logger)
    def get_tasks_filtered(self, title=None, description=None, priority=None, statuses=None, board_id=None):
        return self.storage.get_tasks_filtered(title, description, priority, statuses, board_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_view(self, task_id, current_user_id=None):
        """Fills and returns task view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        task_id : integer
        current_user_id : integer (defaults to None)

        Returns
        -------
        TaskView
            Filled object for representation.

        """
        self.existence.check_existence(model.Task, task_id)
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)

        with session_scope() as session:
            task = self.storage.get_task(task_id, session=session)
            board = self.storage.get_board(task.board_id, session=session) if task.board_id else None
            creator = self.storage.get_task_creator(task_id, session=session)
            executors = self.storage.get_task_executors(task_id, session=session)
            observers = self.storage.get_task_observers(task_id, session=session)
            requester = self.storage.get_task_requester(task_id, session=session)
            finisher = self.storage.get_task_finisher(task_id, session=session)
            dates = self.storage.get_task_dates_filtered(task_id, session=session)
            date_create_list = list(filter(lambda date: date.date_type == enum.TaskDateType.CREATE.value, dates))
            date_create = date_create_list[0] if date_create_list else None
            date_event_list = list(filter(lambda date: date.date_type == enum.TaskDateType.EVENT.value, dates))
            date_event = date_event_list[0] if date_event_list else None
            date_start_list = list(filter(lambda date: date.date_type == enum.TaskDateType.START.value, dates))
            date_start = date_start_list[0] if date_start_list else None
            date_end_list = list(filter(lambda date: date.date_type == enum.TaskDateType.END.value, dates))
            date_end = date_end_list[0] if date_end_list else None
            date_close_list = list(filter(lambda date: date.date_type == enum.TaskDateType.CLOSE.value, dates))
            date_close = date_close_list[0] if date_close_list else None
            dates_edit = list(filter(lambda date: date.date_type == enum.TaskDateType.EDIT.value, dates))
            parent_relations = self.storage.get_task_relations_filtered(child_id=task_id, session=session)
            child_relations = self.storage.get_task_relations_filtered(parent_id=task_id, session=session)
            messages = self.storage.get_messages_filtered(task_id=task_id, session=session)
            reminders = self.storage.get_reminders(task_id, session=session)
            tags = self.storage.get_tags_filtered(task_id=task_id, session=session)
        return vmodel.TaskView(task=task, board=board, creator=creator, executors=executors, observers=observers,
                               requester=requester, finisher=finisher, date_create=date_create, date_event=date_event,
                               date_start=date_start, date_end=date_end, date_close=date_close, messages=messages,
                               reminders=reminders, tags=tags, parent_relations=parent_relations,
                               child_relations=child_relations, dates_edit=dates_edit)

    @exception_log(ex_logger)
    @log(logger)
    def get_tasks_by_tags(self, tags, current_user_id=None):
        """Returns list of task bounded with specified tag.

        Parameters
        ----------
        tags : list(Tag)
        current_user_id : type (defaults to None)

        Returns
        -------
        list(Task)
            Tasks tagged with tags

        """
        return self.storage.get_tasks_by_tags(tags, current_user_id)


    @exception_log(ex_logger)
    @log(logger)
    def get_user_tasks(self, role=None, statuses=None, priority=None, tags=None, title=None, current_user_id=None):
        """Extracts and returns all tasks of user.

        Parameters
        ----------
        role : Role(Enum) (defaults to None)
        statuses : list(Status(Enum)) (defaults to None)
        priority : Priority(Enum) (defaults to None)
        tags : list(Tag) (defaults to None)
        current_user_id : integer (defaults to None)
        title: str

        Returns
        -------
        list(Tasks)
            Filtered by parameters tasks of user.

        """
        return self.storage.get_user_tasks(current_user_id, role=role, statuses=statuses,
                                           priority=priority, tags=tags, title=title)

    @exception_log(ex_logger)
    @log(logger)
    def get_current_user_tasks(self, role=None, priority=None, tags=None, current_user_id=None):
        """Extracts and returns all current(active, overdue, request) tasks of user.

        Parameters
        ----------
        role : Role(Enum) (defaults to None)
        priority : Priority(Enum) (defaults to None)
        tags : list(Tag) (defaults to None)
        current_user_id : integer (defaults to None)

        Returns
        -------
        list(Tasks)
            Filtered by parameters tasks of user.

        """
        with session_scope() as session:
            tasks = self.storage.get_user_tasks(user_id=current_user_id, role=role, priority=priority,
                                                statuses=[enum.TaskStatus.ACTIVE.value, enum.TaskStatus.OVERDUE.value,
                                                          enum.TaskStatus.REQUEST.value],
                                                tags=tags, session=session)
        return tasks

    @exception_log(ex_logger)
    @log(logger)
    def get_parent_tasks(self, child_id, relation_type=None, current_user_id=None):
        """Returns parent tasks of specified task.
        Checks user for authorization.

        Parameters
        ----------
        child_id : integer
        relation_type : TaskRelationType(Enum) (defaults to None)
        current_user_id : integer (defaults to None)

        Returns
        -------
        list(Task)
            Parents of task

        """
        if self.need_check:
            self.authorization.check_permission(child_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_parent_tasks(child_id, relation_type)

    @exception_log(ex_logger)
    @log(logger)
    def get_child_tasks(self, parent_id, relation_type=None, current_user_id=None):
        """Returns subtasks of specified task.
        Checks user for authorization.

        Parameters
        ----------
        parent_id : integer
        relation_type : TaskRelationType(Enum) (defaults to None)
        current_user_id : integer (defaults to None)

        Returns
        -------
        list(Task)
            Children tasks

        """
        if self.need_check:
            self.authorization.check_permission(parent_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_child_tasks(parent_id, relation_type)

    @exception_log(ex_logger)
    @log(logger)
    def add_task(self, task, current_user_id=None):
        """Adds task to the storage.

        Parameters
        ----------
        task : Task
        current_user_id : integer

        Returns
        -------
        Task
            Added to storage task

        """
        if task.board_id:
            self.existence.check_existence(model.Board, task.board_id)
            if self.need_check:
                self.authorization.check_board_permission(task.board_id, current_user_id,
                                                          enum.BoardPermission.EDITOR.value)

        task = self.storage.add_task(task)

        create_date = model.TaskDate(datetime.now(), enum.TaskDateType.CREATE.value, task.task_id)
        self.storage.add_task_date(create_date)

        if current_user_id:
            task_creator = model.TaskUserRelation(task_id=task.task_id, user_id=current_user_id,
                                                  role=enum.Role.CREATOR.value)
            self.add_task_user_relation(task_creator, current_user_id)
        return task

    @exception_log(ex_logger)
    @log(logger)
    def remove_task(self, task_id, current_user_id=None):
        """Removes task from storage returning it or count of removed rows.

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        Task
            

        """
        self.existence.check_existence(model.Task, task_id)
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        if self.is_archived:
            task = self.storage.get_task(task_id)
            task.status = enum.TaskStatus.ARCHIVED.value
            return self.storage.update_task(task)
        else:
            return self.storage.remove_task(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_task(self, task, current_user_id=None):
        """Update task and return it.

        Parameters
        ----------
        task : Task
        current_user_id : integer

        Returns
        -------
        Task
            Updated task.

        """
        self.existence.check_existence(model.Task, task.task_id)
        if task.board_id:
            self.existence.check_existence(model.Board, task.board_id)
            self.authorization.check_board_permission(task.board_id, current_user_id, enum.BoardPermission.EDITOR.value)

        if self.need_check:
            self.authorization.check_permission(task.task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        self.logic.check_block_by_other(task)

        # logic for changing task status/adding or removing requester and finisher
        with session_scope() as session:
            old_task = self.storage.get_task(task.task_id, session=session)
            if old_task.status != enum.TaskStatus.ACTIVE.value and task.status == enum.TaskStatus.ACTIVE.value:
                overdue_dates = self.storage.get_task_dates_filtered(task.task_id,
                                                                     date_types=[enum.TaskDateType.END.value,
                                                                                 enum.TaskDateType.EVENT.value],
                                                                     session=session)
                if overdue_dates and datetime.now() > overdue_dates[0]:
                    task.status = enum.TaskStatus.OVERDUE.value

                relations = self.storage.get_task_user_relations_filtered(task_id=task.task_id,
                                                                          roles=[enum.Role.REQUESTER.value,
                                                                                 enum.Role.FINISHER.value],
                                                                          session=session)
                for relation in relations:
                    self.storage.remove_task_user_relation(relation.relation_id, session=session)

            if old_task.status != enum.TaskStatus.COMPLETED.value and task.status == enum.TaskStatus.COMPLETED.value:
                observers = self.storage.get_task_observers(task.task_id, session=session)
                if observers and (current_user_id not in [obs.user_id for obs in observers]):
                    task.status = enum.TaskStatus.REQUEST.value
                    self.storage.add_task_user_relation(model.TaskUserRelation(enum.Role.REQUESTER.value,
                                                                               task.task_id,
                                                                               current_user_id))
                else:
                    self.storage.add_task_user_relation(model.TaskUserRelation(enum.Role.FINISHER.value,
                                                                               task.task_id,
                                                                               current_user_id))

        updated_task = self.storage.update_task(task)

        if updated_task.status == enum.TaskStatus.COMPLETED.value:
            self.updater.complete_child_tasks(updated_task.task_id)
            self.updater.complete_connected_tasks(updated_task.task_id)
            self.updater.complete_parent_task(updated_task.task_id)
            self.storage.add_task_date(model.TaskDate(datetime.now(),
                                                      enum.TaskDateType.CLOSE.value,
                                                      updated_task.task_id))
        elif updated_task.status == enum.TaskStatus.FAILED.value:
            self.storage.add_task_date(model.TaskDate(datetime.now(),
                                                      enum.TaskDateType.CLOSE.value,
                                                      updated_task.task_id))
        else:
            self.storage.add_task_date(model.TaskDate(datetime.now(),
                                                      enum.TaskDateType.EDIT.value,
                                                      updated_task.task_id))
        return updated_task

    @exception_log(ex_logger)
    @log(logger)
    def get_tree_view(self, root_task_id, is_show_connected=False, current_user_id=None):
        """Fills and returns task tree object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        root_task_id : integer
        is_show_connected : bool
            Specify it to load also completing and blocked tasks.
        current_user_id : integer

        Returns
        -------
        TreeView
            Tree of tasks related to each other.

        """
        if self.need_check:
            self.authorization.check_permission(root_task_id, current_user_id, enum.BoardPermission.READER.value)

        with session_scope() as session:
            def fill_node_view(relation):
                node_task_id = relation.child_id
                node_task = self.storage.get_task(node_task_id, session=session)
                node_relations = self.storage.get_task_relations_filtered(
                    parent_id=node_task_id,
                    relation_types=[enum.TaskRelationType.SUBTASK.value],
                    session=session
                )
                node_child_tasks = [fill_node_view(node_relation) for node_relation in node_relations]

                if is_show_connected:
                    relations = self.storage.get_task_relations_filtered(
                        parent_id=node_task_id,
                        relation_types=[enum.TaskRelationType.COMPLETE.value,
                                        enum.TaskRelationType.BLOCK.value],
                        session=session
                    )
                    complete_tasks = list(filter(
                        lambda rel: rel.relation_type == enum.TaskRelationType.COMPLETE.value, relations
                    ))
                    complete_task_ids = [child_relation.child_id for child_relation in complete_tasks]

                    block_tasks = list(filter(
                        lambda rel: rel.relation_type == enum.TaskRelationType.BLOCK.value, relations
                    ))
                    block_task_ids = [child_relation.child_id for child_relation in block_tasks]
                else:
                    complete_task_ids = None
                    block_task_ids = None

                return vmodel.TreeView(node_task, relation,
                                       child_tasks=node_child_tasks,
                                       complete_task_ids=complete_task_ids,
                                       block_task_ids=block_task_ids)

            root_task = self.storage.get_task(root_task_id, session=session)
            root_relations = self.storage.get_task_relations_filtered(
                parent_id=root_task.task_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value],
                session=session
            )
            root_child_tasks = [fill_node_view(root_relation) for root_relation in root_relations]
            root_view = vmodel.TreeView(root_task, child_tasks=root_child_tasks)
        return root_view
    # endregion

    # region plan methods
    @exception_log(ex_logger)
    @log(logger)
    def get_plan(self, plan_id, current_user_id=None):
        """

        Parameters
        ----------
        plan_id : integer
        current_user_id : integer

        Returns
        -------
        Plan

        """
        plan = self.storage.get_plan(plan_id)
        if plan:
            if self.need_check:
                self.authorization.check_permission(plan.task_id, current_user_id, enum.BoardPermission.READER.value)
            return plan
        else:
            raise ex.NotFoundError(model.Plan, plan_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_plans(self, current_user_id):
        """

        Parameters
        ----------
        current_user_id : integer

        Returns
        -------
        list(Plans)

        """
        return self.storage.get_user_plans(current_user_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_plan_view(self, plan_id, current_user_id=None):
        """Fills and returns plan view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        plan_id : integer
        current_user_id : integer

        Returns
        -------
        PlanView

        """
        self.existence.check_existence(model.Plan, plan_id)

        with session_scope() as session:
            plan = self.storage.get_plan(plan_id, session=session)
            task = self.storage.get_task(plan.task_id, session=session)
            board = self.storage.get_board(task.board_id, session=session) if task.board_id else None
            creator = self.storage.get_task_creator(task.task_id, session=session)
            executors = self.storage.get_task_executors(task.task_id, session=session)
            observers = self.storage.get_task_observers(task.task_id, session=session)
            date_create = self.storage.get_task_dates_filtered(task_id=task.task_id, session=session,
                                                               date_types=[enum.TaskDateType.CREATE.value])
            tags = self.storage.get_task_tags(task.task_id, session=session)

        if self.need_check:
            self.authorization.check_permission(task.task_id, current_user_id, enum.BoardPermission.READER.value)
        return vmodel.PlanView(plan, task, board, creator, executors, observers, date_create, tags)

    @exception_log(ex_logger)
    @log(logger)
    def add_plan(self, plan, current_user_id=None):
        """Adds plan to the storage.

        Parameters
        ----------
        plan : Plan
        current_user_id : integer

        Returns
        -------
        Plan

        """
        if self.need_check:
            self.authorization.check_permission(plan.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        self.logic.check_plan(plan)

        task = self.storage.get_task(plan.task_id)
        if task.status != enum.TaskStatus.TEMPLATE.value:
            template_task = model.Task(task.title, task.board_id, task.description,
                                       task.priority, enum.TaskStatus.TEMPLATE.value)
            template_task = self.storage.add_task(template_task)
            with session_scope() as session:
                self.storage.clone_task_users(task.task_id, template_task.task_id, session=session)
                self.storage.clone_task_tags(task.task_id, template_task.task_id, session=session)
            plan.task_id = template_task.task_id

        self.storage.add_task_date(model.TaskDate(datetime.now(), enum.TaskDateType.CREATE.value, plan.task_id))

        return self.storage.add_plan(plan)

    @exception_log(ex_logger)
    @log(logger)
    def remove_plan(self, plan_id, current_user_id=None):
        """

        Parameters
        ----------
        plan_id : integer
        current_user_id : integer

        Returns
        -------
        Plan

        """
        self.existence.check_existence(model.Plan, plan_id)

        plan = self.storage.get_plan(plan_id)

        if self.need_check:
            self.authorization.check_permission(plan.task_id, current_user_id,
                                                enum.BoardPermission.EDITOR.value)

        if self.is_archived:
            plan.status = enum.PlanStatus.ARCHIVED.value
            return self.storage.update_plan(plan)
        else:
            count_deleted = self.storage.remove_plan(plan_id)
            self.storage.remove_task(plan.task_id)
            return count_deleted

    @exception_log(ex_logger)
    @log(logger)
    def update_plan(self, plan, current_user_id=None):
        """

        Parameters
        ----------
        plan : Plan
        current_user_id : integer

        Returns
        -------
        Plan
            

        """
        self.existence.check_existence(model.Plan, plan.plan_id)
        if self.need_check:
            self.authorization.check_permission(plan.task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        old_plan = self.storage.get_plan(plan.plan_id)
        if old_plan.task_id != plan.task_id:
            self.storage.remove_task(old_plan.task_id)

            self.existence.check_existence(model.Task, plan.task_id)
            task = self.storage.get_task(plan.task_id)
            if task.status != enum.TaskStatus.TEMPLATE.value:
                template_task = model.Task(task.title, task.board_id, task.description,
                                           task.priority, enum.TaskStatus.TEMPLATE.value)
                template_task = self.storage.add_task(template_task)
                self.storage.clone_task_users(task.task_id, template_task.task_id)
                self.storage.clone_task_tags(task.task_id, template_task.task_id)
                plan.task_id = template_task.task_id

        if plan.end_type != enum.PlanEndType.NUMBER.value:
            plan.count_iteration = None
        if plan.end_type != enum.PlanEndType.DATE.value:
            plan.end_date = None

        return self.storage.update_plan(plan)

    # endregion

    # region board methods
    @exception_log(ex_logger)
    @log(logger)
    def get_board(self, board_id, current_user_id=None):
        """Returns board specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        board_id : integer
        current_user_id : integer

        Returns
        -------
        Board
            

        """
        board = self.storage.get_board(board_id)
        if board:
            if self.need_check:
                self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.READER.value)
            return board
        else:
            raise ex.NotFoundError(model.Board, board_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_board_view(self, board_id, current_user_id=None):
        """Fills and returns board view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        board_id : integer
        current_user_id : integer

        Returns
        -------
        BoardView
            

        """
        self.existence.check_existence(model.Board, board_id)
        if self.need_check:
            self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.READER.value)

        with session_scope() as session:
            board = self.storage.get_board(board_id, session=session)
            creator = self.storage.get_board_creator(board_id, session=session)
            admins = self.storage.get_board_users(board_id, [enum.BoardPermission.ADMIN.value], session=session)
            editors = self.storage.get_board_users(board_id, [enum.BoardPermission.EDITOR.value], session=session)
            readers = self.storage.get_board_users(board_id, [enum.BoardPermission.READER.value], session=session)
            blocked = self.storage.get_board_users(board_id, [enum.BoardPermission.BLOCKED.value], session=session)
            tasks = self.storage.get_tasks_filtered(board_id=board_id, session=session,
                                                    statuses=[enum.TaskStatus.ACTIVE.value,
                                                              enum.TaskStatus.OVERDUE.value,
                                                              enum.TaskStatus.REQUEST.value,
                                                              enum.TaskStatus.COMPLETED.value,
                                                              enum.TaskStatus.OVERDUE.value])
            messages = self.storage.get_messages_filtered(board_id=board_id)
            dates = self.storage.get_task_dates_filtered(board_id=board_id)

        return vmodel.BoardView(board, creator, admins, editors, readers, blocked, tasks, messages, dates)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_boards(self, current_user_id, status=None, board_type=None, title=None):
        """

        Parameters
        ----------
        current_user_id : integer
        status : BoardStatus(Enum)
        board_type : BoardType(Enum)
        title: str
        Returns
        -------
        list(Board)
            

        """
        return self.storage.get_user_boards(current_user_id, status, board_type, title)

    @exception_log(ex_logger)
    @log(logger)
    def add_board(self, board, current_user_id=None):
        """Adds board to the storage.

        Parameters
        ----------
        board : Board
        current_user_id :  BoardStatus(Enum)

        Returns
        -------
        Board
            

        """
        board = self.storage.add_board(board)
        creator = model.UserBoardRelation(user_id=current_user_id,
                                          board_id=board.board_id,
                                          permission=enum.BoardPermission.CREATOR.value)

        self.storage.add_user_board_relation(creator)
        return board

    @exception_log(ex_logger)
    @log(logger)
    def remove_board(self, board_id, current_user_id=None):
        """

        Parameters
        ----------
        board_id : integer
        current_user_id : integer

        Returns
        -------
        Board
            

        """
        self.existence.check_existence(model.Board, board_id)
        if self.need_check:
            self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.ADMIN.value)

        if self.is_archived:
            board = self.storage.get_board(board_id)
            board.status = enum.BoardStatus.ARCHIVED.value
            return self.storage.update_board(board)
        else:
            return self.storage.remove_board(board_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_board(self, board, current_user_id=None):
        """

        Parameters
        ----------
        board : Board
        current_user_id : integer

        Returns
        -------
        Board
            Description of returned object

        """
        if self.need_check:
            self.authorization.check_board_permission(board.board_id, current_user_id, enum.BoardPermission.ADMIN.value)
        self.existence.check_existence(model.Board, board.board_id)

        old_board = self.storage.get_board(board.board_id)
        if old_board.board_type != enum.BoardType.SINGLE.value and board.board_type == enum.BoardType.SINGLE.value:
            self.transform_to_single_board(board.board_id, current_user_id)

        return self.storage.update_board(board)

    # endregion

    # region task_date methods
    @exception_log(ex_logger)
    @log(logger)
    def get_task_date(self, task_date_id, current_user_id=None):
        """Returns task date specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        task_date_id : integer
        current_user_id : integer

        Returns
        -------
        TaskDate
            

        """
        task_date = self.storage.get_task_date(task_date_id)
        if task_date:
            if self.need_check:
                self.authorization.check_permission(task_date.task_id, current_user_id,
                                                    enum.BoardPermission.READER.value)
            return task_date
        else:
            raise ex.NotFoundError(model.TaskDate, task_date_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_date_view(self, task_date_id, current_user_id=None):
        """Fills and returns date view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        task_date_id : integer
        current_user_id : integer

        Returns
        -------
        TaskDateView
            

        """
        self.existence.check_existence(model.TaskDate, task_date_id)

        with session_scope() as session:
            task_date = self.storage.get_task_date(task_date_id, session=session)
            task = self.storage.get_task(task_date.task_id, session=session)

        if self.need_check:
            self.authorization.check_permission(task_date.task_id, current_user_id, enum.BoardPermission.READER.value)
        return vmodel.TaskDateView(task_date, task)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_dates_filtered(self, task_id, date_types=None, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        date_types : integer
        current_user_id : integer

        Returns
        -------
        list(TaskDate)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_dates_filtered(task_id=task_id, date_types=date_types)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_dates(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(TaskDate)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_dates(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def add_task_date(self, task_date, current_user_id=None):
        """Adds task date to the storage.

        Parameters
        ----------
        task_date : TaskDate
        current_user_id : integer

        Returns
        -------
        TaskDate
            

        """
        if self.need_check:
            self.authorization.check_permission(task_date.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        self.logic.check_task_date(task_date, self.storage.get_task_dates(task_date.task_id))
        return self.storage.add_task_date(task_date)

    @exception_log(ex_logger)
    @log(logger)
    def add_task_dates(self, task_dates, current_user_id=None):
        """Adds task dates to the storage.

        Parameters
        ----------
        task_dates : list(TaskDate)
        current_user_id : integer

        """
        task_dates = list(filter(lambda date: date is not None, task_dates))
        with session_scope() as session:
            for task_date in task_dates:
                if self.need_check:
                    self.authorization.check_permission(task_date.task_id, current_user_id,
                                                        enum.BoardPermission.EDITOR.value)
                self.logic.check_task_date(task_date, task_dates)
                self.storage.add_task_date(task_date, session=session)

    @exception_log(ex_logger)
    @log(logger)
    def remove_task_date(self, task_date_id, current_user_id=None):
        """

        Parameters
        ----------
        task_date_id : integer
        current_user_id : integer

        Returns
        -------
        TaskDate
            

        """
        self.existence.check_existence(model.TaskDate, task_date_id)
        task_date = self.storage.get_task_date(task_date_id)
        if self.need_check:
            self.authorization.check_permission(task_date.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.remove_task_date(task_date_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_task_date(self, task_date, current_user_id=None):
        """

        Parameters
        ----------
        task_date :TaskDate
        current_user_id : integer

        Returns
        -------
        TaskDate
            

        """
        self.existence.check_existence(model.TaskDate, task_date.task_date_id)
        self.logic.check_task_date(task_date, self.storage.get_task_dates(task_date.task_id))
        task_date = self.storage.get_task_date(task_date.task_date_id)
        if self.need_check:
            self.authorization.check_permission(task_date.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.update_task_date(task_date)

    # endregion

    # region reminder methods
    @exception_log(ex_logger)
    @log(logger)
    def get_reminder(self, reminder_id, current_user_id=None):
        """Returns reminder specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        reminder_id : integer
        current_user_id : integer

        Returns
        -------
        Reminder
            

        """
        reminder = self.storage.get_reminder(reminder_id)
        if reminder:
            if self.need_check:
                self.authorization.check_permission(reminder.task_id, current_user_id,
                                                    enum.BoardPermission.READER.value)
            return reminder
        else:
            raise ex.NotFoundError(model.Reminder, reminder_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_reminder_view(self, reminder_id, current_user_id=None):
        """Fills and returns reminder view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        reminder_id : integer
        current_user_id : integer

        Returns
        -------
        ReminderView
            

        """
        self.existence.check_existence(model.Reminder, reminder_id)

        with session_scope() as session:
            reminder = self.storage.get_reminder(reminder_id, session=session)
            task = self.storage.get_task(reminder.task_id, session=session)

        if self.need_check:
            self.authorization.check_permission(task.task_id, current_user_id, enum.BoardPermission.READER.value)
        return vmodel.ReminderView(reminder, task)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_reminders(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(Reminder)

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)

        return self.storage.get_task_reminders(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_reminders(self, current_user_id):
        """

        Parameters
        ----------
        current_user_id : integer

        Returns
        -------
        list(Reminder)

        """
        return self.storage.get_user_reminders(current_user_id)

    @exception_log(ex_logger)
    @log(logger)
    def add_reminder(self, reminder, current_user_id=None):
        """Adds reminder to the storage.

        Parameters
        ----------
        reminder : integer
        current_user_id : integer

        Returns
        -------
        Reminder

        """
        if self.need_check:
            self.authorization.check_permission(reminder.task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        if reminder.end_type == enum.ReminderEndType.DATE.value and reminder.end_date < reminder.start_date:
            raise ex.LogicError("Start date of the reminder can't be bigger then end date.")

        return self.storage.add_reminder(reminder)

    @exception_log(ex_logger)
    @log(logger)
    def remove_reminder(self, reminder_id, current_user_id=None):
        """

        Parameters
        ----------
        reminder_id : integer
        current_user_id : integer

        Returns
        -------
        count_removed: integer

        """
        self.existence.check_existence(model.Reminder, reminder_id)
        reminder = self.storage.get_reminder(reminder_id)
        if self.need_check:
            self.authorization.check_permission(reminder.task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        if self.is_archived:
            reminder.status = enum.ReminderStatus.ARCHIVED.value
            return self.storage.update_reminder(reminder)
        else:
            return self.storage.remove_reminder(reminder_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_reminder(self, reminder, current_user_id=None):
        """

        Parameters
        ----------
        reminder : Reminder
        current_user_id : integer

        Returns
        -------
        Reminder

        """
        if self.need_check:
            self.authorization.check_permission(reminder.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        self.existence.check_existence(model.Reminder, reminder.reminder_id)

        if reminder.end_type != enum.ReminderEndType.NUMBER.value:
            reminder.count_iteration = None
        if reminder.end_type != enum.ReminderEndType.DATE.value:
            reminder.end_date = None

        return self.storage.update_reminder(reminder)

    # endregion

    # region task_message methods
    @exception_log(ex_logger)
    @log(logger)
    def get_message(self, message_id, current_user_id=None):
        """Returns message specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        message_id : integer
        current_user_id : integer

        Returns
        -------
        Message

        """
        message = self.storage.get_message(message_id)
        if message:
            if self.need_check:
                self.authorization.check_permission(message.task_id, current_user_id, enum.BoardPermission.READER.value)
            return message
        else:
            raise ex.NotFoundError(model.Message, message_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_message_view(self, message_id, current_user_id=None):
        """

        Parameters
        ----------
        message_id : integer
        current_user_id : integer

        Returns
        -------
        MessageView
        """
        self.existence.check_existence(model.Message, message_id)
        with session_scope() as session:
            message = self.storage.get_message(message_id, session=session)
            user = self.storage.get_user(message.user_id, session=session) if message.user_id else None
            task = self.storage.get_task(message.task_id, session=session)

        if self.need_check:
            self.authorization.check_permission(task.task_id, current_user_id, enum.BoardPermission.READER.value)
        return vmodel.MessageView(user, message, task)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_messages(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(Message)

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_messages_filtered(task_id=task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_messages(self, current_user_id=None, date_from=None, date_to=None, task_id=None, seen=None):
        """

        Parameters
        ----------
        current_user_id : integer
        date_from : TaslDate
        date_to : TaskDate
        task_id : integer
        seen : boolean

        Returns
        -------
        list(Message)

        """
        return self.storage.get_messages_filtered(user_id=current_user_id, date_from=date_from,
                                                  date_to=date_to, task_id=task_id, seen=seen)

    @exception_log(ex_logger)
    @log(logger)
    def add_message(self, message, current_user_id=None):
        """Adds message to the storage.

        Parameters
        ----------
        message : Message
        current_user_id : integer

        Returns
        -------
        Message

        """
        if self.need_check:
            self.authorization.check_permission(message.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.add_message(message)

    @exception_log(ex_logger)
    @log(logger)
    def remove_message(self, message_id, current_user_id=None):
        """

        Parameters
        ----------
        message_id : integer
        current_user_id : integer

        Returns
        -------
        count_removed: integer

        """
        message = self.storage.get_message(message_id)
        if self.need_check:
            self.authorization.check_permission(message.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.remove_message(message_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_message(self, message, current_user_id=None):
        """

        Parameters
        ----------
        message : Message
        current_user_id : integer

        Returns
        -------
        Message

        """
        self.existence.check_existence(model.Message, message.message_id)
        if self.need_check:
            self.authorization.check_permission(message.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.update_message(message)

    # endregion

    # region task_relation methods
    def get_task_relation(self, relation_id, current_user_id=None):
        """Returns task-task relation specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        TaskRelation

        """
        relation = self.storage.get_task_relation(relation_id)
        if relation:
            if self.need_check:
                self.authorization.check_permission(relation.parent_id, current_user_id,
                                                    enum.BoardPermission.READER.value)
            return relation
        else:
            raise ex.NotFoundError(model.TaskRelation, relation_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_relation_filtered(self, parent_id=None, child_id=None, current_user_id=None):
        """

        Parameters
        ----------
        parent_id : integer
        child_id : integer
        current_user_id : integer

        Returns
        -------
        TaskRelation

        """
        if parent_id and self.need_check:
            self.authorization.check_permission(parent_id, current_user_id, enum.BoardPermission.READER.value)
        elif child_id and self.need_check:
            self.authorization.check_permission(child_id, current_user_id, enum.BoardPermission.READER.value)
        elif not parent_id and not child_id:
            raise ValueError("You must specify parent_id or child_id.")
        return self.storage.get_task_relations_filtered(parent_id=parent_id,
                                                        child_id=child_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_relation_view(self, task_relation_id, current_user_id=None):
        """Fills and returns task-task relation view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        task_relation_id : integer
        current_user_id : integer

        Returns
        -------
        TaskRelationView

        """
        self.existence.check_existence(model.TaskRelation, task_relation_id)

        with session_scope() as session:
            relation = self.storage.get_task_relation(task_relation_id, session=session)
            parent_task = self.storage.get_task(relation.parent_id, session=session)
            child_task = self.storage.get_task(relation.child_id, session=session)

        if self.need_check:
            self.authorization.check_permission(parent_task.task_id, current_user_id,
                                                enum.BoardPermission.READER.value)
        return vmodel.TaskRelationView(relation, parent_task, child_task)

    @exception_log(ex_logger)
    @log(logger)
    def add_task_relation(self, task_relation, current_user_id=None):
        """Adds task-task relation to the storage.

        Parameters
        ----------
        task_relation : TaskRelation
        current_user_id : integer

        Returns
        -------
        TaskRelation
        """
        self.duplication.check_task_relation(task_relation)
        self.logic.check_task_relation(task_relation)
        if self.need_check:
            self.authorization.check_permission(task_relation.parent_id, current_user_id,
                                                enum.BoardPermission.EDITOR.value)
        return self.storage.add_task_relation(task_relation)

    @exception_log(ex_logger)
    @log(logger)
    def remove_task_relation(self, relation_id, current_user_id=None):
        """

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        TaskRelation

        """
        relation = self.storage.get_task_relation(relation_id)
        if self.need_check:
            self.authorization.check_permission(relation.parent_id, current_user_id,
                                                enum.BoardPermission.EDITOR.value)
        return self.storage.remove_task_relation(relation_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_task_relation(self, task_relation, current_user_id=None):
        """

        Parameters
        ----------
        task_relation : TaskRelation
        current_user_id : integer

        Returns
        -------
        TaskRelation

        """
        self.existence.check_existence(model.TaskRelation, task_relation.relation_id)
        if self.need_check:
            self.authorization.check_permission(task_relation.parent_id, current_user_id,
                                                enum.BoardPermission.EDITOR.value)
        return self.storage.update_task_relation(task_relation)

    # endregion

    # region task_user_relation methods
    @exception_log(ex_logger)
    @log(logger)
    def get_task_user_relation(self, relation_id, current_user_id=None):
        """Returns task-user specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        TaskUserRelation

        """
        relation = self.storage.get_task_user_relation(relation_id)
        if relation:
            if self.need_check:
                self.authorization.check_permission(relation.task_id, current_user_id,
                                                    enum.BoardPermission.READER.value)
            return relation
        else:
            raise ex.NotFoundError(model.TaskUserRelation, relation_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_user_relation_view(self, task_id, user_id, current_user_id=None):
        """Fills and returns task-user relation view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        task_id : integer
        user_id : integer
        current_user_id : integer

        Returns
        -------
        TaskUserRelationView

        """
        self.existence.check_existence(model.Task, task_id)
        self.existence.check_existence(model.User, user_id)

        with session_scope() as session:
            relations = self.storage.get_task_user_relations_filtered(task_id=task_id,
                                                                      user_id=user_id,
                                                                      session=session)
            user = self.storage.get_user(user_id, session=session)
            task = self.storage.get_task(task_id, session=session)
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return vmodel.TaskUserRelationView(relations, task, user)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_user_relations_filtered(self, task_id=None, user_id=None, roles=None, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        user_id : integer
        roles : Role(Enum)
        current_user_id : integer

        Returns
        -------
        list(TaskUserRelation)

        """
        if self.need_check and task_id:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_user_relations_filtered(task_id=task_id,
                                                             user_id=user_id,
                                                             roles=roles)

    @exception_log(ex_logger)
    @log(logger)
    def add_task_user_relation(self, task_user, current_user_id=None):
        """Adds task-user to the storage.

        Parameters
        ----------
        task_user : TaskUserRelation
        current_user_id : integer

        Returns
        -------
        TaskUserRelation

        """
        self.duplication.check_task_user_relation(task_user)
        self.logic.check_task_user_relation(task_user)
        self.existence.check_existence(model.User, task_user.user_id)
        self.existence.check_existence(model.Task, task_user.task_id)
        if self.need_check:
            self.authorization.check_permission(task_user.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.add_task_user_relation(task_user)

    @exception_log(ex_logger)
    @log(logger)
    def remove_task_user_relation(self, relation_id, current_user_id=None):
        """

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        count_removed: integer

        """
        relation = self.storage.get_task_user_relation(relation_id)
        if self.need_check:
            self.authorization.check_permission(relation.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.remove_task_user_relation(relation_id)

    # endregion

    # region user_board_relation methods
    @exception_log(ex_logger)
    @log(logger)
    def get_user_board_relation(self, relation_id, current_user_id=None):
        """Returns user-board specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        UserBoardRelation

        """
        relation = self.storage.get_user_board_relation(relation_id)
        if relation:
            if self.need_check:
                self.authorization.check_board_permission(relation.board_id, current_user_id,
                                                          enum.BoardPermission.ADMIN.value)
            return relation
        else:
            raise ex.NotFoundError(model.UserBoardRelation, relation_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_board_relations_filtered(self, user_id=None, board_id=None, permission=None, current_user_id=None):
        """

        Parameters
        ----------
        user_id : integer
        board_id : integer
        permission : Permission(Enum)
        current_user_id : integer

        Returns
        -------
        list(UserBoardRelation)

        """
        if self.need_check and board_id:
            self.authorization.check_board_permission(board_id, current_user_id,
                                                      enum.BoardPermission.READER.value)
        return self.storage.get_user_board_relations_filtered(user_id, board_id, permission)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_board_relation_view(self, user_board_relation_id, current_user_id=None):
        """Fills and returns user-board relation view object for representation. Checks
        user authorization and task existence.

        Parameters
        ----------
        user_board_relation_id : integer
        current_user_id : integer

        Returns
        -------
        UserBoardRelationView

        """
        self.existence.check_existence(model.UserBoardRelation, user_board_relation_id)

        with session_scope() as session:
            relation = self.storage.get_user_board_relation(user_board_relation_id, session=session)
            user = self.storage.get_user(relation.user_id, session=session)
            board = self.storage.get_board(relation.board_id, session=session)

        if self.need_check:
            self.authorization.check_board_permission(relation.board_id, current_user_id,
                                                      enum.BoardPermission.ADMIN.value)
        return vmodel.UserBoardRelationView(relation, user, board)

    @exception_log(ex_logger)
    @log(logger)
    def add_user_board_relation(self, relation, current_user_id=None):
        """Adds user-board relation to the storage.

        Parameters
        ----------
        relation : UserBoardRelation
        current_user_id : integer

        Returns
        -------
        UserBoardRelation

        """
        self.logic.check_user_board_relation(relation)
        self.duplication.check_user_board_relation(relation)
        if self.need_check:
            self.authorization.check_board_permission(relation.board_id, current_user_id,
                                                      enum.BoardPermission.ADMIN.value)
        return self.storage.add_user_board_relation(relation)

    @exception_log(ex_logger)
    @log(logger)
    def remove_user_board_relation(self, relation_id, current_user_id=None):
        """

        Parameters
        ----------
        relation_id : integer
        current_user_id : integer

        Returns
        -------
        UserBoardRelation

        """
        relation = self.storage.get_user_board_relation(relation_id)
        if self.need_check:
            self.authorization.check_board_permission(relation.board_id, current_user_id,
                                                      enum.BoardPermission.ADMIN.value)

        task_relations = self.storage.get_task_user_relations_filtered(user_id=relation.user_id,
                                                                       board_id=relation.board_id,
                                                                       roles=[enum.Role.EXECUTOR.value,
                                                                              enum.Role.OBSERVER.value])
        for task_relation in task_relations:
            self.storage.remove_task_user_relation(task_relation.relation_id)

        return self.storage.remove_user_board_relation(relation_id)

    @exception_log(ex_logger)
    @log(logger)
    def update_user_board_relation(self, relation, current_user_id=None):
        """

        Parameters
        ----------
        relation : UserBoardRelation
        current_user_id : integer

        Returns
        -------
        UserBoardRelation
            

        """
        self.existence.check_existence(model.UserBoardRelation, relation.relation_id)
        if self.need_check:
            self.authorization.check_board_permission(relation.board_id, current_user_id,
                                                      enum.BoardPermission.ADMIN.value)
        if relation.permission < enum.BoardPermission.EDITOR.value:
            task_relations = self.storage.get_task_user_relations_filtered(user_id=relation.user_id,
                                                                           board_id=relation.board_id,
                                                                           roles=[enum.Role.EXECUTOR.value,
                                                                                  enum.Role.OBSERVER.value])
            for task_relation in task_relations:
                self.storage.remove_task_user_relation(task_relation.relation_id)

        return self.storage.update_user_board_relation(relation)

    # endregion

    # region tag methods
    @exception_log(ex_logger)
    @log(logger)
    def get_tag(self, tag_id, current_user_id=None):
        """Returns tag specified by its unique id. Checks
        user permissions and task existence.

        Parameters
        ----------
        tag_id : integer
        current_user_id : integer

        Returns
        -------
        Tag
            

        """
        self.existence.check_existence(model.Tag, tag_id)
        tag = self.storage.get_tag(tag_id)
        if self.need_check:
            self.authorization.check_permission(tag.task_id, current_user_id, enum.BoardPermission.READER.value)
        return tag

    @exception_log(ex_logger)
    @log(logger)
    def get_user_tags(self, current_user_id, text=None):
        """

        Parameters
        ----------
        current_user_id : integer

        Returns
        -------
        list(Tag)
            

        """
        return self.storage.get_user_tags(current_user_id, text)

    @exception_log(ex_logger)
    @log(logger)
    def get_board_tags(self, board_id, current_user_id=None):
        """

        Parameters
        ----------
        board_id : integer
        current_user_id : integer

        Returns
        -------
        list(Tag)
            

        """
        if self.need_check:
            self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_board_tags(board_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_tags(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(Tag)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_tags(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def add_tag(self, tag, current_user_id=None):
        """Adds tag to the storage.

        Parameters
        ----------
        tag : Tag
        current_user_id : integer

        Returns
        -------
        Tag
            

        """
        if self.need_check:
            self.authorization.check_permission(tag.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        self.duplication.check_task_tag(tag)
        return self.storage.add_tag(tag)

    @exception_log(ex_logger)
    @log(logger)
    def add_tags(self, tags, current_user_id=None):
        """Adds tags to the storage.

        Parameters
        ----------
        tags : list(Tag)
        current_user_id : integer

        """
        if tags is not None:
            self.existence.check_existence(model.Task, tags[0].task_id)
        if self.need_check:
            self.authorization.check_permission(tags[0].task_id, current_user_id, enum.BoardPermission.EDITOR.value)

        for tag in tags:
            self.storage.add_tag(tag)

    @exception_log(ex_logger)
    @log(logger)
    def remove_tag(self, tag_id, current_user_id=None):
        """

        Parameters
        ----------
        tag_id : integer
        current_user_id : integer

        Returns
        -------
        Tag
            

        """
        self.existence.check_existence(model.Tag, tag_id)
        tag = self.storage.get_tag(tag_id)
        if self.need_check:
            self.authorization.check_permission(tag.task_id, current_user_id, enum.BoardPermission.EDITOR.value)
        return self.storage.remove_tag(tag_id)

    @exception_log(ex_logger)
    @log(logger)
    def remove_tag_by_text(self, task_id, text, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        text : string
        current_user_id : integer

        Returns
        -------
        Tag
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.remove_tag_by_text(task_id, text)

    # endregion

    # region users methods
    @exception_log(ex_logger)
    @log(logger)
    def get_user(self, user_id):
        """Returns user specified by its unique id.

        Parameters
        ----------
        user_id : integer

        Returns
        -------
        User
            

        """
        self.existence.check_existence(model.User, user_id)
        return self.storage.get_user(user_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_users(self):
        """Returns all users.

        Returns
        -------
        list(User)


        """
        return self.storage.get_users()

    @exception_log(ex_logger)
    @log(logger)
    def get_user_by_email(self, email):
        """

        Parameters
        ----------
        email : string

        Returns
        -------
        User
            

        """
        user = self.storage.get_user_by_email(email)
        if user:
            return user
        else:
            raise ex.NotFoundError(model.User, email)

    @exception_log(ex_logger)
    @log(logger)
    def get_user_by_login(self, login):
        """Returns task specified by its unique login.

        Parameters
        ----------
        login : string

        Returns
        -------
        User
            

        """
        user = self.storage.get_user_by_login(login)
        if user:
            return user
        else:
            raise ex.NotFoundError(model.User, login)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_creator(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        User
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_creator(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_executors(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(User)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_executors(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_observers(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(User)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_observers(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_requester(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        list(User)
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_requester(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_task_finisher(self, task_id, current_user_id=None):
        """

        Parameters
        ----------
        task_id : integer
        current_user_id : integer

        Returns
        -------
        User
            

        """
        if self.need_check:
            self.authorization.check_permission(task_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_task_finisher(task_id)

    @exception_log(ex_logger)
    @log(logger)
    def get_board_users(self, board_id, permissions, current_user_id=None):
        """

        Parameters
        ----------
        board_id : integer
        permissions : Permission(Enum)
        current_user_id : integer

        Returns
        -------
        list(Users)
            Users of the board.

        """
        if self.need_check:
            self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.READER.value)
        return self.storage.get_board_users(board_id, permissions)

    @exception_log(ex_logger)
    @log(logger)
    def add_user(self, user):
        """Adds user to the storage.

        Parameters
        ----------
        user : User

        Returns
        -------
        User
            Added user containing generic id.

        """
        self.logic.check_user(user)
        return self.storage.add_user(user)

    @exception_log(ex_logger)
    @log(logger)
    def remove_user(self, user_id):
        """

        Parameters
        ----------
        user_id : integer

        Returns
        -------
        count_removed
            If count_removed > 0 removing ended successful.

        """
        self.existence.check_existence(model.User, user_id)
        count_removed = self.storage.remove_user(user_id)
        if count_removed:
            return count_removed
        else:
            raise ex.NotFoundError

    @exception_log(ex_logger)
    @log(logger)
    def remove_user_by_email(self, email):
        """

        Parameters
        ----------
        email : string

        Returns
        -------
        count_removed
            If count_removed > 0 removing ended successful

        """
        count_removed = self.storage.remove_user_by_email(email)
        if count_removed:
            return count_removed
        else:
            raise ex.NotFoundError

    @exception_log(ex_logger)
    @log(logger)
    def remove_user_by_login(self, login):
        """

        Parameters
        ----------
        login : string
            User's login

        Returns
        -------
        count_removed
            If count_removed > 0 removing ended successful

        """
        count_removed = self.storage.remove_user_by_login(login)
        if count_removed:
            return count_removed
        else:
            raise ex.NotFoundError

    @exception_log(ex_logger)
    @log(logger)
    def update_user(self, user):
        """

        Parameters
        ----------
        user : User

        Returns
        -------
        User
            Updated user.

        """
        self.existence.check_existence(model.User, user.user_id)
        self.logic.check_user(user)
        return self.storage.update_user(user)

    # endregion

    @exception_log(ex_logger)
    @log(logger)
    def clone_task_users(self, from_task_id, to_task_id):
        """Clone task users crating plan task or crating task by plan task.

        Parameters
        ----------
        from_task_id : integer
            Task whose users will be cloned
        to_task_id : integer - task that will receive users

        """
        task_users = self.storage.get_task_user_relations_filtered(task_id=from_task_id)
        for task_user in task_users:
            if task_user.role in [enum.Role.OBSERVER.value, enum.Role.EXECUTOR.value, enum.Role.CREATOR.value]:
                task_user.task_id = to_task_id
                self.storage.add_task_user_relation(task_user)

    @exception_log(ex_logger)
    @log(logger)
    def clone_task_tags(self, from_task_id, to_task_id):
        """Clone task tags crating plan task or crating task by plan task.

        Parameters
        ----------
        from_task_id : integer - task whose tags will be cloned
        to_task_id : integer - task that will receive tags

        """
        task_tags = self.storage.get_tags_filtered(task_id=from_task_id)
        for task_tag in task_tags:
            task_tag.task_id = to_task_id
            self.storage.add_tag(task_tag)

    @exception_log(ex_logger)
    @log(logger)
    def transform_to_single_board(self, board_id, current_user_id=None):
        """Method needed to transform board to single, in other words to delete other user_relation except creator.

        Parameters
        ----------
        board_id : integer
        current_user_id : integer - creator of the board

        """
        if self.need_check:
            self.authorization.check_board_permission(board_id, current_user_id, enum.BoardPermission.CREATOR.value)
        with session_scope() as session:
            relations = self.storage.get_user_board_relations_filtered(board_id=board_id, session=session)
            for relation in relations:
                if relation.permission != enum.BoardPermission.CREATOR.value:
                    self.storage.remove_user_board_relation(relation.relation_id, session=session)

            relations = self.storage.get_task_user_relations_filtered(board_id=board_id, session=session)
            for relation in relations:
                if relation.role == enum.Role.CREATOR.value:
                    relation.user_id = current_user_id
                    self.storage.update_task_user_relation(relation, session=session)
                else:
                    self.storage.remove_task_user_relation(relation.relation_id, session=session)

    @staticmethod
    @exception_log(ex_logger)
    @log(logger)
    def set_up_database(data_storage_class, connection_string):
        """Method to initial set up database if necessary

        Args:
            data_storage_class: SQLAlchemyRepos - ORM for connection with database
            connection_string: string - password, user, host - connection string to database

        Returns:
            connection_string: string - updated connection string with name of the database
        """

        return data_storage_class.set_up_database(connection_string)

import unittest

from datetime import date
from datetime import datetime
from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.services import exceptions as ex
from irontasklib.services.services import Services
from irontasklib.storage.sqlalchemy_repos import SQLAlchemyRepos


class TestTasks(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))
        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.service.add_tag(model.Tag(self.task.task_id, "tag"), self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_task(self):
        added_task = self.service.get_task(self.task.task_id, self.user_admin.user_id)

        self.assertEqual(self.task.title, added_task.title, "Task title equality")
        self.assertEqual(self.task.description, added_task.description, "Task description equality")
        self.assertEqual(self.task.board_id, added_task.board_id, "Task board_id equality")
        self.assertEqual(self.task.status, added_task.status, "Task status equality")
        self.assertEqual(self.task.priority, added_task.priority, "Task priority equality")

    def test_get_task_by_tag(self):
        added_task = self.service.get_tasks_by_tags(["tag"], self.user_admin.user_id)[0]

        self.assertEqual(self.task.title, added_task.title, "Task title equality")
        self.assertEqual(self.task.description, added_task.description, "Task description equality")
        self.assertEqual(self.task.board_id, added_task.board_id, "Task board_id equality")
        self.assertEqual(self.task.status, added_task.status, "Task status equality")
        self.assertEqual(self.task.priority, added_task.priority, "Task priority equality")

    def test_get_user_tasks(self):
        added_task = self.service.get_user_tasks(current_user_id=self.user_admin.user_id)[0]

        self.assertEqual(self.task.title, added_task.title, "Task title equality")
        self.assertEqual(self.task.description, added_task.description, "Task description equality")
        self.assertEqual(self.task.board_id, added_task.board_id, "Task board_id equality")
        self.assertEqual(self.task.status, added_task.status, "Task status equality")
        self.assertEqual(self.task.priority, added_task.priority, "Task priority equality")

    def test_get_current_user_tasks(self):
        added_task = self.service.get_current_user_tasks(current_user_id=self.user_admin.user_id)[0]
        self.assertEqual(self.task.title, added_task.title, "Task title equality")
        self.assertEqual(self.task.description, added_task.description, "Task description equality")
        self.assertEqual(self.task.board_id, added_task.board_id, "Task board_id equality")
        self.assertEqual(self.task.status, added_task.status, "Task status equality")
        self.assertEqual(self.task.priority, added_task.priority, "Task priority equality")

        self.task.status = enum.TaskStatus.COMPLETED.value
        self.service.update_task(self.task, self.user_admin.user_id)
        added_task = self.service.get_current_user_tasks(self.user_admin.user_id)
        self.assertFalse(added_task)

    def test_get_parent_task(self):
        self.child_task = self.service.add_task(model.Task("child", self.board.board_id, "description"),
                                                self.user_admin.user_id)
        self.task_relation = self.service.add_task_relation(model.TaskRelation(enum.TaskRelationType.SUBTASK.value,
                                                                               self.task.task_id,
                                                                               self.child_task.task_id),
                                                            self.user_admin.user_id)
        parent_task = self.service.get_parent_tasks(self.child_task.task_id,
                                                    current_user_id=self.user_admin.user_id)[0]
        self.assertEqual(parent_task.task_id, self.task.task_id)
        self.assertEqual(parent_task.title, self.task.title)

    def test_get_child_task(self):
        self.child_task = self.service.add_task(model.Task("child", self.board.board_id, "description"),
                                                self.user_admin.user_id)
        self.task_relation = self.service.add_task_relation(model.TaskRelation(enum.TaskRelationType.SUBTASK.value,
                                                                               self.task.task_id,
                                                                               self.child_task.task_id),
                                                            self.user_admin.user_id)
        child_task = self.service.get_child_tasks(self.task.task_id,
                                                  current_user_id=self.user_admin.user_id)[0]
        self.assertEqual(child_task.task_id, self.child_task.task_id)
        self.assertEqual(child_task.title, self.child_task.title)

    def test_add_remove_task(self):
        new_task = self.service.add_task(model.Task("new", self.board.board_id, "description"),
                                         self.user_admin.user_id)
        self.assertIsNotNone(new_task.task_id)
        gotten_task = self.service.get_task(new_task.task_id,
                                            self.user_admin.user_id)
        self.assertEqual(new_task.task_id, gotten_task.task_id)
        self.assertEqual(new_task.title, gotten_task.title)

        self.assertTrue(self.service.remove_task(new_task.task_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_task(new_task.task_id, self.user_admin.user_id)

    def test_update_task(self):
        args = {
            model.TASK_TITLE: "changed_title",
            model.TASK_DESCRIPTION: "changed_description",
            model.TASK_BOARD_ID: None,
            model.TASK_PRIORITY: enum.TaskPriority.LOWEST.value,
            model.TASK_STATUS: enum.TaskStatus.ACTIVE.value
        }
        task = self.service.get_task(self.task.task_id, self.user_admin.user_id)
        for key, value in args.items():
            setattr(task, key, value)

        updated_task = self.service.update_task(task, self.user_admin.user_id)

        self.assertEqual(updated_task.title, args[model.TASK_TITLE], "Task title equality")
        self.assertEqual(updated_task.description, args[model.TASK_DESCRIPTION], "Task description equality")
        self.assertIsNone(updated_task.board_id, "Task board_id equality")
        self.assertEqual(updated_task.status, args[model.TASK_STATUS], "Task status equality")
        self.assertEqual(updated_task.priority, args[model.TASK_PRIORITY], "Task priority equality")


class TestPlan(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))
        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.service.add_tag(model.Tag(self.task.task_id, "tag"),
                             self.user_admin.user_id)
        self.plan = self.service.add_plan(model.Plan(task_id=self.task.task_id,
                                                     start_date=date(2018, 7, 12),
                                                     periodicity={'months': 1},
                                                     end_date=date(2018, 9, 24),
                                                     end_type=enum.PlanEndType.DATE.value),
                                          self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_plan(self.plan.plan_id, self.user_admin.user_id)
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_plan(self):
        gotten_plan = self.service.get_plan(self.plan.plan_id, self.user_admin.user_id)
        self.assertTrue(gotten_plan)
        self.assertEqual(self.plan.task_id, gotten_plan.task_id)

    def test_add_remove_plan(self):
        new_plan = self.service.add_plan(model.Plan(self.task.task_id,
                                                    start_date=date(2018, 7, 12),
                                                    periodicity={'month': 1},
                                                    count_iteration=10,
                                                    end_type=enum.PlanEndType.NUMBER.value),
                                         self.user_admin.user_id)
        self.assertTrue(new_plan.plan_id)
        gotten_plan = self.service.get_plan(new_plan.plan_id, self.user_admin.user_id)
        self.assertTrue(gotten_plan)
        self.assertEqual(new_plan.end_type, gotten_plan.end_type)

        self.assertTrue(self.service.remove_plan(new_plan.plan_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.assertIsNone(self.service.get_plan(new_plan.plan_id, self.user_admin.user_id))

    def test_update_plan(self):
        args = {
            model.PLAN_END_TYPE: enum.PlanEndType.NUMBER.value,
            model.PLAN_COUNT_ITERATION: 3,
            model.PLAN_START: date(2018, 4, 5),
            model.PLAN_PERIODICITY: {'month': 1}
        }
        plan = self.service.get_plan(self.plan.plan_id, self.user_admin.user_id)
        for key, value in args.items():
            setattr(plan, key, value)

        updated_plan = self.service.update_plan(plan, self.user_admin.user_id)

        self.assertEqual(updated_plan.start_date, args[model.PLAN_START])
        self.assertEqual(updated_plan.end_type, args[model.PLAN_END_TYPE])
        self.assertEqual(updated_plan.count_iteration, args[model.PLAN_COUNT_ITERATION])
        self.assertEqual(updated_plan.periodicity, args[model.PLAN_PERIODICITY])
        self.assertIsNone(updated_plan.end_date)


class TestBoards(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.user_usual = self.service.add_user(model.User("test_usual", "test_usual", "test_usual@gmail.com"))
        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="text board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")
        self.service.remove_user_by_login("test_usual")

    def test_get_board(self):
        added_board = self.service.get_board(self.board.board_id, self.user_admin.user_id)

        self.assertEqual(self.board.title, added_board.title, "Board title equality")
        self.assertEqual(self.board.description, added_board.description, "Board description equality")
        self.assertEqual(self.board.board_type, added_board.board_type, "Board type equality")

    def test_get_user_boards(self):
        self.assertFalse(self.service.get_user_boards(self.user_usual.user_id))

        gotten_board = self.service.get_user_boards(self.user_admin.user_id)[0]
        self.assertEqual(gotten_board.board_id, self.board.board_id)
        self.assertEqual(gotten_board.title, self.board.title)

    def test_add_remove_board(self):
        new_board = self.service.add_board(model.Board(title="new board",
                                                       description="text board description",
                                                       board_type=enum.BoardType.PRIVATE.value),
                                           self.user_admin.user_id)

        self.assertIsNotNone(new_board.board_id)

        gotten_board = self.service.get_board(new_board.board_id, self.user_admin.user_id)
        self.assertEqual(gotten_board.board_id, new_board.board_id)
        self.assertEqual(gotten_board.title, new_board.title)

        self.assertTrue(self.service.remove_board(new_board.board_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_board(new_board.board_id, self.user_admin.user_id)

    def test_update_board(self):
        args = {
            model.BOARD_TITLE: "changed title",
            model.BOARD_DESCRIPTION: "changed description",
            model.BOARD_STATUS: enum.BoardStatus.CLOSED.value,
            model.BOARD_TYPE: enum.BoardType.PUBLIC.value
        }
        board = self.service.get_board(self.board.board_id, self.user_admin.user_id)
        for key, value in args.items():
            setattr(board, key, value)

        updated_board = self.service.update_board(board, self.user_admin.user_id)
        self.assertEqual(updated_board.title, args[model.BOARD_TITLE], "Board title equality")
        self.assertEqual(updated_board.description, args[model.BOARD_DESCRIPTION], "Board description equality")
        self.assertEqual(updated_board.status, args[model.BOARD_STATUS], "Board status equality")
        self.assertEqual(updated_board.board_type, args[model.BOARD_TYPE], "Board type equality")


class TestTaskDates(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))
        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.task_date_start = self.service.add_task_date(model.TaskDate(datetime(2018, 7, 5, 12, 30),
                                                                         enum.TaskDateType.START.value,
                                                                         self.task.task_id),
                                                          self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_task_date(self):
        gotten_task_date = self.service.get_task_date(self.task_date_start.task_date_id, self.user_admin.user_id)

        self.assertEqual(self.task_date_start.task_date_id, gotten_task_date.task_date_id)
        self.assertEqual(self.task_date_start.date, gotten_task_date.date)
        self.assertEqual(self.task_date_start.task_id, gotten_task_date.task_id)
        self.assertEqual(self.task_date_start.date_type, gotten_task_date.date_type)

    def test_get_task_dates_filtered(self):
        self.assertFalse(self.service.get_task_dates_filtered(self.task.task_id,
                                                              date_types=[enum.TaskDateType.END.value],
                                                              current_user_id=self.user_admin.user_id))
        gotten_task_date = self.service.get_task_dates_filtered(self.task.task_id,
                                                                date_types=[enum.TaskDateType.START.value],
                                                                current_user_id=self.user_admin.user_id)[0]

        self.assertEqual(self.task_date_start.task_date_id, gotten_task_date.task_date_id)
        self.assertEqual(self.task_date_start.date, gotten_task_date.date)
        self.assertEqual(self.task_date_start.task_id, gotten_task_date.task_id)
        self.assertEqual(self.task_date_start.date_type, gotten_task_date.date_type)

    def test_add_get_task_dates(self):
        self.service.remove_task_date(self.task_date_start.task_date_id, self.user_admin.user_id)
        task_date_end = model.TaskDate(datetime(2018, 12, 5, 11), enum.TaskDateType.END.value, self.task.task_id)
        task_date_start = model.TaskDate(datetime(2018, 11, 5, 13, 34),
                                         enum.TaskDateType.START.value, self.task.task_id)
        task_date_event = model.TaskDate(datetime(2018, 5, 5, 12, 23),
                                         enum.TaskDateType.EVENT.value, self.task.task_id)

        with self.assertRaises(ex.LogicError):
            self.service.add_task_dates([task_date_end, task_date_event], self.user_admin.user_id)

        with self.assertRaises(ex.LogicError):
            self.service.add_task_dates([task_date_start, task_date_event], self.user_admin.user_id)

        with self.assertRaises(ex.LogicError):
            task_date_end = model.TaskDate(datetime(2017, 12, 5, 12, 9), enum.TaskDateType.END.value, self.task.task_id)
            self.service.add_task_dates([task_date_start, task_date_end], self.user_admin.user_id)

        task_date_end = model.TaskDate(datetime(2018, 12, 5, 8, 24), enum.TaskDateType.END.value, self.task.task_id)

        self.service.add_task_dates([task_date_start, task_date_end], self.user_admin.user_id)
        task_dates = self.service.get_task_dates(self.task.task_id, self.user_admin.user_id)
        self.assertTrue(any(task_date.date_type == enum.TaskDateType.START.value for task_date in task_dates))
        self.assertTrue(any(task_date.date_type == enum.TaskDateType.END.value for task_date in task_dates))
        self.assertFalse(any(task_date.date_type == enum.TaskDateType.EVENT.value for task_date in task_dates))
        for task_date in task_dates:
            self.service.remove_task_date(task_date.task_date_id, self.user_admin.user_id)

        self.service.add_task_dates([task_date_event], self.user_admin.user_id)
        task_dates = self.service.get_task_dates(self.task.task_id, self.user_admin.user_id)
        self.assertFalse(any(task_date.date_type == enum.TaskDateType.START.value for task_date in task_dates))
        self.assertFalse(any(task_date.date_type == enum.TaskDateType.END.value for task_date in task_dates))
        self.assertTrue(any(task_date.date_type == enum.TaskDateType.EVENT.value for task_date in task_dates))
        for task_date in task_dates:
            self.service.remove_task_date(task_date.task_date_id, self.user_admin.user_id)

    def test_add_task_date(self):
        with self.assertRaises(ex.LogicError):
            self.service.add_task_date(model.TaskDate(datetime(2018, 8, 15, 14, 25),
                                                      enum.TaskDateType.EVENT.value,
                                                      self.task.task_id),
                                       self.user_admin.user_id)
        task_date_end = self.service.add_task_date(model.TaskDate(datetime(2018, 11, 5, 12, 5),
                                                                  enum.TaskDateType.END.value,
                                                                  self.task.task_id),
                                                   self.user_admin.user_id)
        self.assertIsNotNone(task_date_end.task_date_id)
        gotten_task_date = self.service.get_task_date(task_date_end.task_date_id, self.user_admin.user_id)

        self.assertEqual(task_date_end.task_date_id, gotten_task_date.task_date_id)
        self.assertEqual(task_date_end.date, gotten_task_date.date)

    def test_remove_task_date(self):
        self.assertTrue(self.service.remove_task_date(self.task_date_start.task_date_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_task_date(self.task_date_start.task_date_id, self.user_admin.user_id)

    def test_update_task_date(self):
        args = {
            model.TASK_DATE_TYPE: enum.TaskDateType.END.value,
            model.TASK_DATE: datetime(2018, 7, 9, 17, 14),
        }

        task_date = self.service.get_task_date(self.task_date_start.task_date_id, self.user_admin.user_id)

        for key, value in args.items():
            setattr(task_date, key, value)

        updated_task_date = self.service.update_task_date(task_date, self.user_admin.user_id)

        self.assertEqual(self.task_date_start.task_date_id, updated_task_date.task_date_id)
        self.assertEqual(self.task_date_start.date, updated_task_date.date)
        self.assertEqual(self.task_date_start.task_id, updated_task_date.task_id)
        self.assertEqual(self.task_date_start.date_type, updated_task_date.date_type)


class TestReminder(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PUBLIC.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.reminder = self.service.add_reminder(model.Reminder(start_date=datetime(2018, 9, 12, 23, 30),
                                                                 task_id=self.task.task_id,
                                                                 periodicity={'month': 1},
                                                                 text="it's notification",
                                                                 end_type=enum.ReminderEndType.NUMBER.value,
                                                                 count_iteration=10),
                                                  self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_reminder(self.reminder.reminder_id, self.user_admin.user_id)
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_reminder(self):
        gotten_reminder = self.service.get_reminder(self.reminder.reminder_id, self.user_admin.user_id)

        self.assertEqual(gotten_reminder.reminder_id, self.reminder.reminder_id)
        self.assertEqual(gotten_reminder.start_date, self.reminder.start_date)
        self.assertEqual(gotten_reminder.task_id, self.reminder.task_id)
        self.assertEqual(gotten_reminder.end_type, self.reminder.end_type)

    def test_add_remove_reminder(self):
        new_reminder = self.service.add_reminder(model.Reminder(start_date=datetime(2018, 9, 12, 23, 30),
                                                                task_id=self.task.task_id,
                                                                periodicity={'month': 1},
                                                                text="it's notification",
                                                                end_type=enum.ReminderEndType.DATE.value,
                                                                end_date=datetime(2018, 10, 15, 12, 20)),
                                                 self.user_admin.user_id)

        self.assertIsNotNone(new_reminder.reminder_id)
        gotten_reminder = self.service.get_reminder(new_reminder.reminder_id, self.user_admin.user_id)

        self.assertEqual(gotten_reminder.reminder_id, new_reminder.reminder_id)
        self.assertEqual(gotten_reminder.task_id, new_reminder.task_id)
        self.assertEqual(gotten_reminder.end_type, new_reminder.end_type)

        self.assertTrue(self.service.remove_reminder(new_reminder.reminder_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_reminder(new_reminder.reminder_id, self.user_admin.user_id)

    def test_update_reminder(self):
        args = {
            model.REMINDER_PERIODICITY: {'month': 1},
            model.REMINDER_TEXT: "updated notification",
            model.REMINDER_START: datetime(2018, 8, 30, 17, 45),
            model.REMINDER_END_TYPE: enum.ReminderEndType.NEVER.value
        }

        reminder = self.service.get_reminder(self.reminder.reminder_id, self.user_admin.user_id)
        for key, value in args.items():
            setattr(reminder, key, value)

        updated_reminder = self.service.update_reminder(reminder, self.user_admin.user_id)

        self.assertEqual(updated_reminder.periodicity, args[model.REMINDER_PERIODICITY])
        self.assertEqual(updated_reminder.text, args[model.REMINDER_TEXT])
        self.assertEqual(updated_reminder.start_date, args[model.REMINDER_START])
        self.assertEqual(updated_reminder.end_type, args[model.REMINDER_END_TYPE])
        self.assertIsNone(updated_reminder.count_iteration)


class TestMessages(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PUBLIC.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.message = self.service.add_message(model.Message(text="it's a message",
                                                              task_id=self.task.task_id,
                                                              user_id=self.user_admin.user_id,
                                                              date=datetime.now().replace(microsecond=0)),
                                                self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_message(self.message.message_id, self.user_admin.user_id)
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_message(self):
        gotten_message = self.service.get_message(self.message.message_id, self.user_admin.user_id)

        self.assertEqual(self.message.message_id, gotten_message.message_id)
        self.assertEqual(self.message.task_id, gotten_message.task_id)
        self.assertEqual(self.message.user_id, gotten_message.user_id)
        self.assertEqual(self.message.date, gotten_message.date)

    def test_get_task_messages(self):
        gotten_message = self.service.get_task_messages(self.task.task_id, self.user_admin.user_id)[0]

        self.assertEqual(self.message.message_id, gotten_message.message_id)
        self.assertEqual(self.message.task_id, gotten_message.task_id)
        self.assertEqual(self.message.user_id, gotten_message.user_id)
        self.assertEqual(self.message.date, gotten_message.date)

    def test_get_user_messages(self):
        gotten_message = self.service.get_user_messages(self.user_admin.user_id)[0]

        self.assertEqual(self.message.message_id, gotten_message.message_id)
        self.assertEqual(self.message.task_id, gotten_message.task_id)
        self.assertEqual(self.message.user_id, gotten_message.user_id)
        self.assertEqual(self.message.date, gotten_message.date)

    def test_add_remove_message(self):
        new_message = self.service.add_message(model.Message(text="it's a new message",
                                                             task_id=self.task.task_id,
                                                             user_id=self.user_admin.user_id,
                                                             date=datetime.now().replace(microsecond=0)),
                                               self.user_admin.user_id)
        self.assertIsNotNone(new_message.message_id)

        gotten_message = self.service.get_message(new_message.message_id, self.user_admin.user_id)

        self.assertEqual(new_message.message_id, gotten_message.message_id)
        self.assertEqual(new_message.task_id, gotten_message.task_id)
        self.assertEqual(new_message.user_id, gotten_message.user_id)
        self.assertEqual(new_message.date, gotten_message.date)

        self.assertTrue(self.service.remove_message(new_message.message_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_message(new_message.message_id, self.user_admin.user_id)

    def test_update_message(self):
        args = {
            model.MESSAGE_TEXT: "updated message text",
            model.MESSAGE_DATE: datetime(2018, 12, 23, 18, 15),
            model.MESSAGE_SEEN: True
        }

        message = self.service.get_message(self.message.message_id, self.user_admin.user_id)

        for key, value in args.items():
            setattr(message, key, value)

        updated_message = self.service.update_message(message, self.user_admin.user_id)

        self.assertEqual(updated_message.message_id, self.message.message_id)
        self.assertEqual(updated_message.text, args[model.MESSAGE_TEXT])
        self.assertEqual(updated_message.date, args[model.MESSAGE_DATE])
        self.assertEqual(updated_message.seen, args[model.MESSAGE_SEEN])


class TestTaskRelations(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PUBLIC.value),
                                            self.user_admin.user_id)
        self.parent_task = self.service.add_task(model.Task(title="parent_task",
                                                            description="test task description",
                                                            priority=enum.TaskPriority.HIGHEST.value,
                                                            board_id=self.board.board_id),
                                                 self.user_admin.user_id)
        self.child_task = self.service.add_task(model.Task(title="child_task",
                                                           description="test task description",
                                                           priority=enum.TaskPriority.HIGHEST.value,
                                                           board_id=self.board.board_id),
                                                self.user_admin.user_id)
        self.relation = self.service.add_task_relation(
            model.TaskRelation(relation_type=enum.TaskRelationType.COMPLETE.value,
                               parent_id=self.parent_task.task_id,
                               child_id=self.child_task.task_id),
            self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task_relation(self.relation.relation_id, self.user_admin.user_id)
        self.service.remove_task(self.parent_task.task_id, self.user_admin.user_id)
        self.service.remove_task(self.child_task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_task_relation(self):
        gotten_relation = self.service.get_task_relation(self.relation.relation_id, self.user_admin.user_id)

        self.assertEqual(self.relation.relation_id, gotten_relation.relation_id)
        self.assertEqual(self.relation.parent_id, gotten_relation.parent_id)
        self.assertEqual(self.relation.child_id, gotten_relation.child_id)
        self.assertEqual(self.relation.relation_type, gotten_relation.relation_type)

    def test_get_task_relation_filtered(self):
        gotten_relation = self.service.get_task_relation_filtered(current_user_id=self.user_admin.user_id,
                                                                  parent_id=self.parent_task.task_id,
                                                                  child_id=self.child_task.task_id)[0]

        self.assertEqual(self.relation.relation_id, gotten_relation.relation_id)
        self.assertEqual(self.relation.parent_id, gotten_relation.parent_id)
        self.assertEqual(self.relation.child_id, gotten_relation.child_id)
        self.assertEqual(self.relation.relation_type, gotten_relation.relation_type)

    def test_add_remove_relation(self):
        with self.assertRaises(ex.DuplicationError):
            self.service.add_task_relation(model.TaskRelation(relation_type=enum.TaskRelationType.BLOCK.value,
                                                              parent_id=self.parent_task.task_id,
                                                              child_id=self.child_task.task_id),
                                           self.user_admin.user_id)

        grand_parent_task = self.service.add_task(model.Task(title="parent_task",
                                                             description="test task description",
                                                             priority=enum.TaskPriority.HIGHEST.value,
                                                             board_id=self.board.board_id),
                                                  self.user_admin.user_id)

        new_relation = self.service.add_task_relation(
            model.TaskRelation(relation_type=enum.TaskRelationType.BLOCK.value,
                               parent_id=grand_parent_task.task_id,
                               child_id=self.parent_task.task_id),
            self.user_admin.user_id)

        self.assertIsNotNone(new_relation.relation_id)

        gotten_relation = self.service.get_task_relation(new_relation.relation_id, self.user_admin.user_id)

        self.assertEqual(new_relation.relation_id, gotten_relation.relation_id)
        self.assertEqual(new_relation.parent_id, gotten_relation.parent_id)
        self.assertEqual(new_relation.child_id, gotten_relation.child_id)
        self.assertEqual(new_relation.relation_type, gotten_relation.relation_type)

        self.assertTrue(self.service.remove_task_relation(new_relation.relation_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_task_relation(new_relation.relation_id, self.user_admin.user_id)

    def test_update_relation(self):
        args = {
            model.TASK_RELATION_TYPE: enum.TaskRelationType.SUBTASK.value
        }

        relation = self.service.get_task_relation(self.relation.relation_id, self.user_admin.user_id)

        for key, value in args.items():
            setattr(relation, key, value)

        updated_relation = self.service.update_task_relation(relation, self.user_admin.user_id)

        self.assertEqual(updated_relation.relation_type, args[model.TASK_RELATION_TYPE])


class TestTaskUserRelations(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PUBLIC.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)

        self.executor = self.service.add_user(model.User("executor", "executor", "executor@gmail.com"))
        self.service.add_user_board_relation(
            model.UserBoardRelation(self.board.board_id,
                                    self.executor.user_id,
                                    enum.BoardPermission.EDITOR.value),
            self.user_admin.user_id)
        self.executor_relation = self.service.add_task_user_relation(
            model.TaskUserRelation(enum.Role.EXECUTOR.value,
                                   self.task.task_id,
                                   self.executor.user_id),
            self.user_admin.user_id)

        self.observer = self.service.add_user(model.User("observer", "observer", "observer@gmail.com"))
        self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                     self.observer.user_id,
                                                                     enum.BoardPermission.EDITOR.value),
                                             self.user_admin.user_id)
        self.service.add_task_user_relation(model.TaskUserRelation(enum.Role.OBSERVER.value,
                                                                   self.task.task_id,
                                                                   self.observer.user_id),
                                            self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")
        self.service.remove_user_by_login("executor")
        self.service.remove_user_by_login("observer")

    def test_get_task_user_relation(self):
        task_executor_relation = self.service.get_task_user_relation(self.executor_relation.relation_id,
                                                                     self.user_admin.user_id)

        self.assertEqual(self.executor_relation.task_id, task_executor_relation.task_id)
        self.assertEqual(self.executor_relation.user_id, task_executor_relation.user_id)
        self.assertEqual(self.executor_relation.role, task_executor_relation.role)

    def test_get_task_user_relations_filtered(self):
        task_executor_relation = self.service.get_task_user_relations_filtered(current_user_id=self.user_admin.user_id,
                                                                               task_id=self.task.task_id,
                                                                               user_id=self.executor.user_id)[0]

        self.assertEqual(self.executor_relation.task_id, task_executor_relation.task_id)
        self.assertEqual(self.executor_relation.user_id, task_executor_relation.user_id)
        self.assertEqual(self.executor_relation.role, task_executor_relation.role)

    def test_add_remove_task_user_relation(self):
        new_executor = self.service.add_user(model.User("new_executor", "new_executor", "new_executor@gmail.com"))
        self.service.add_user_board_relation(
            model.UserBoardRelation(self.board.board_id,
                                    new_executor.user_id,
                                    enum.BoardPermission.EDITOR.value),
            self.user_admin.user_id)
        new_executor_relation = self.service.add_task_user_relation(
            model.TaskUserRelation(enum.Role.EXECUTOR.value,
                                   self.task.task_id,
                                   new_executor.user_id),
            self.user_admin.user_id)

        self.assertIsNotNone(new_executor_relation.relation_id)

        gotten_relation = self.service.get_task_user_relation(new_executor_relation.relation_id,
                                                              self.user_admin.user_id)

        self.assertEqual(new_executor_relation.task_id, gotten_relation.task_id)
        self.assertEqual(new_executor_relation.user_id, gotten_relation.user_id)
        self.assertEqual(new_executor_relation.role, gotten_relation.role)

        self.assertTrue(self.service.remove_task_user_relation(gotten_relation.relation_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_task_user_relation(new_executor_relation.relation_id, self.user_admin.user_id)
        self.service.remove_user_by_login("new_executor")

    def test_executor_update_task(self):
        executor_task = self.service.get_task(self.task.task_id, self.executor.user_id)
        executor_task.status = enum.TaskStatus.COMPLETED.value
        self.service.update_task(executor_task, self.executor.user_id)
        updated_task = self.service.get_task(self.task.task_id, self.executor.user_id)
        self.assertEqual(updated_task.status, enum.TaskStatus.REQUEST.value)
        requester = self.service.get_task_user_relations_filtered(task_id=updated_task.task_id,
                                                                  roles=[enum.Role.REQUESTER.value],
                                                                  user_id=self.executor.user_id,
                                                                  current_user_id=self.executor.user_id)
        self.assertTrue(requester)

    def test_observer_update_task(self):
        observer_task = self.service.get_task(self.task.task_id, self.observer.user_id)
        observer_task.status = enum.TaskStatus.COMPLETED.value
        self.service.update_task(observer_task, self.observer.user_id)
        updated_task = self.service.get_task(self.task.task_id, self.observer.user_id)
        self.assertEqual(updated_task.status, enum.TaskStatus.COMPLETED.value)
        finisher = self.service.get_task_user_relations_filtered(task_id=updated_task.task_id,
                                                                 roles=[enum.Role.FINISHER.value],
                                                                 user_id=self.observer.user_id,
                                                                 current_user_id=self.observer.user_id)
        self.assertTrue(finisher)


class TestUserBoardRelations(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage, need_check=True)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="board",
                                                        description="text board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)

        self.task = self.service.add_task(model.Task(title="task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)

        self.board_unregistered = self.service.add_user(model.User("unregistered", "unregistered",
                                                                   "unregistred@gmail.com"))
        self.board_blocked = self.service.add_user(model.User("blocked", "blocked", "blocked@gmail.com"))
        blocked = model.UserBoardRelation(self.board.board_id,
                                          self.board_blocked.user_id,
                                          enum.BoardPermission.BLOCKED.value)
        self.board_blocked_relation = self.service.add_user_board_relation(blocked, self.user_admin.user_id)

        self.board_reader = self.service.add_user(model.User("reader", "reader", "reader@gmail.com"))
        reader = model.UserBoardRelation(self.board.board_id, self.board_reader.user_id,
                                         enum.BoardPermission.READER.value)
        self.board_reader_relation = self.service.add_user_board_relation(reader, self.user_admin.user_id)

        self.board_editor = self.service.add_user(model.User("editor", "editor", "editor@gmail.com"))
        editor = model.UserBoardRelation(self.board.board_id, self.board_editor.user_id,
                                         enum.BoardPermission.EDITOR.value)
        self.board_editor_relation = self.service.add_user_board_relation(editor, self.user_admin.user_id)

        self.board_admin = self.service.add_user(model.User("admin", "admin", "admin@gmail.com"))
        admin = model.UserBoardRelation(self.board.board_id, self.board_admin.user_id, enum.BoardPermission.ADMIN.value)
        self.board_admin_relation = self.service.add_user_board_relation(admin, self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_user_by_login("blocked")
        self.service.remove_user_by_login("reader")
        self.service.remove_user_by_login("editor")
        self.service.remove_user_by_login("admin")
        self.service.remove_user_by_login("unregistered")

        self.service.remove_board(self.board.board_id, self.user_admin.user_id)

        self.service.remove_user_by_login("test_admin")

    def test_get_user_board_relation(self):
        gotten_relation = self.service.get_user_board_relation(self.board_reader_relation.relation_id,
                                                               self.user_admin.user_id)

        self.assertEqual(gotten_relation.relation_id, self.board_reader_relation.relation_id)
        self.assertEqual(gotten_relation.board_id, self.board_reader_relation.board_id)
        self.assertEqual(gotten_relation.user_id, self.board_reader_relation.user_id)
        self.assertEqual(gotten_relation.permission, self.board_reader_relation.permission)

    def test_add_remove_user_board_relation(self):
        new_user = self.service.add_user(model.User("new_user", "new_user", "new_user@gmail.com"))
        new_relation = self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                                    new_user.user_id,
                                                                                    enum.BoardPermission.EDITOR.value),
                                                            self.user_admin.user_id)
        self.assertIsNotNone(new_relation.relation_id)

        gotten_relation = self.service.get_user_board_relation(new_relation.relation_id, self.user_admin.user_id)

        self.assertEqual(gotten_relation.relation_id, new_relation.relation_id)
        self.assertEqual(gotten_relation.board_id, new_relation.board_id)
        self.assertEqual(gotten_relation.user_id, new_relation.user_id)
        self.assertEqual(gotten_relation.permission, new_relation.permission)

        self.assertTrue(self.service.remove_user_board_relation(new_relation.relation_id, self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_user_board_relation(new_relation.relation_id, self.user_admin.user_id)

        self.service.remove_user_by_login("new_user")

    def update_user_board_relation(self):
        args = {
            model.USER_BOARD_RELATION_PERMISSION: enum.BoardPermission.BLOCKED.value
        }
        relation = self.service.get_user_board_relation(self.board_admin_relation.relation_id, self.user_admin.user_id)

        for key, value in args.items():
            setattr(relation, key, value)

        updated_relation = self.service.update_user_board_relation(relation, self.user_admin.user_id)
        self.assertEqual(updated_relation.permission, args[model.USER_BOARD_RELATION_PERMISSION])

    def test_other_creator(self):
        creator_user = self.service.add_user(model.User("creator", "creator", "creator@gmail.com"))
        with self.assertRaises(ex.LogicError):
            creator = model.UserBoardRelation(self.board.board_id, creator_user.user_id,
                                              enum.BoardPermission.CREATOR.value)
            self.service.add_user_board_relation(creator, self.user_admin.user_id)
        self.service.remove_user_by_login("creator")

    def test_blocked_user(self):
        with self.assertRaises(ex.AuthorizationError):
            self.service.get_task(self.task.task_id, self.board_blocked.user_id)

    def test_private_board(self):
        with self.assertRaises(ex.AuthorizationError):
            self.service.get_task(self.task.task_id, self.board_unregistered.user_id)

        with self.assertRaises(ex.AuthorizationError):
            self.service.get_task(self.task.task_id, self.board_blocked.user_id)

        self.service.get_task(self.task.task_id, self.board_reader.user_id)
        with self.assertRaises(ex.AuthorizationError):
            self.task.title = "updated"
            self.service.update_task(self.task, self.board_reader.user_id)

        editor_task = self.service.update_task(self.task, self.board_editor.user_id)
        get_editor_task = self.service.get_task(self.task.task_id, self.board_editor.user_id)
        self.assertEqual(editor_task.title, get_editor_task.title, "Title equality")
        with self.assertRaises(ex.AuthorizationError):
            self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                         self.board_unregistered.user_id,
                                                                         enum.BoardPermission.EDITOR.value),
                                                 self.board_editor.user_id)

        self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                     self.board_unregistered.user_id,
                                                                     enum.BoardPermission.EDITOR.value),
                                             self.board_admin.user_id)

    def test_public_board(self):
        self.board.board_type = enum.BoardType.PUBLIC.value
        self.service.update_board(self.board, self.user_admin.user_id)

        self.service.get_task(self.task.task_id, self.board_unregistered.user_id)
        with self.assertRaises(ex.AuthorizationError):
            self.task.title = "updated"
            self.service.update_task(self.task, self.board_unregistered.user_id)

        with self.assertRaises(ex.AuthorizationError):
            self.service.get_task(self.task.task_id, self.board_blocked.user_id)

        self.service.get_task(self.task.task_id, self.board_reader.user_id)
        with self.assertRaises(ex.AuthorizationError):
            self.task.title = "updated"
            self.service.update_task(self.task, self.board_reader.user_id)

        editor_task = self.service.update_task(self.task, self.board_editor.user_id)
        get_editor_task = self.service.get_task(self.task.task_id, self.board_editor.user_id)
        self.assertEqual(editor_task.title, get_editor_task.title, "Title equality")
        with self.assertRaises(ex.AuthorizationError):
            self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                         self.board_unregistered.user_id,
                                                                         enum.BoardPermission.EDITOR.value),
                                                 self.board_editor.user_id)

        self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                     self.board_unregistered.user_id,
                                                                     enum.BoardPermission.EDITOR.value),
                                             self.board_admin.user_id)

    def test_single_board(self):
        self.board.board_type = enum.BoardType.SINGLE.value
        self.service.update_board(self.board, self.user_admin.user_id)

        self.assertListEqual(self.service.get_board_users(board_id=self.board.board_id,
                                                          permissions=[enum.BoardPermission.READER.value],
                                                          current_user_id=self.user_admin.user_id), [])
        self.assertListEqual(self.service.get_board_users(board_id=self.board.board_id,
                                                          permissions=[enum.BoardPermission.EDITOR.value],
                                                          current_user_id=self.user_admin.user_id), [])
        self.assertListEqual(self.service.get_board_users(board_id=self.board.board_id,
                                                          permissions=[enum.BoardPermission.BLOCKED.value],
                                                          current_user_id=self.user_admin.user_id), [])
        self.assertListEqual(self.service.get_board_users(board_id=self.board.board_id,
                                                          permissions=[enum.BoardPermission.ADMIN.value],
                                                          current_user_id=self.user_admin.user_id), [])
        creator = self.service.get_board_users(board_id=self.board.board_id,
                                               permissions=[enum.BoardPermission.CREATOR.value],
                                               current_user_id=self.user_admin.user_id)[0]
        self.assertEqual(creator.user_id, self.user_admin.user_id)

        with self.assertRaises(ex.AuthorizationError):
            self.service.get_task(self.task.task_id, self.board_unregistered.user_id)

        with self.assertRaises(ex.LogicError):
            self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                         self.board_editor.user_id,
                                                                         enum.BoardPermission.EDITOR.value),
                                                 self.user_admin.user_id)


class TestTags(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))
        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PRIVATE.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)
        self.first_tag = self.service.add_tag(model.Tag(self.task.task_id, "first"), self.user_admin.user_id)
        self.second_tag = self.service.add_tag(model.Tag(self.task.task_id, "second"), self.user_admin.user_id)
        self.third_tag = self.service.add_tag(model.Tag(self.task.task_id, "third"), self.user_admin.user_id)
        self.fourth_tag = self.service.add_tag(model.Tag(self.task.task_id, "fourth"), self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user_by_login("test_admin")

    def test_get_tag(self):
        gotten_tag = self.service.get_tag(self.second_tag.tag_id, self.user_admin.user_id)

        self.assertEqual(gotten_tag.tag_id, self.second_tag.tag_id)
        self.assertEqual(gotten_tag.text, self.second_tag.text)
        self.assertEqual(gotten_tag.task_id, self.second_tag.task_id)

    def test_get_user_tags(self):
        gotten_tags = self.service.get_user_tags(self.user_admin.user_id)
        tags_text = [tag.text for tag in gotten_tags]

        self.assertEqual(len(tags_text), 4)
        self.assertTrue(self.first_tag.text in tags_text)
        self.assertTrue(self.second_tag.text in tags_text)
        self.assertTrue(self.third_tag.text in tags_text)
        self.assertTrue(self.fourth_tag.text in tags_text)

    def test_get_board_tags(self):
        gotten_tags = self.service.get_board_tags(self.board.board_id, self.user_admin.user_id)
        tags_text = [tag.text for tag in gotten_tags]

        self.assertEqual(len(tags_text), 4)
        self.assertTrue(self.first_tag.text in tags_text)
        self.assertTrue(self.second_tag.text in tags_text)
        self.assertTrue(self.third_tag.text in tags_text)
        self.assertTrue(self.fourth_tag.text in tags_text)

    def test_get_task_tags(self):
        gotten_tags = self.service.get_task_tags(self.task.task_id, self.user_admin.user_id)
        tags_text = [tag.text for tag in gotten_tags]

        self.assertEqual(len(tags_text), 4)
        self.assertTrue(self.first_tag.text in tags_text)
        self.assertTrue(self.second_tag.text in tags_text)
        self.assertTrue(self.third_tag.text in tags_text)
        self.assertTrue(self.fourth_tag.text in tags_text)

    def test_add_remove_tag(self):
        new_tag = self.service.add_tag(model.Tag(self.task.task_id, "new"), self.user_admin.user_id)

        self.assertIsNotNone(new_tag.tag_id)

        gotten_tag = self.service.get_tag(new_tag.tag_id, self.user_admin.user_id)

        self.assertEqual(gotten_tag.tag_id, new_tag.tag_id)
        self.assertEqual(gotten_tag.text, new_tag.text)
        self.assertEqual(gotten_tag.task_id, new_tag.task_id)

        self.assertTrue(self.service.remove_tag(gotten_tag.tag_id, self.user_admin.user_id))

        with self.assertRaises(ex.NotFoundError):
            self.service.get_tag(new_tag.tag_id, self.user_admin.user_id)

    def test_remove_tag_by_text(self):
        new_tag = self.service.add_tag(model.Tag(self.task.task_id, "new"), self.user_admin.user_id)
        self.assertIsNotNone(new_tag.tag_id)

        self.assertTrue(self.service.remove_tag_by_text(self.task.task_id, "new", self.user_admin.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_tag(new_tag.tag_id, self.user_admin.user_id)


class TestUsers(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        storage = SQLAlchemyRepos(
            connection_string="mysql://root:lost4815162342@localhost:3306/irontask?charset=utf8")
        cls.service = Services(storage, need_check=True)

    def setUp(self):
        self.user_admin = self.service.add_user(model.User("test_admin", "test_admin", "test_admin@gmail.com"))

        self.board = self.service.add_board(model.Board(title="test_board",
                                                        description="test board description",
                                                        board_type=enum.BoardType.PUBLIC.value),
                                            self.user_admin.user_id)
        self.task = self.service.add_task(model.Task(title="test_task",
                                                     description="test task description",
                                                     priority=enum.TaskPriority.HIGHEST.value,
                                                     board_id=self.board.board_id),
                                          self.user_admin.user_id)

        self.executor = self.service.add_user(model.User("executor", "executor", "executor@gmail.com"))
        self.service.add_user_board_relation(
            model.UserBoardRelation(self.board.board_id,
                                    self.executor.user_id,
                                    enum.BoardPermission.EDITOR.value),
            self.user_admin.user_id)
        self.executor_relation = self.service.add_task_user_relation(
            model.TaskUserRelation(enum.Role.EXECUTOR.value,
                                   self.task.task_id,
                                   self.executor.user_id),
            self.user_admin.user_id)

        self.observer = self.service.add_user(model.User("observer", "observer", "observer@gmail.com"))
        self.service.add_user_board_relation(model.UserBoardRelation(self.board.board_id,
                                                                     self.observer.user_id,
                                                                     enum.BoardPermission.EDITOR.value),
                                             self.user_admin.user_id)
        self.service.add_task_user_relation(model.TaskUserRelation(enum.Role.OBSERVER.value,
                                                                   self.task.task_id,
                                                                   self.observer.user_id),
                                            self.user_admin.user_id)

    def tearDown(self):
        self.service.remove_task(self.task.task_id, self.user_admin.user_id)
        self.service.remove_board(self.board.board_id, self.user_admin.user_id)
        self.service.remove_user(self.user_admin.user_id)
        self.service.remove_user(self.observer.user_id)
        self.service.remove_user(self.executor.user_id)

    def test_get_user(self):
        gotten_user = self.service.get_user(self.user_admin.user_id)

        self.assertEqual(self.user_admin.user_id, gotten_user.user_id)
        self.assertEqual(self.user_admin.login, gotten_user.login)
        self.assertEqual(self.user_admin.email, gotten_user.email)

    def test_get_user_by_email(self):
        gotten_user = self.service.get_user_by_email("test_admin@gmail.com")

        self.assertEqual(self.user_admin.user_id, gotten_user.user_id)
        self.assertEqual(self.user_admin.login, gotten_user.login)
        self.assertEqual(self.user_admin.email, gotten_user.email)

    def test_get_user_by_login(self):
        gotten_user = self.service.get_user_by_login("test_admin")

        self.assertEqual(self.user_admin.user_id, gotten_user.user_id)
        self.assertEqual(self.user_admin.login, gotten_user.login)
        self.assertEqual(self.user_admin.email, gotten_user.email)

    def test_get_task_creator(self):
        task_creator = self.service.get_task_creator(self.task.task_id, self.user_admin.user_id)

        self.assertEqual(self.user_admin.user_id, task_creator.user_id)
        self.assertEqual(self.user_admin.login, task_creator.login)
        self.assertEqual(self.user_admin.email, task_creator.email)

    def test_get_task_executors(self):
        executor = self.service.get_task_executors(self.task.task_id, self.user_admin.user_id)[0]

        self.assertEqual(executor.user_id, self.executor.user_id)
        self.assertEqual(executor.login, self.executor.login)
        self.assertEqual(executor.email, self.executor.email)

    def test_get_task_observers(self):
        observer = self.service.get_task_observers(self.task.task_id, self.user_admin.user_id)[0]

        self.assertEqual(observer.user_id, self.observer.user_id)
        self.assertEqual(observer.login, self.observer.login)
        self.assertEqual(observer.email, self.observer.email)

    def test_get_task_requester(self):
        executor_task = self.service.get_task(self.task.task_id, self.executor.user_id)
        executor_task.status = enum.TaskStatus.COMPLETED.value
        self.service.update_task(executor_task, self.executor.user_id)
        updated_task = self.service.get_task(self.task.task_id, self.executor.user_id)
        self.assertEqual(updated_task.status, enum.TaskStatus.REQUEST.value)
        requester = self.service.get_task_requester(task_id=updated_task.task_id,
                                                    current_user_id=self.executor.user_id)
        self.assertTrue(requester)
        self.assertEqual(requester.user_id, self.executor.user_id)

    def test_get_task_finisher(self):
        observer_task = self.service.get_task(self.task.task_id, self.observer.user_id)
        observer_task.status = enum.TaskStatus.COMPLETED.value
        self.service.update_task(observer_task, self.observer.user_id)
        updated_task = self.service.get_task(self.task.task_id, self.observer.user_id)
        self.assertEqual(updated_task.status, enum.TaskStatus.COMPLETED.value)
        finisher = self.service.get_task_finisher(task_id=updated_task.task_id,
                                                  current_user_id=self.observer.user_id)
        self.assertTrue(finisher)
        self.assertEqual(finisher.user_id, self.observer.user_id)

    def test_get_board_users(self):
        creator = self.service.get_board_users(self.board.board_id,
                                               [enum.BoardPermission.CREATOR.value],
                                               self.user_admin.user_id)[0]
        self.assertEqual(creator.user_id, self.user_admin.user_id)

        editor = self.service.get_board_users(self.board.board_id,
                                              [enum.BoardPermission.EDITOR.value],
                                              self.user_admin.user_id)[0]
        self.assertEqual(editor.user_id, self.executor.user_id)

    def test_update_user(self):
        args = {
            model.USER_EMAIL: "new_email",
            model.USER_LOGIN: "new_login"
        }

        user = self.service.get_user(self.user_admin.user_id)
        for key, value in args.items():
            setattr(user, key, value)

        updated_user = self.service.update_user(user)

        self.assertEqual(updated_user.email, args[model.USER_EMAIL])
        self.assertEqual(updated_user.login, args[model.USER_LOGIN])

    def test_add_remove_user(self):
        new_user = self.service.add_user(model.User("new_user", "new_user", "new_user@gmail.com"))
        self.assertIsNotNone(new_user.user_id)

        gotten_user = self.service.get_user(new_user.user_id)

        self.assertEqual(new_user.user_id, gotten_user.user_id)
        self.assertEqual(new_user.login, gotten_user.login)
        self.assertEqual(new_user.email, gotten_user.email)

        self.assertTrue(self.service.remove_user(gotten_user.user_id))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_user(new_user.user_id)

    def test_remove_user_by_login(self):
        new_user = self.service.add_user(model.User("new_user", "new_user", "new_user@gmail.com"))
        self.assertIsNotNone(new_user.user_id)

        self.assertTrue(self.service.remove_user_by_login("new_user"))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_user(new_user.user_id)

    def test_remove_user_by_email(self):
        new_user = self.service.add_user(model.User("new_user", "new_user", "new_user@gmail.com"))
        self.assertIsNotNone(new_user.user_id)

        self.assertTrue(self.service.remove_user_by_email("new_user@gmail.com"))
        with self.assertRaises(ex.NotFoundError):
            self.service.get_user(new_user.user_id)


if __name__ == '__main__':
    unittest.main()

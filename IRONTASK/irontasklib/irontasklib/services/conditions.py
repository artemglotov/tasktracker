from copy import copy
from datetime import (datetime,
                      timedelta)

from dateutil.relativedelta import relativedelta

from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.model.enums import Periodicity
from irontasklib.storage.sqlalchemy_repos import session_scope


class Condition:
    def __init__(self, data_storage):
        self.storage = data_storage

    def check_all(self):
        """

        :return:
        """
        self.check_tasks()
        self.check_reminders()
        self.check_plans()

    def check_tasks(self):
        tasks = self.storage.get_tasks_filtered(statuses=[enum.TaskStatus.ACTIVE.value])

        for task in tasks:
            task_dates = self.storage.get_task_dates_filtered(task.task_id, date_types=[enum.TaskDateType.END.value,
                                                                                        enum.TaskDateType.EVENT.value])
            if task.status == enum.TaskStatus.ACTIVE.value and task_dates and task_dates[0].date > datetime.now():
                task.status = enum.TaskStatus.OVERDUE.value
                self.storage.add_message(model.Message("Attention! Task {} is overdue.".format(task.task_id),
                                                       task.task_id, datetime.now()))
                self.storage.update_task(task)


    def check_reminders(self):
        reminders = self.storage.get_reminders_filtered(status=enum.ReminderStatus.ON.value)
        for reminder in reminders:
            while True:
                if reminder.start_date > datetime.now():
                    break

                notification_date = copy(reminder.start_date)

                add_period = {
                    Periodicity.YEARS.value: lambda interval: relativedelta(years=interval),
                    Periodicity.MONTHS.value: lambda interval: relativedelta(months=interval),
                    Periodicity.DAYS.value: lambda interval: relativedelta(days=interval),
                    Periodicity.HOURS.value: lambda interval: relativedelta(hours=interval),
                    Periodicity.MINUTES.value: lambda interval: relativedelta(minutes=interval)
                }

                for key, value in reminder.periodicity.items():
                    notification_date += add_period[key](value)

                if ((reminder.end_type == enum.ReminderEndType.DATE.value
                     and notification_date > reminder.end_date)
                        or (reminder.end_type in [enum.ReminderEndType.NUMBER.value]
                            and reminder.current_iteration == reminder.count_iteration)):
                    reminder.status = enum.ReminderStatus.OFF.value
                    break

                if reminder.current_iteration is None:
                    reminder.current_iteration = 0
                reminder.current_iteration += 1

                self.storage.add_message(model.Message(reminder.text, reminder.task_id, reminder.start_date))

                reminder.start_date = notification_date

            self.storage.update_reminder(reminder)


    def check_plans(self):
        plans = self.storage.get_plans_filtered(status=enum.PlanStatus.ON.value)

        for plan in plans:
            while True:
                if plan.start_date > datetime.now():
                    break

                notification_date = copy(plan.start_date)

                add_period = {
                    Periodicity.YEARS.value: lambda interval: relativedelta(years=interval),
                    Periodicity.MONTHS.value: lambda interval: relativedelta(months=interval),
                    Periodicity.DAYS.value: lambda interval: relativedelta(days=interval),
                    Periodicity.HOURS.value: lambda interval: relativedelta(hours=interval),
                    Periodicity.MINUTES.value: lambda interval: relativedelta(minutes=interval)
                }

                for key, value in plan.periodicity.items():
                    notification_date += add_period[key](value)

                if ((plan.end_type == enum.ReminderEndType.DATE.value
                     and notification_date > plan.end_date)
                        or (plan.end_type == enum.ReminderEndType.NUMBER.value
                            and plan.current_iteration == plan.count_iteration)):
                    plan.status = enum.PlanStatus.OFF.value
                    break

                if plan.current_iteration is None:
                    plan.current_iteration = 0
                plan.current_iteration += 1

                self.create_plan_task(plan)

                plan.start_date = notification_date

            self.storage.update_plan(plan)

    def create_plan_task(self, plan):
        import pdb
        pdb.set_trace()
        template_task = self.storage.get_plan_task(plan.plan_id)
        task = model.Task(template_task.title, template_task.board_id, template_task.description,
                          template_task.priority)
        task = self.storage.add_task(task)

        with session_scope() as session:
            self.storage.add_task_date(model.TaskDate(plan.start_date, enum.TaskDateType.CREATE.value, task.task_id),
                                       session=session)
            self.storage.clone_task_users(template_task.task_id, task.task_id, session=session)
        self.storage.clone_task_tags(template_task.task_id, task.task_id, session=session)

    def complete_connected_tasks(self, task_id):
        completed_task_ids = []
        with session_scope() as session:
            relations = self.storage.get_task_relations_filtered(
                parent_id=task_id,
                relation_types=[enum.TaskRelationType.COMPLETE.value],
                session=session
            )

            for relation in relations:
                completing_task = self.storage.get_task(relation.child_id, session=session)
                if completing_task.status != enum.TaskStatus.COMPLETED.value:
                    completing_task.status = enum.TaskStatus.COMPLETED.value
                    self.storage.update_task(completing_task, session=session)
                    self.storage.add_task_date(model.TaskDate(datetime.now(),
                                                              enum.TaskDateType.CLOSE.value,
                                                              completing_task.task_id))
                    completed_task_ids.append(completing_task.task_id)

        for completed_task_id in completed_task_ids:
            self.complete_child_tasks(completed_task_id)
            self.complete_connected_tasks(completed_task_id)

    def complete_parent_task(self, task_id):
        completed_parent_id = None
        with session_scope() as session:
            parent_relation = self.storage.get_task_relations_filtered(
                child_id=task_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value],
                session=session
            )
            if parent_relation and parent_relation[0]:
                parent_id = parent_relation[0].parent_id
                child_relations = self.storage.get_task_relations_filtered(
                    parent_id=parent_id,
                    relation_types=[enum.TaskRelationType.SUBTASK.value],
                    session=session
                )

                def is_child_completed(child_id):
                    if child_id != task_id:
                        child_task = self.storage.get_task(child_id, session=session)
                        return child_task.status in [enum.TaskStatus.COMPLETED.value,
                                                     enum.TaskStatus.ARCHIVED.value]
                    return True

                if all(is_child_completed(relation.child_id) for relation in child_relations):
                    parent_task = self.storage.get_task(parent_id, session=session)
                    parent_task.status = enum.TaskStatus.COMPLETED.value
                    self.storage.update_task(parent_task, session=session)
                    self.storage.add_task_date(model.TaskDate(datetime.now(),
                                               enum.TaskDateType.CLOSE.value,
                                               parent_task.task_id))
                    completed_parent_id = parent_task.parent_id

        if completed_parent_id:
            self.complete_connected_tasks(completed_parent_id)
            self.complete_parent_task(completed_parent_id)

    def complete_child_tasks(self, task_id):
        completed_task_ids = []
        with session_scope() as session:
            relations = self.storage.get_task_relations_filtered(
                parent_id=task_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value],
                session=session
            )
            for relation in relations:
                child_task = self.storage.get_task(relation.child_id, session=session)
                child_task.status = enum.TaskStatus.COMPLETED.value
                self.storage.update_task(child_task, session=session)
                self.storage.add_task_date(model.TaskDate(datetime.now(),
                                           enum.TaskDateType.CLOSE.value,
                                           child_task.task_id))
                completed_task_ids.append(child_task.task_id)

        for completed_task_id in completed_task_ids:
            self.complete_child_tasks(completed_task_id)

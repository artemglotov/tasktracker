from irontasklib.model import models as model
from irontasklib.model import enums as enum
from irontasklib.services import exceptions as ex
from irontasklib.services.log_services import log
from irontasklib.services.log_services import default_logger


class DuplicationConstraint:
    logger = default_logger()

    def __init__(self, data_storage):
        self.storage = data_storage

    @log(logger)
    def check_task_user_relation(self, task_user):
        args = {
            model.TASK_USER_RELATION_TASK_ID: task_user.task_id,
            model.TASK_USER_RELATION_USER_ID: task_user.user_id,
            model.TASK_USER_RELATION_ROLE: task_user.role
        }
        if self.storage.is_duplication(model.TaskUserRelation, **args):
            raise ex.DuplicationError("Task {task_id} already contains user {user_id} with role {role}.".
                                      format(task_id=task_user.task_id,
                                             user_id=task_user.user_id,
                                             role=enum.Role(task_user.role).name.lower()))

    @log(logger)
    def check_user_board_relation(self, user_board):
        args = {
            model.USER_BOARD_RELATION_USER_ID: user_board.user_id,
            model.USER_BOARD_RELATION_BOARD_ID: user_board.board_id
        }
        if self.storage.is_duplication(model.UserBoardRelation, **args):
            raise ex.DuplicationError("Board {board_id} already contains permission for user {user_id}.".
                                      format(board_id=user_board.board_id, user_id=user_board.user_id))

    @log(logger)
    def check_task_tag(self, tag):
        args = {
            model.TAG_TEXT: tag.text,
            model.TAG_TASK_ID: tag.task_id
        }
        if self.storage.is_duplication(model.Tag, **args):
            raise ex.DuplicationError("Task {task_id} already contains text '{text}'.".format(task_id=tag.task_id,
                                                                                              text=tag.text))

    @log(logger)
    def check_task_relation(self, task_relation):
        args = {
            model.TASK_RELATION_PARENT_ID: task_relation.parent_id,
            model.TASK_RELATION_CHILD_ID: task_relation.child_id
        }
        if self.storage.is_duplication(model.TaskRelation, **args):
            raise ex.DuplicationError("Tasks {parent_id} and {child_id} already contain relation.".
                                      format(parent_id=task_relation.parent_id,
                                             child_id=task_relation.child_id))

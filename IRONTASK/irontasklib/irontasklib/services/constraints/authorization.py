from irontasklib.services import exceptions as ex
from irontasklib.services.log_services import log
from irontasklib.services.log_services import default_logger
from irontasklib.model import enums as enum


class AuthorizationConstraints:
    logger = default_logger()

    def __init__(self, data_storage):
        self.storage = data_storage

    @log(logger)
    def check_permission(self, task_id, user_id, min_allowed_permission):
        if user_id is None:
            raise ValueError(user_id)
        board = self.storage.get_task_board(task_id)
        relations = (self.storage.get_user_board_relations_filtered(user_id=user_id, board_id=board.board_id)
                     if board else None)
        user_relation = relations[0] if relations else None

        if (board is not None
                and board.board_type == enum.BoardType.PUBLIC.value
                and (user_relation is None or user_relation.permission != enum.BoardPermission.BLOCKED.value)
                and min_allowed_permission == enum.BoardPermission.READER.value):
            return

        if board is not None:
            self.check_board_permission(board.board_id, user_id, min_allowed_permission)
        else:
            self.check_task_permission(task_id, user_id)

    @log(logger)
    def check_board_permission(self, board_id, user_id, min_allowed_permission):
        if user_id is None:
            raise ValueError(user_id)
        if board_id is not None:
            board = self.storage.get_board(board_id)
            relations = self.storage.get_user_board_relations_filtered(user_id, board_id)
            permission = relations[0].permission if relations else None

            if board and (permission is None or permission < min_allowed_permission):
                raise ex.AuthorizationError("You don't have any permissions to work with the task "
                                            "on the board with id {}".
                                            format(board_id))

    @log(logger)
    def check_task_permission(self, task_id, user_id):
        if user_id is None:
            raise ValueError(user_id)
        if task_id is not None:
            creator = self.storage.get_task_creator(task_id)
            if creator and user_id != creator.user_id:
                raise ex.AuthorizationError("You don't have any permissions to work with the task with id {}".
                                            format(task_id))

    @log(logger)
    def check_plan(self, plan, user_id):
        task = self.storage.get_task(plan.task_id)
        relations = self.storage.get_user_board_relations_filtered(user_id, task.board_id)
        permission = relations[0].permission if relations else None
        if permission not in [enum.BoardPermission.CREATOR.value,
                              enum.BoardPermission.ADMIN.value,
                              enum.BoardPermission.EDITOR.value]:
            raise ex.AuthorizationError("You don't have permissions to add plan to task on board with id {}".
                                        format(task.board_id))

from datetime import datetime

from irontasklib.storage.sqlalchemy_repos import session_scope

from irontasklib.services import exceptions as ex
from irontasklib.services.log_services import log
from irontasklib.services.log_services import default_logger
from irontasklib.model import enums as enum


class LogicConstraint:
    logger = default_logger()

    def __init__(self, data_storage):
        self.storage = data_storage

    @log(logger)
    def check_task_user_relation(self, relation):
        if relation.role == enum.Role.CREATOR.value:
            if self.storage.get_task_creator(relation.task_id) is not None:
                raise ex.LogicError("Task {task_id} already contains creator.".format(task_id=relation.task_id))

        if relation.role == enum.Role.REQUESTER.value:
            if self.storage.get_task_requester(relation.task_id) is not None:
                raise ex.LogicError("Task {task_id} already contains requester.".format(task_id=relation.task_id))

        if relation.role == enum.Role.FINISHER.value:
            if self.storage.get_task_finisher(relation.task_id) is not None:
                raise ex.LogicError("Task {task_id} already contains finisher.".format(task_id=relation.task_id))

        board = self.storage.get_task_board(relation.task_id)
        if board and board.board_type != enum.BoardType.SINGLE.value:
            relations = self.storage.get_user_board_relations_filtered(board_id=board.board_id,
                                                                       user_id=relation.user_id)
            if not relations or relations[0].permission < enum.BoardPermission.EDITOR.value:
                raise ex.LogicError("User with permissions less than 'editor' can't be user of task.")
        else:
            creator_relations = self.storage.get_task_user_relations_filtered(task_id=relation.task_id,
                                                                              roles=[enum.Role.CREATOR.value])

            if creator_relations:
                raise ex.LogicError("You can't add users to solo tasks")

    @log(logger)
    def check_task_date(self, task_date, existing_dates):
        for existing_date in existing_dates:
            if (task_date.task_date_id == existing_date.task_date_id
                    and task_date.date_type == existing_date.date_type
                    and task_date.date == existing_date.date):
                continue
            if task_date.date_type == existing_date.date_type and task_date.task_date_id != existing_date.task_date_id:
                raise ex.LogicError("Task already has date of type {date_type}. Update existing one"
                                    .format(date_type=enum.TaskDateType(task_date.date_type).name.lower()))

            if ((task_date.date_type == enum.TaskDateType.START.value
                 or task_date.date_type == enum.TaskDateType.END.value)
                    and existing_date.date_type == enum.TaskDateType.EVENT.value):
                raise ex.LogicError("You can't add period date, because task already has event date")

            if (task_date.date_type == enum.TaskDateType.START.value
                    and existing_date.date_type == enum.TaskDateType.END.value
                    and task_date.date > existing_date.date):
                raise ex.LogicError(("Task has end date and"
                                     " it's earlier than the date you're trying to add"))

            if (task_date.date_type == enum.TaskDateType.END.value
                    and existing_date.date_type == enum.TaskDateType.START.value
                    and task_date.date < existing_date.date):
                raise ex.LogicError(("Task has start date and"
                                     " it's later than the date you're trying to add"))

            if (task_date.date_type == enum.TaskDateType.EVENT.value
                    and (existing_date.date_type == enum.TaskDateType.END.value
                         or existing_date.date_type == enum.TaskDateType.START.value)):
                raise ex.LogicError("You can't add event date to task, it already has period date(s)")

    @log(logger)
    def check_user(self, user):
        existing_user = self.storage.get_user(user.user_id) if user.user_id else None

        gotten_user = self.storage.get_user_by_email(user.email)
        if gotten_user and existing_user and gotten_user.user_id != existing_user.user_id:
            raise ex.LogicError("User with email '{email}' already exist.".format(email=user.email))
        gotten_user = self.storage.get_user_by_login(user.login)
        if gotten_user and existing_user and gotten_user.user_id != existing_user.user_id:
            raise ex.LogicError("User with login '{login}' already exist.".format(login=user.login))

    @log(logger)
    def check_plan(self, plan):
        if self.storage.get_plans_filtered(plan.task_id):
            raise ex.LogicError("Task with id {task_id} already has a plan.".format(task_id=plan.task_id))

    @log(logger)
    def check_task_status(self, task, user_id):
        old_task = self.storage.get_task(task.task_id)
        if old_task.status != enum.TaskStatus.ACTIVE.value and task.status == enum.TaskStatus.ACTIVE.value:
            overdue_dates = self.storage.get_task_dates_filtered(task_id=task.task_id,
                                                                 date_types=[enum.TaskDateType.END.value,
                                                                             enum.TaskDateType.EVENT.value])
            if overdue_dates and datetime.now() > overdue_dates[0]:
                task.status = enum.TaskStatus.OVERDUE.value

        if old_task.status != enum.TaskStatus.COMPLETED.value and task.status == enum.TaskStatus.COMPLETED.value:
            observers = self.storage.get_task_observers(task.task_id)
            if observers and (user_id not in [obs.user_id for obs in observers]):
                task.status = enum.TaskStatus.REQUEST.value

    @log(logger)
    def check_task_relation(self, relation):
        # check if these tasks are on the same board or both don't connected with any board
        parent_board = self.storage.get_task_board(relation.parent_id)
        child_board = self.storage.get_task_board(relation.child_id)
        if ((parent_board and not child_board)
                or (not parent_board and child_board)
                or (parent_board and child_board and parent_board.board_id != child_board.board_id)):
            raise ex.LogicError("Tasks with ids {} and {} must be located on the same board."
                                .format(relation.parent_id, relation.child_id))

        # check if already exist parent task
        if relation.relation_type == enum.TaskRelationType.SUBTASK.value:
            parent_relation = self.storage.get_task_relations_filtered(
                child_id=relation.child_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value]
            )
            if parent_relation:
                raise ex.LogicError("Any task can have only one parent task")

        # check aggregate relations
        if relation.relation_type in [enum.TaskRelationType.BLOCK.value, enum.TaskRelationType.COMPLETE.value]:
            parent_relations = self.storage.get_task_relations_filtered(
                child_id=relation.parent_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value]
            )
            parent_parent_id = parent_relations[0].parent_id if parent_relations else None
            child_relations = self.storage.get_task_relations_filtered(
                child_id=relation.child_id,
                relation_types=[enum.TaskRelationType.SUBTASK.value]
            )
            child_parent_id = child_relations[0].parent_id if child_relations else None

            if ((not parent_parent_id or not child_parent_id or parent_parent_id != child_parent_id)
                 and (parent_parent_id or child_parent_id)):
                raise ex.LogicError("Blocking or completing tasks must have the same parent, "
                                    "or both shouldn't have a parent.")

        if relation.relation_type == enum.TaskRelationType.SUBTASK.value:
            connected_task_ids = [connected_relation.parent_id
                                  for connected_relation in self.storage.get_task_relations_filtered(
                                      child_id=relation.child_id,
                                      relation_types=[enum.TaskRelationType.COMPLETE.value,
                                                      enum.TaskRelationType.BLOCK.value]
                                  )]
            connected_task_ids.extend([connected_relation.child_id
                                       for connected_relation in self.storage.get_task_relations_filtered(
                                            parent_id=relation.child_id,
                                            relation_types=[enum.TaskRelationType.COMPLETE.value,
                                                            enum.TaskRelationType.BLOCK.value]
                                       )])
            if connected_task_ids:
                raise ex.LogicError("Blocking or completing tasks must have the same parent, "
                                    "or both shouldn't have a parent.")

        # check circles in relations
        if relation.relation_type in [enum.TaskRelationType.SUBTASK.value, enum.TaskRelationType.BLOCK.value]:
            with session_scope() as session:
                def check_next_parent(child_id, compare_with_id):
                    next_relations = self.storage.get_task_relations_filtered(child_id=child_id,
                                                                              relation_types=[relation.relation_type],
                                                                              session=session)
                    if not next_relations:
                        return
                    for next_relation in next_relations:
                        if next_relation.parent_id == compare_with_id:
                            raise ex.LogicError("You can't add this relation because it create a circle.")
                        check_next_parent(next_relation.parent_id, compare_with_id)

                check_next_parent(relation.parent_id, relation.child_id)

    @log(logger)
    def check_user_board_relation(self, relation):
        board = self.storage.get_board(relation.board_id)
        if board.board_type == enum.BoardType.SINGLE.value:
            raise ex.LogicError("Single board has only one user.")

        if relation.permission == enum.BoardPermission.CREATOR.value:
            board_creator = self.storage.get_board_creator(relation.board_id)
            if board_creator:
                raise ex.LogicError("Creator of board {board_id} already exist.".format(board_id=relation.board_id))

    @log(logger)
    def check_block_by_other(self, task):
        with session_scope() as session:
            blocked_task_relations = self.storage.get_task_relations_filtered(
                child_id=task.task_id,
                relation_types=[enum.TaskRelationType.BLOCK.value],
                session=session
            )
            for relation in blocked_task_relations:
                blocked_task = self.storage.get_task(relation.parent_id, session=session)
                if blocked_task.status not in [enum.TaskStatus.COMPLETED.value, enum.TaskStatus.ARCHIVED.value]:
                    raise ex.LogicError("You can't change this task until task {} is performed"
                                        .format(blocked_task.task_id))

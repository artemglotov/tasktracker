from irontasklib.services import exceptions as ex
from irontasklib.services.log_services import log
from irontasklib.services.log_services import default_logger


class ExistenceConstraint:
    logger = default_logger()

    def __init__(self, data_storage):
        self.storage = data_storage

    @log(logger)
    def check_existence(self, obj_model, obj_id):
        if not self.storage.is_exist(obj_model, obj_id):
            raise ex.NotFoundError(obj_model, obj_id)

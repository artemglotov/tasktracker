class IronTaskError(Exception):
    def __init__(self, message):
        super().__init__(message)


class LogicError(IronTaskError):
    """LogicError - raises if user tries to perform illogical operation

        For example, trying to add creator to task who already exist

        Parameters
        ----------
        message: str
            Describes reason of the exception

    """
    def __init__(self, message):
        super().__init__(message)


class DuplicationError(IronTaskError):
    """DuplicationError - raises if user tries to add the same model

            For example, trying to add the same relation to tasks

            Parameters
            ----------
            message: str
                Describes reason of the exception

        """
    def __init__(self, message):
        super().__init__(message)


class AuthorizationError(IronTaskError):
    """AuthorizationError - raises if user hasn't access to doing some operation

                For example, trying to update task on private bord if he is not a user of this board

                Parameters
                ----------
                message: str
                    Describes reason of the exception

            """
    def __init__(self, message):
        super().__init__(message)


class NotFoundError(IronTaskError):
    """NotFoundError - raises if there is no required object in storage

        Parameters
        ----------
        obj_id: int
            Id of object that wasn't found
        obj_model: object
            Title of object class

    """
    def __init__(self, obj_model, obj_id):
        super().__init__("Couldn't find {type} with id {id}".format(type=obj_model.__name__, id=obj_id))
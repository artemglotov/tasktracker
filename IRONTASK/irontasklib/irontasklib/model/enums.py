from enum import Enum


class Periodicity(Enum):
    YEARS = "years"
    MONTHS = "months"
    DAYS = "days"
    HOURS = "hours"
    MINUTES = "minutes"


class Role(Enum):
    CREATOR = 1
    EXECUTOR = 2
    OBSERVER = 3
    REQUESTER = 4
    FINISHER = 5  # who close task


class BoardStatus(Enum):
    OPENED = 1
    CLOSED = 2
    ARCHIVED = 3


class BoardType(Enum):
    PUBLIC = 1
    PRIVATE = 2
    SINGLE = 3


class TaskDateType(Enum):
    CREATE = 1
    START = 2
    END = 3
    EVENT = 4
    CLOSE = 5
    EDIT = 6


class TaskStatus(Enum):
    ACTIVE = 1  # active, if task-event or current date is between start and end date of the task
    REQUEST = 2  # completed, but need response from observers (current)
    OVERDUE = 3  # after specified time-frame (current)
    COMPLETED = 4  # completed
    FAILED = 5  # if task hasn't done you may specify this status
    TEMPLATE = 6  # template task for plan
    ARCHIVED = 7  # for deleted tasks


class TaskPriority(Enum):
    LOWEST = 1
    LOW = 2
    NORMAL = 3
    HIGH = 4
    HIGHEST = 5


class BoardPermission(Enum):
    BLOCKED = 1
    UNREGISTERED = 2
    READER = 3
    EDITOR = 4
    ADMIN = 5
    CREATOR = 6


class TaskRelationType(Enum):
    SUBTASK = 1
    COMPLETE = 2
    BLOCK = 3


class PlanPeriod(Enum):
    HOUR = 1
    DAY = 2
    WEEK = 3
    MONTH = 4
    YEAR = 5


class ReminderPeriod(Enum):
    HOUR = 1
    DAY = 2
    WEEK = 3
    MONTH = 4
    YEAR = 5
    NONE = 6


class PlanEndType(Enum):
    NEVER = 1
    DATE = 2
    NUMBER = 3


class ReminderEndType(Enum):
    NEVER = 1
    DATE = 2
    NUMBER = 3
    SINGLE = 4


class PlanStatus(Enum):
    ON = 1
    OFF = 2
    ARCHIVED = 3


class ReminderStatus(Enum):
    ON = 1
    OFF = 2
    ARCHIVED = 3

class TaskView:
    def __init__(self, task, board, creator, executors, observers, requester, finisher, date_create, date_event,
                 date_start, date_end, date_close, messages, reminders, tags, parent_relations, child_relations,
                 dates_edit):
        self.task = task
        self.board = board
        self.creator = creator
        self.executors = executors
        self.observers = observers
        self.requester = requester
        self.finisher = finisher
        self.date_create = date_create
        self.date_event = date_event
        self.date_start = date_start
        self.date_end = date_end
        self.date_close = date_close
        self.messages = messages
        self.reminders = reminders
        self.tags = tags
        self.parent_relations = parent_relations
        self.child_relations = child_relations
        self.dates_edit = dates_edit


class PlanView:
    def __init__(self, plan, task, board, creator, executors, observers, date_create, tags):
        self.plan = plan
        self.task = task
        self.board = board
        self.creator = creator
        self.executors = executors
        self.observers = observers
        self.date_create = date_create
        self.tags = tags


class BoardView:
    def __init__(self, board, creator, admins, editors, readers, blocked, tasks, messages, dates):
        self.board = board
        self.creator = creator
        self.admins = admins
        self.editors = editors
        self.readers = readers
        self.blocked = blocked
        self.tasks = tasks
        self.messages = messages
        self.dates = dates


class TaskDateView:
    def __init__(self, task_date, task):
        self.task_date = task_date
        self.task = task


class ReminderView:
    def __init__(self, reminder, task):
        self.reminder = reminder
        self.task = task


class TaskRelationView:
    def __init__(self, relation, parent_task, child_task):
        self.relation = relation
        self.parent_task = parent_task
        self.child_task = child_task


class UserBoardRelationView:
    def __init__(self, relation, user, board):
        self.relation = relation
        self.user = user
        self.board = board


class TaskUserRelationView:
    def __init__(self, relations, task, user):
        self.relations = relations
        self.task = task
        self.user = user


class MessageView:
    def __init__(self, user, message, task):
        self.user = user
        self.message = message
        self.task = task


class TreeView:
    def __init__(self, task, relation=None, child_tasks=None, complete_task_ids=None, block_task_ids=None):
        self.task = task
        self.relation = relation
        self.child_tasks = child_tasks or []
        self.complete_task_ids = complete_task_ids or []
        self.block_task_ids = block_task_ids or []
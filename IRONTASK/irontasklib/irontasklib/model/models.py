from irontasklib.model import enums as enum

CURRENT_USER_ID = "current_user_id"

USER_ID = "user_id"
USER_LOGIN = "login"
USER_PASSWORD = "password"
USER_EMAIL = "email"

USER_BOARD_RELATION_ID = "relation_id"
USER_BOARD_RELATION_USER_ID = "user_id"
USER_BOARD_RELATION_BOARD_ID = "board_id"
USER_BOARD_RELATION_PERMISSION = "permission"

BOARD_ID = "board_id"
BOARD_TITLE = "title"
BOARD_COLOR = "color"
BOARD_DESCRIPTION = "description"
BOARD_STATUS = "status"
BOARD_TYPE = "board_type"

TASK_ID = "task_id"
TASK_BOARD_ID = "board_id"
TASK_TITLE = "title"
TASK_DESCRIPTION = "description"
TASK_STATUS = "status"
TASK_PRIORITY = "priority"
TASK_IS_ALLOW_MESSAGES = "is_allow_messages"
TASK_WITHOUT_BOARD = "without_board"

PLAN_ID = "plan_id"
PLAN_TASK_ID = "task_id"
PLAN_PERIODICITY = "periodicity"
PLAN_END_TYPE = "end_type"
PLAN_STATUS = "status"
PLAN_START = "start_date"
PLAN_END = "end_date"
PLAN_CURRENT_ITERATION = "current_iteration"
PLAN_COUNT_ITERATION = "count_iteration"
PLAN_NOT_END = "not_end"


TASK_DATE_ID = "task_date_id"
TASK_DATE_TASK_ID = "task_id"
TASK_DATE = "date"
TASK_DATE_TYPE = "date_type"

TASK_DATE_EVENT = "event_date"
TASK_DATE_START = "start_date"
TASK_DATE_END = "end_date"


TASK_USER_RELATION_ID = "relation_id"
TASK_USER_RELATION_TASK_ID = "task_id"
TASK_USER_RELATION_USER_ID = "user_id"
TASK_USER_RELATION_ROLE = "role"
TASK_USER_RELATION_FULL = "full"

TASK_EXECUTORS = "executors"
TASK_OBSERVERS = "observers"

REMINDER_ID = "reminder_id"
REMINDER_TASK_ID = "task_id"
REMINDER_STATUS = "status"
REMINDER_TEXT = "text"
REMINDER_PERIODICITY = "periodicity"
REMINDER_END_TYPE = "end_type"
REMINDER_START = "start_date"
REMINDER_END = "end_date"
REMINDER_CURRENT_ITERATION = "current_iteration"
REMINDER_COUNT_ITERATION = "count_iteration"
REMINDER_NOT_END = "not_end"

TASK_RELATION_ID = "relation_id"
TASK_RELATION_PARENT_ID = "parent_id"
TASK_RELATION_CHILD_ID = "child_id"
TASK_RELATION_TYPE = "relation_type"

TAG_ID = "tag_id"
TAG_TASK_ID = "task_id"
TAG_BOARD_ID = "board_id"
TAG_USER_ID = "user_id"
TAG_TEXT = "text"

TAGS = "tags"

MESSAGE_ID = "message_id"
MESSAGE_TASK_ID = "task_id"
MESSAGE_USER_ID = "user_id"
MESSAGE_TEXT = "text"
MESSAGE_DATE = "date"
MESSAGE_SEEN = "seen"
MESSAGES_FROM_DATE = "from"
MESSAGES_TO_DATE = "to"

DETAILS = "details"


class User:
    def __init__(self, login, password=None, email=None, user_id=None):
        self.user_id = user_id
        self.login = login
        self.password = password
        self.email = email


class UserBoardRelation:
    def __init__(self, board_id, user_id, permission=enum.BoardPermission.READER.value, relation_id=None):
        self.relation_id = relation_id
        self.user_id = user_id
        self.board_id = board_id
        self.permission = permission


class Board:
    def __init__(self, title, color=None, description=None, status=enum.BoardStatus.OPENED.value,
                 board_type=enum.BoardType.PUBLIC.value, board_id=None):
        self.board_id = board_id
        self.title = title
        self.color = color
        self.description = description
        self.status = status
        self.board_type = board_type


class Task:
    def __init__(self, title, board_id=None, description=None, priority=enum.TaskPriority.NORMAL.value,
                 status=enum.TaskStatus.ACTIVE.value, task_id=None):
        self.task_id = task_id
        self.board_id = board_id
        self.title = title
        self.description = description
        self.status = status
        self.priority = priority


class Plan:
    def __init__(self, task_id, start_date, end_date=None, count_iteration=None,
                 current_iteration=0, end_type=enum.PlanEndType.NEVER.value, status=enum.PlanStatus.ON.value,
                 periodicity=None, plan_id=None):
        self.plan_id = plan_id
        self.task_id = task_id
        self.periodicity = periodicity
        self.end_type = end_type
        self.status = status
        self.start_date = start_date
        self.end_date = end_date
        self.current_iteration = current_iteration
        self.count_iteration = count_iteration


class TaskDate:
    def __init__(self, date=None, date_type=None, task_id=None, task_date_id=None):
        self.task_date_id = task_date_id
        self.task_id = task_id
        self.date = date
        self.date_type = date_type


class TaskUserRelation:
    def __init__(self, role, task_id, user_id, relation_id=None):
        self.relation_id = relation_id
        self.task_id = task_id
        self.user_id = user_id
        self.role = role


class Reminder:
    def __init__(self, start_date, task_id, periodicity=None, end_type=None, count_iteration=None, current_iteration=0,
                 end_date=None, text=None, status=enum.ReminderStatus.ON.value,
                 reminder_id=None):
        self.reminder_id = reminder_id
        self.task_id = task_id
        self.status = status
        self.text = text
        self.periodicity = periodicity
        self.end_type = end_type
        self.start_date = start_date
        self.end_date = end_date
        self.current_iteration = current_iteration
        self.count_iteration = count_iteration


class TaskRelation:
    def __init__(self, relation_type, parent_id, child_id, relation_id=None):
        self.relation_id = relation_id
        self.parent_id = parent_id
        self.child_id = child_id
        self.relation_type = relation_type


class Tag:
    def __init__(self, task_id, text, tag_id=None):
        self.tag_id = tag_id
        self.task_id = task_id
        self.text = text


class Message:
    def __init__(self, text, task_id, date, user_id=None, seen=False, message_id=None):
        self.message_id = message_id
        self.task_id = task_id
        self.user_id = user_id
        self.text = text
        self.date = date
        self.seen = seen


def get_display_model(model, value_to_str_translator):
    for key, value in vars(model).items():
        if key in value_to_str_translator:
            setattr(model, key, value_to_str_translator[key](value))
        elif value is None:
            setattr(model, key, "-")
    return vars(model)

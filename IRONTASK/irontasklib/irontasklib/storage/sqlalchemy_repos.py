from sqlalchemy.orm import relationship
from sqlalchemy.orm import mapper
from sqlalchemy.orm import clear_mappers, aliased
from sqlalchemy import and_, func
from sqlalchemy import create_engine
from sqlalchemy import (Column,
                        DateTime,
                        ForeignKey,
                        MetaData,
                        String,
                        Table,
                        text)
from sqlalchemy.dialects.mysql.types import (TINYINT,
                                             INTEGER)
from sqlalchemy.types import JSON
from irontasklib.model import models as model
from irontasklib.model import enums as enum

from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker

Session = sessionmaker()


@contextmanager
def session_scope():

    session = Session()
    try:
        yield session
        session.commit()
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()


def session_injector(func):
    """Decorator for functions. Create and append session object if it isn't exist

    Parameters
    ----------
    func

    Returns
    -------
    decorated func

    """
    def injected_func(*args, **kwargs):
        if "session" not in kwargs:
            with session_scope() as session:
                return func(session=session, *args, **kwargs)
        else:
            return func(*args, **kwargs)
    return injected_func


class SQLAlchemyRepos:
    def __init__(self,
                 connection_string,
                 board_class=model.Board,
                 plan_class=model.Plan,
                 task_date_class=model.TaskDate,
                 message_class=model.Message,
                 task_relation_class=model.TaskRelation,
                 reminder_class=model.Reminder,
                 task_class=model.Task,
                 tag_class=model.Tag,
                 task_user_relation_class=model.TaskUserRelation,
                 user_board_relation_class=model.UserBoardRelation,
                 user_class=model.User):
        """Init SQLAlchemyRepos object specifying classes that will be mapped on database

        Parameters
        ----------
        connection_string: str
        board_class: Board
        plan_class: Plan
        task_date_class: TaskDate
        message_class: Message
        task_relation_class: TaskRelation
        reminder_class: Reminder
        task_class: Task
        tag_class: Tag
        task_user_relation_class: TaskUserRelation
        user_board_relation_class: UserBoardRelation
        user_class: User
        """
        engine = create_engine(connection_string)
        Session.configure(bind=engine, expire_on_commit=False)
        database_meta = MetaData(bind=engine)

        self.board_class = board_class
        self.plan_class = plan_class
        self.task_date_class = task_date_class
        self.message_class = message_class
        self.task_relation_class = task_relation_class
        self.reminder_class = reminder_class
        self.task_class = task_class
        self.tag_class = tag_class
        self.task_user_relation_class = task_user_relation_class
        self.user_board_relation_class = user_board_relation_class
        self.user_class = user_class

        database_meta.reflect()

        self.board_table = database_meta.tables["boards"]
        self.plan_table = database_meta.tables["plans"]
        self.task_date_table = database_meta.tables["task_dates"]
        self.message_table = database_meta.tables["messages"]
        self.task_relation_table = database_meta.tables["task_relations"]
        self.reminder_table = database_meta.tables["reminders"]
        self.task_table = database_meta.tables["tasks"]
        self.tag_table = database_meta.tables["tags"]
        self.task_user_relation_table = database_meta.tables["task_user_relations"]
        self.user_board_relation_table = database_meta.tables["user_board_relations"]
        self.user_table = database_meta.tables["users"]

        clear_mappers()

        # map plan table
        mapper(self.plan_class, self.plan_table,
               properties={
                   "task": relationship(self.task_class)
               })

        # map users table
        mapper(self.user_class, self.user_table)

        # map boards table
        mapper(self.board_class, self.board_table)

        # map task_dates table
        mapper(self.task_date_class, self.task_date_table,
               properties={
                   "task": relationship(self.task_class)
               })

        # map messages table
        mapper(self.message_class, self.message_table,
               properties={
                   "task": relationship(self.task_class),
                   "user": relationship(self.user_class)
               })

        # map task_relations table
        mapper(self.task_relation_class, self.task_relation_table,
               properties={
                   "parent_task":
                       relationship(self.task_class,
                                    primaryjoin=self.task_relation_table.c.parent_id == self.task_table.c.task_id),
                   "child_task":
                       relationship(self.task_class,
                                    primaryjoin=self.task_relation_table.c.child_id == self.task_table.c.task_id)
               })

        # map task_reminders table
        mapper(self.reminder_class, self.reminder_table,
               properties={
                   "task": relationship(self.task_class)
               })

        # map tasks table
        mapper(self.task_class, self.task_table,
               properties={
                   "board": relationship(self.board_class)
               })

        # map task_users table
        mapper(self.task_user_relation_class, self.task_user_relation_table,
               properties={
                   "task": relationship(self.task_class),
                   "user": relationship(self.user_class)

               })

        # map user_boards table
        mapper(self.user_board_relation_class, self.user_board_relation_table,
               properties={
                   "user": relationship(self.user_class),
                   "board": relationship(self.board_class)
               })

        # map task_tags table
        mapper(self.tag_class, self.tag_table,
               properties={
                   "task": relationship(self.task_class)
               })

    # region task queries
    @session_injector
    def get_task(self, task_id, session=None):
        return session.query(self.task_class).get(task_id)

    @session_injector
    def get_tasks_by_tags(self, tags, current_user_id=None, session=None):
        task_ids = (session.query(self.tag_class.task_id)
                    .filter(self.tag_class.text.in_(tags))
                    .group_by(self.tag_class.task_id)
                    .having(func.count(self.tag_class.tag_id) == len(tags))
                    .subquery())
        tasks = (session.query(self.task_class).join(self.task_user_relation_class)
                 .filter(self.task_class.task_id.in_(task_ids))
                 .filter(self.task_user_relation_class.user_id == current_user_id if current_user_id else True))
        return tasks.all()

    @session_injector
    def get_tasks_filtered(self, title=None, description=None, priority=None, statuses=None, board_id=None, session=None):
        return (session.query(self.task_class)
                .filter(and_(title in self.task_class.title if title else True,
                             description in self.task_class.description if description else True,
                             priority == self.task_class.priority if priority else True,
                             self.task_class.status.in_(statuses) if statuses else True,
                             board_id == self.task_class.board_id if board_id else True))).all()

    @session_injector
    def get_user_tasks(self, user_id, role=None, statuses=None, priority=None, tags=None, title=None, session=None):
        return (session.query(self.task_class).join(self.task_user_relation_class).outerjoin(self.tag_class)
                .filter(and_(self.task_user_relation_class.user_id == user_id,
                             self.task_class.title.like('%' + title + '%') if title else True,
                             self.task_user_relation_class.role == role if role else True,
                             self.task_class.status.in_(statuses) if statuses else True,
                             self.task_class.priority == priority if priority else True,
                             self.tag_class.tag in tags if tags else True))).all()

    @session_injector
    def get_plan_task(self, plan_id, session=None):
        return session.query(self.task_class).join(self.plan_class).filter_by(plan_id=plan_id).first()

    @session_injector
    def get_parent_tasks(self, child_task_id, relation_type, session=None):
        return (session.query(self.task_class)
                .join(self.task_relation_class, self.task_relation_class.parent_id == self.task_class.task_id)
                .filter(and_(self.task_relation_class.child_id == child_task_id,
                             self.task_relation_class.relation_type == relation_type if relation_type else True))).all()

    @session_injector
    def get_child_tasks(self, parent_task_id, relation_type, session=None):
        return (session.query(self.task_class)
                .join(self.task_relation_class, self.task_relation_class.child_id == self.task_class.task_id)
                .filter(and_(self.task_relation_class.parent_id == parent_task_id,
                             self.task_relation_class.relation_type == relation_type if relation_type else True))).all()

    @session_injector
    def add_task(self, task, session=None):
        session.add(task)
        return task

    @session_injector
    def remove_task(self, task_id, session=None):
        return session.query(self.task_class).filter_by(task_id=task_id).delete()

    @session_injector
    def update_task(self, task, session=None):
        session.add(task)
        return session.query(self.task_class).get(task.task_id)
    # endregion

    # region plan queries
    @session_injector
    def get_plan(self, plan_id, session=None):
        return session.query(self.plan_class).get(plan_id)

    @session_injector
    def get_user_plans(self, user_id, session=None):
        return (session.query(self.plan_class).join(self.task_class).join(self.task_user_relation_class)
                .filter_by(user_id=user_id)).all()

    @session_injector
    def get_plans_filtered(self, task_id=None, status=None, session=None):
        return (session.query(self.plan_class)
                .filter(and_(self.plan_class.task_id == task_id if task_id else True,
                             self.plan_class.status == status if status else True))).all()

    @session_injector
    def add_plan(self, plan, session=None):
        session.add(plan)
        return plan

    @session_injector
    def remove_plan(self, plan_id, session=None):
        return session.query(self.plan_class).filter_by(plan_id=plan_id).delete()

    @session_injector
    def update_plan(self, plan, session=None):
        session.add(plan)
        return session.query(self.plan_class).get(plan.plan_id)
    # endregion

    # region task_user_relation queries
    @session_injector
    def get_task_user_relation(self, relation_id, session=None):
        return session.query(self.task_user_relation_class).get(relation_id)

    @session_injector
    def get_task_user_relations_filtered(self, task_id=None, user_id=None, roles=None,
                                         board_id=None, status=None, session=None):
        return (session.query(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_user_relation_class.task_id == task_id if task_id else True,
                             self.task_user_relation_class.user_id == user_id if user_id else True,
                             self.task_user_relation_class.role.in_(roles) if roles else True,
                             self.task_class.board_id == board_id if board_id else True,
                             self.task_class.status == status if status else True))).all()

    @session_injector
    def add_task_user_relation(self, relation, session=None):
        session.add(relation)
        return relation

    @session_injector
    def update_task_user_relation(self, relation, session=None):
        session.add(relation)
        return session.query(self.task_user_relation_class).get(relation.relation_id)

    @session_injector
    def remove_task_user_relation(self, relation_id, session=None):
        return session.query(self.task_user_relation_class).filter_by(relation_id=relation_id).delete()
    # endregion

    # region board queries
    @session_injector
    def get_board(self, board_id, session=None):
        return session.query(self.board_class).get(board_id)

    @session_injector
    def get_user_boards(self, user_id, status=None, board_type=None, title=None, session=None):
        return (session.query(self.board_class).join(self.user_board_relation_class)
                .filter(and_(self.user_board_relation_class.user_id == user_id,
                             self.board_class.title.like('%' + title + '%') if title else True,
                             self.board_class.status == status if status is not None else True,
                             self.board_class.board_type == board_type if board_type is not None else True))).all()

    @session_injector
    def get_task_board(self, task_id, session=None):
        return (session.query(self.board_class).join(self.task_class).filter_by(task_id=task_id)).first()

    @session_injector
    def add_board(self, board, session=None):
        session.add(board)
        return board

    @session_injector
    def remove_board(self, board_id, session=None):
        return session.query(self.board_class).filter_by(board_id=board_id).delete()

    @session_injector
    def update_board(self, board, session=None):
        session.add(board)
        return session.query(self.board_class).get(board.board_id)
    # endregion

    # region user_board_relation queries
    @session_injector
    def get_user_board_relation(self, relation_id, session=None):
        return session.query(self.user_board_relation_class).get(relation_id)

    @session_injector
    def get_user_board_relations_filtered(self, user_id=None, board_id=None, permission=None, session=None):
        return (session.query(self.user_board_relation_class)
                .filter(and_(self.user_board_relation_class.user_id == user_id if user_id else True,
                             self.user_board_relation_class.board_id == board_id if board_id else True,
                             self.user_board_relation_class.permission == permission if permission else True))).all()

    @session_injector
    def add_user_board_relation(self, user_board_relation, session=None):
        session.add(user_board_relation)
        return user_board_relation

    @session_injector
    def remove_user_board_relation(self, relation_id, session=None):
        return session.query(self.user_board_relation_class).filter_by(relation_id=relation_id).delete()

    @session_injector
    def update_user_board_relation(self, user_board_relation, session=None):
        session.add(user_board_relation)
        return session.query(self.user_board_relation_class).get(user_board_relation.relation_id)

    # # endregion

    # region task_date queries
    @session_injector
    def get_task_date(self, task_date_id, session=None):
        return session.query(self.task_date_class).get(task_date_id)

    @session_injector
    def get_task_dates(self, task_id, session=None):
        return session.query(self.task_date_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_task_dates_filtered(self, task_id=None, date_types=None, date_from=None, date_to=None,
                                board_id=None, session=None):
        return (session.query(self.task_date_class).outerjoin(self.task_class).outerjoin(self.board_class)
                .filter(and_(self.task_date_class.task_id == task_id if task_id else True,
                             self.board_class.board_id == board_id if board_id else True,
                             self.task_date_class.date_type.in_(date_types) if date_types else True,
                             self.task_date_class.date >= date_from if date_from else True,
                             self.task_date_class.date <= date_to if date_from else True))).all()

    @session_injector
    def add_task_date(self, task_date, session=None):
        session.add(task_date)
        return task_date

    @session_injector
    def remove_task_date(self, task_date_id, session=None):
        return session.query(self.task_date_class).filter_by(task_date_id=task_date_id).delete()

    @session_injector
    def update_task_date(self, task_date, session=None):
        session.add(task_date)
        return session.query(self.task_date_class).get(task_date.task_date_id)
    # endregion

    # region message queries
    @session_injector
    def get_message(self, message_id, session=None):
        return session.query(self.message_class).get(message_id)

    @session_injector
    def get_messages_filtered(self, task_id=None, user_id=None, text=None, date_from=None, date_to=None,
                              seen=None, board_id=None, session=None):
        return (session.query(self.message_class)
                .join(self.task_class)
                .outerjoin(self.task_user_relation_class,
                           self.task_user_relation_class.task_id == self.message_class.task_id)
                .filter(and_(self.message_class.task_id == task_id if task_id else True,
                             self.task_class.board_id == board_id if board_id else True,
                             self.task_user_relation_class.user_id == user_id if user_id else True,
                             text in self.message_class.text if text else True,
                             self.message_class.date >= date_from if date_from else True,
                             self.message_class.date <= date_to if date_to else True,
                             self.message_class.seen == seen if seen else True))).all()

    @session_injector
    def add_message(self, message, session=None):
        session.add(message)
        return message

    @session_injector
    def remove_message(self, message_id, session=None):
        return session.query(self.message_class).filter_by(message_id=message_id).delete()

    @session_injector
    def update_message(self, message, session=None):
        session.add(message)
        return session.query(self.message_class).get(message.message_id)
    # endregion

    # region task_relation queries
    @session_injector
    def get_task_relation(self, relation_id, session=None):
        return session.query(self.task_relation_class).get(relation_id)

    @session_injector
    def get_task_relations_filtered(self, parent_id=None, child_id=None, relation_types=None, session=None):
        return (session.query(self.task_relation_class).
                filter(and_(self.task_relation_class.parent_id == parent_id if parent_id else True,
                            self.task_relation_class.child_id == child_id if child_id else True,
                            self.task_relation_class.relation_type.in_(relation_types) if relation_types else True))).all()

    @session_injector
    def add_task_relation(self, task_relation, session=None):
        session.add(task_relation)
        return task_relation

    @session_injector
    def remove_task_relation(self, relation_id, session=None):
        return session.query(self.task_relation_class).filter_by(relation_id=relation_id).delete()

    @session_injector
    def update_task_relation(self, task_relation, session=None):
        session.add(task_relation)
        return session.query(self.task_relation_class).get(task_relation.relation_id)
    # # endregion

    # region reminder queries
    @session_injector
    def get_reminder(self, reminder_id, session=None):
        return session.query(self.reminder_class).get(reminder_id)

    @session_injector
    def get_reminders_filtered(self, status=None, session=None):
        return (session.query(self.reminder_class)
                .filter(self.reminder_class.status == status if status else True)).all()

    @session_injector
    def get_task_reminders(self, task_id, session=None):
        return session.query(self.reminder_class).join(self.task_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_reminders(self, task_id, session=None):
        return session.query(self.reminder_class).filter_by(task_id=task_id).all()

    @session_injector
    def get_user_reminders(self, user_id, session=None):
        return (session.query(self.reminder_class).join(self.task_class).join(self.task_user_relation_class)
                .filter_by(user_id=user_id)).all()

    @session_injector
    def add_reminder(self, reminder, session=None):
        session.add(reminder)
        return reminder

    @session_injector
    def remove_reminder(self, reminder_id, session=None):
        return session.query(self.reminder_class).filter_by(reminder_id=reminder_id).delete()

    @session_injector
    def update_reminder(self, reminder, session=None):
        session.add(reminder)
        return session.query(self.reminder_class).get(reminder.reminder_id)
    # endregion

    # region tag queries
    @session_injector
    def get_tag(self, tag_id, session=None):
        return session.query(self.tag_class).get(tag_id)

    @session_injector
    def get_tags_filtered(self, task_id=None, text=None, session=None):
        return (session.query(self.tag_class)
                .filter(and_(self.tag_class.task_id == task_id if task_id else True,
                             text in self.tag_class.text if text else True))).all()

    @session_injector
    def get_board_tags(self, board_id, session=None):
        return (session.query(self.tag_class).join(self.task_class)
                .filter(self.task_class.board_id == board_id)).all()

    @session_injector
    def get_task_tags(self, task_id, session=None):
        return (session.query(self.tag_class).join(self.task_class)
                .filter(self.task_class.task_id == task_id)).all()

    @session_injector
    def get_user_tags(self, user_id, text=None, session=None):
        return (session.query(self.tag_class)
                .join(self.task_class)
                .join(self.task_user_relation_class)
                .filter(and_(self.task_user_relation_class.user_id == user_id,
                             self.tag_class.text.like('%' + text + '%')))).all()

    @session_injector
    def add_tag(self, tag, session=None):
        session.add(tag)
        return tag

    @session_injector
    def remove_tag(self, tag_id, session=None):
        return session.query(self.tag_class).filter_by(tag_id=tag_id).delete()

    @session_injector
    def remove_tag_by_text(self, task_id, tag, session=None):
        return session.query(self.tag_class).filter_by(text=tag, task_id=task_id).delete()
    # endregion

    # region user queries
    @session_injector
    def get_user(self, user_id, session=None):
        return session.query(self.user_class).get(user_id)

    @session_injector
    def get_users(self, session=None):
        return session.query(self.user_class).all()

    @session_injector
    def get_user_by_email(self, email, session=None):
        return session.query(self.user_class).filter_by(email=email).first()

    @session_injector
    def get_user_by_login(self, login, session=None):
        return session.query(self.user_class).filter_by(login=login).first()

    @session_injector
    def get_task_creator(self, task_id, session=None):
        return (session.query(self.user_class).join(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_class.task_id == task_id,
                             self.task_user_relation_class.role == enum.Role.CREATOR.value))).first()

    @session_injector
    def get_task_executors(self, task_id, session=None):
        return (session.query(self.user_class).join(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_class.task_id == task_id,
                             self.task_user_relation_class.role == enum.Role.EXECUTOR.value))).all()

    @session_injector
    def get_task_observers(self, task_id, session=None):
        return (session.query(self.user_class).join(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_class.task_id == task_id,
                             self.task_user_relation_class.role == enum.Role.OBSERVER.value))).all()

    @session_injector
    def get_task_requester(self, task_id, session=None):
        return (session.query(self.user_class).join(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_class.task_id == task_id,
                             self.task_user_relation_class.role == enum.Role.REQUESTER.value))).first()

    @session_injector
    def get_task_finisher(self, task_id, session=None):
        return (session.query(self.user_class).join(self.task_user_relation_class).join(self.task_class)
                .filter(and_(self.task_class.task_id == task_id,
                             self.task_user_relation_class.role == enum.Role.FINISHER.value))).first()

    @session_injector
    def get_board_creator(self, board_id, session=None):
        return (session.query(self.user_class).join(self.user_board_relation_class)
                .filter(and_(self.user_board_relation_class.board_id == board_id,
                             self.user_board_relation_class.permission == enum.BoardPermission.CREATOR.value))).first()

    @session_injector
    def get_board_users(self, board_id, permissions=None, session=None):
        return (session.query(self.user_class).join(self.user_board_relation_class)
                .filter(and_(self.user_board_relation_class.board_id == board_id,
                             self.user_board_relation_class.permission.in_(permissions) if permissions else True))).all()

    @session_injector
    def add_user(self, user, session=None):
        session.add(user)
        return user

    @session_injector
    def remove_user(self, user_id, session=None):
        return session.query(self.user_class).filter_by(user_id=user_id).delete()

    @session_injector
    def remove_user_by_email(self, email, session=None):
        return session.query(self.user_class).filter_by(email=email).delete()

    @session_injector
    def remove_user_by_login(self, login, session=None):
        return session.query(self.user_class).filter_by(login=login).delete()

    @session_injector
    def update_user(self, user, session=None):
        session.add(user)
        return session.query(self.user_class).get(user.user_id)
    # endregion

    @session_injector
    def is_duplication(self, model_obj, session=None, **kwargs):
        return session.query(model_obj).filter_by(**kwargs).first() is not None

    @session_injector
    def is_exist(self, model_obj, obj_id, session=None):
        return session.query(model_obj).get(obj_id) is not None

    @session_injector
    def clone_task_users(self, from_task_id, to_task_id, session=None):
        task_users = self.get_task_user_relations_filtered(task_id=from_task_id, session=session)
        for task_user in task_users:
            if task_user.role in [enum.Role.OBSERVER.value, enum.Role.EXECUTOR.value, enum.Role.CREATOR.value]:
                relation = model.TaskUserRelation(task_user.role, to_task_id, task_user.user_id)
                self.add_task_user_relation(relation, session=session)

    @session_injector
    def clone_task_tags(self, from_task_id, to_task_id, session=None):
        task_tags = self.get_tags_filtered(task_id=from_task_id, session=session)
        for task_tag in task_tags:
            tag = model.Tag(to_task_id, task_tag.text)
            self.add_tag(tag, session=session)

    @staticmethod
    def set_up_database(connection_string):
        """Creating database and tables if it is necessary

        Parameters
        ----------
        connection_string - string format mysql//user:PASSWORD@host:port to connect to RDBMS

        Returns
        -------
        connection_string - string format mysql//user:PASSWORD@host:port/irontask to connect to database

        """
        mysql_engine = create_engine(connection_string)
        existing_databases = mysql_engine.execute("SHOW DATABASES;")
        # Results are a list of single item tuples, so unpack each tuple
        existing_databases = [d[0] for d in existing_databases]

        # Create database if not exists
        if 'irontask' not in existing_databases:
            mysql_engine.execute("CREATE DATABASE IF NOT EXISTS {0} ".format('irontask'))
        connection_string += '/irontask'

        engine = create_engine(connection_string)
        metadata = MetaData(engine)

        if not engine.dialect.has_table(engine, 'boards'):
            Table(
                'boards', metadata,
                Column('board_id', INTEGER(11), primary_key=True),
                Column('title', String(100), nullable=False),
                Column('color', String(10)),
                Column('description', String(200)),
                Column('status', TINYINT(4), nullable=False, server_default=text("'0'")),
                Column('board_type', TINYINT(4), nullable=False, server_default=text("'0'"))
            )

        if not engine.dialect.has_table(engine, 'users'):
            Table(
                'users', metadata,
                Column('user_id', INTEGER(11), primary_key=True),
                Column('login', String(30), nullable=False),
                Column('password', String(30)),
                Column('email', String(30))
            )

        if not engine.dialect.has_table(engine, 'tasks'):
            Table(
                'tasks', metadata,
                Column('task_id', INTEGER(11), primary_key=True),
                Column('board_id', ForeignKey('boards.board_id', ondelete='CASCADE'), index=True),
                Column('title', String(100), nullable=False),
                Column('description', String(200)),
                Column('status', TINYINT(4), nullable=False, server_default=text("'1'")),
                Column('priority', TINYINT(4), nullable=False, server_default=text("'0'"))
            )

        if not engine.dialect.has_table(engine, 'user_board_relations'):
            Table(
                'user_board_relations', metadata,
                Column('relation_id', INTEGER(11), primary_key=True),
                Column('user_id', ForeignKey('users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('board_id', ForeignKey('boards.board_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('permission', INTEGER(11), nullable=False, server_default=text("'1'"))
            )

        if not engine.dialect.has_table(engine, 'messages'):
            Table(
                'messages', metadata,
                Column('message_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('user_id', ForeignKey('users.user_id', ondelete='CASCADE'), index=True),
                Column('text', String(400)),
                Column('date', DateTime, nullable=False),
                Column('seen', TINYINT(1), nullable=False, server_default=text("'0'"))
            )

        if not engine.dialect.has_table(engine, 'plans'):
            Table(
                'plans', metadata,
                Column('plan_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('status', TINYINT(4), nullable=False),
                Column('periodicity', JSON(none_as_null=True), nullable=False),
                Column('end_type', TINYINT(4), nullable=False),
                Column('start_date', DateTime, nullable=False),
                Column('end_date', DateTime),
                Column('week_days', String(20)),
                Column('count_iteration', INTEGER(11)),
                Column('current_iteration', INTEGER(11))
            )

        if not engine.dialect.has_table(engine, 'reminders'):
            Table(
                'reminders', metadata,
                Column('reminder_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('status', TINYINT(4), nullable=False, server_default=text("'1'")),
                Column('text', String(200)),
                Column('periodicity', JSON(none_as_null=True)),
                Column('end_type', TINYINT(4), nullable=False),
                Column('start_date', DateTime),
                Column('end_date', DateTime),
                Column('week_days', String(20)),
                Column('count_iteration', INTEGER(11)),
                Column('current_iteration', INTEGER(11))
            )

        if not engine.dialect.has_table(engine, 'tags'):
            Table(
                'tags', metadata,
                Column('tag_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('text', String(50), nullable=False)
            )

        if not engine.dialect.has_table(engine, 'task_dates'):
            Table(
                'task_dates', metadata,
                Column('task_date_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('date', DateTime, nullable=False),
                Column('date_type', TINYINT(4), nullable=False)
            )

        if not engine.dialect.has_table(engine, 'task_relations'):
            Table(
                'task_relations', metadata,
                Column('relation_id', INTEGER(11), primary_key=True),
                Column('parent_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('child_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('relation_type', TINYINT(4), nullable=False)
            )

        if not engine.dialect.has_table(engine, 'task_user_relations'):
            Table(
                'task_user_relations', metadata,
                Column('relation_id', INTEGER(11), primary_key=True),
                Column('task_id', ForeignKey('tasks.task_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('user_id', ForeignKey('users.user_id', ondelete='CASCADE'), nullable=False, index=True),
                Column('role', TINYINT(4), nullable=False)
            )

        metadata.create_all()
        return connection_string


"""
    This package includes modules for work with
    data base

    Modules:
        services - provides data access and interaction
        constraints - contains constraints for data
        exceptions - lists exceptions that can rise within this package

    Functions:
        log - decorator to write logs of functions calling events
        exception_log - decorator to write logs of exceptions and warnings

"""
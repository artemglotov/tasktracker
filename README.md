# IRONTASK

Этот проект создан для того, чтобы вы смогли уследить за каждым мгновением вашей жизни и не упустить ничего важного. Планируйте задачи, выполняйте их, делитесь с друзьями. Этот тасктрекер создан в первую очередь для групповой работы: он позволит распределить обязанности, назначить ответственных и проверяющих, организвоать различные взаимосвязи между вашими задачами. Добро пожаловать в семью IRONTASK! 

### Terminal 

Программа предоставляет удобный и локаничный консольный интерфейс для работы с вышеперечисленными функциями. 

**Как установить программу:**

Скопируйте репоизторий:

```bash
user$ git clone https://artemglotov@bitbucket.org/artemglotov/tasktracker.git
```
Перейдите в дирикторию IRONTASK/irontaskterminal и установите программу:

```bash
user$ cd IRONTASK/irontaskterminal
user$ python3 setup.py install
```
**_Важно_! ** Перед первым использованием, запустите инициалирующую команду, указав connection string вашей базы данных (_MySQL, PostgreSQL, Oracle, Microsoft SQL Server, SQLite_) и имя профиля, если у вас ещё его нет (в любом случае создать профиль и залогиниться можно будет позже). Обращение к программе осуществляется с помощю команды **_iron_**.

```bash
user$ user$ iron init db//user:PASSWORD@host:port
Database was set up

```

Наши поздравления! Теперь вы можете запустить программу, используя команду **_iron_**!

**Пример использования**

``` bash
user$ iron task add "My first task"
---- Task was created ----
My first task
Description: None
Task id: 1085
Status: active
Priority: normal
Created on: 2018-09-11 09:13:49 (id: 24)
Creator:
        'artem' (id: 1)

user$ iron tasks
1 - My task (active)
7 - Planned task (active)
8 - Free task (active)
9 - Task of board (active)
1078 - Second task updated (active)
1084 - New task (active)
1085 - My first task (active)

user$ iron task update 1085 --priority high
---- Task was updated ----
My first task
Description: None
Task id: 1085
Status: active
Priority: high
Created on: 2018-09-11 09:13:49 (id: 24)
Edited on:
        2018-09-11 09:16:32 (id: 26)
Creator:
        'artem' (id: 1)

```



### Library 

Билиблиотека расположена в директории IRONTASK/irontasklib данного репозитория и представляет собой API для работы с приложением.  

**Как установить бибилиотеку:**

Скопируйте репоизторий:

``` bash 
user$ git clone https://artemglotov@bitbucket.org/artemglotov/tasktracker.git
```
Перейдите в дирикторию IRONTASK/irontasklib и установите модуль библиотеки:

``` bash
user$ cd IRONTASK/irontasklib
user$ python3 setup.py install
```

**Как использовать библиотеку:**

1. Импортируйте модуль библиотеки
2. Импортируйте из пакета _services_ одноименный модуль.
3. Пакет _storage_ содержит модуль реализации DAL для различных типов хранилищ, импортируйте его
4. Используя импортированный модуль, создайте _data storage class_ 
5. Имопртируйте из модуля services класс _Service_
6. Создайте объект класса Service, предоставив _data storage class_ 

**Пример использования библиотеки:**

``` python
>>> import irontasklib
>>> from irontasklib.services import services
>>> from irontasklib.storage import sqlalchemy_repos
>>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
>>> data_storage = sqlalchemy_repos.SQLAlchemyRepos(connection_string)
>>> service = services.Service(data_storage)
>>> from irontasklib.models import models as model
>>> user = service.add_user(model.User("Mitz"))
>>> task = service.add_task(model.Task("Finish him"))
>>> service.get_user_tasks(user.user_id)[0].title
"Finish him"
```


